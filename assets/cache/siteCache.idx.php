<?php
$c=&$this->config;$c['settings_version']="1.4.2";$c['manager_theme']="default";$c['server_offset_time']="0";$c['manager_language']="russian-UTF8";$c['modx_charset']="UTF-8";$c['site_name']="Enteley";$c['site_start']="1";$c['error_page']="7";$c['unauthorized_page']="7";$c['site_status']="1";$c['auto_template_logic']="parent";$c['default_template']="3";$c['old_template']="3";$c['publish_default']="1";$c['friendly_urls']="1";$c['friendly_alias_urls']="1";$c['use_alias_path']="1";$c['cache_type']="2";$c['failed_login_attempts']="3";$c['blocked_minutes']="60";$c['use_captcha']="0";$c['emailsender']="kiddr@rambler.ru";$c['use_editor']="1";$c['use_browser']="1";$c['fe_editor_lang']="russian-UTF8";$c['fck_editor_toolbar']="standard";$c['fck_editor_autolang']="0";$c['editor_css_path']="";$c['editor_css_selectors']="";$c['upload_maxsize']="10485760";$c['manager_layout']="4";$c['auto_menuindex']="1";$c['session.cookie.lifetime']="604800";$c['mail_check_timeperiod']="600";$c['manager_direction']="ltr";$c['xhtml_urls']="0";$c['automatic_alias']="1";$c['datetime_format']="dd-mm-YYYY";$c['warning_visibility']="0";$c['remember_last_tab']="1";$c['enable_bindings']="1";$c['seostrict']="1";$c['number_of_results']="30";$c['theme_refresher']="";$c['show_picker']="0";$c['show_newresource_btn']="0";$c['show_fullscreen_btn']="0";$c['email_sender_method']="1";$c['site_id']="5ae6fd49cbcf4";$c['site_unavailable_page']="";$c['reload_site_unavailable']="";$c['site_unavailable_message']="В настоящее время сайт недоступен.";$c['siteunavailable_message_default']="В настоящее время сайт недоступен.";$c['enable_filter']="0";$c['enable_at_syntax']="0";$c['cache_default']="1";$c['search_default']="1";$c['custom_contenttype']="application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json";$c['docid_incrmnt_method']="0";$c['enable_cache']="1";$c['minifyphp_incache']="0";$c['server_protocol']="http";$c['rss_url_news']="http://feeds.feedburner.com/evocms-release-news";$c['track_visitors']="0";$c['top_howmany']="10";$c['friendly_url_prefix']="";$c['friendly_url_suffix']=".html";$c['make_folders']="0";$c['aliaslistingfolder']="0";$c['allow_duplicate_alias']="0";$c['use_udperms']="1";$c['udperms_allowroot']="0";$c['email_method']="mail";$c['smtp_auth']="0";$c['smtp_secure']="none";$c['smtp_host']="smtp.example.com";$c['smtp_port']="25";$c['smtp_username']="you@example.com";$c['reload_emailsubject']="";$c['emailsubject']="Данные для авторизации";$c['emailsubject_default']="Данные для авторизации";$c['reload_signupemail_message']="";$c['signupemail_message']="Здравствуйте, [+uid+]!\n\nВаши данные для авторизации в системе управления сайтом [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация";$c['system_email_signup_default']="Здравствуйте, [+uid+]!\n\nВаши данные для авторизации в системе управления сайтом [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация";$c['reload_websignupemail_message']="";$c['websignupemail_message']="Здравствуйте, [+uid+]!\n\nВаши данные для авторизации на [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация";$c['system_email_websignup_default']="Здравствуйте, [+uid+]!\n\nВаши данные для авторизации на [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация";$c['reload_system_email_webreminder_message']="";$c['webpwdreminder_message']="Здравствуйте, [+uid+]!\n\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\n\n[+surl+]\n\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\n\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\n\nС уважением, Администрация";$c['system_email_webreminder_default']="Здравствуйте, [+uid+]!\n\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\n\n[+surl+]\n\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\n\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\n\nС уважением, Администрация";$c['tree_page_click']="27";$c['use_breadcrumbs']="0";$c['global_tabs']="1";$c['group_tvs']="0";$c['resource_tree_node_name']="pagetitle";$c['session_timeout']="15";$c['tree_show_protected']="0";$c['datepicker_offset']="-10";$c['number_of_logs']="100";$c['number_of_messages']="40";$c['which_editor']="TinyMCE4";$c['tinymce4_theme']="custom";$c['tinymce4_skin']="lightgray";$c['tinymce4_skintheme']="inlite";$c['tinymce4_template_docs']="";$c['tinymce4_template_chunks']="";$c['tinymce4_entermode']="p";$c['tinymce4_element_format']="xhtml";$c['tinymce4_schema']="html5";$c['tinymce4_custom_plugins']="advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube";$c['tinymce4_custom_buttons1']="undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect";$c['tinymce4_custom_buttons2']="link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code formatselect";$c['tinymce4_custom_buttons3']="";$c['tinymce4_custom_buttons4']="";$c['tinymce4_blockFormats']="Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3";$c['allow_eval']="with_scan";$c['safe_functions_at_eval']="time,date,strtotime,strftime";$c['check_files_onlogin']="index.php\n.htaccess\nmanager/index.php\nmanager/includes/config.inc.php";$c['validate_referer']="1";$c['rss_url_security']="http://feeds.feedburner.com/evocms-security-news";$c['error_reporting']="1";$c['send_errormail']="0";$c['pwd_hash_algo']="UNCRYPT";$c['reload_captcha_words']="";$c['captcha_words']="EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";$c['captcha_words_default']="EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";$c['filemanager_path']="C:/OpenServer/domains/enteley.com/assets/";$c['upload_files']="bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,fla,flv,swf,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip,JPG,JPEG,PNG,GIF,svg,tpl";$c['upload_images']="bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,svg";$c['upload_media']="au,avi,mp3,mp4,mpeg,mpg,wav,wmv";$c['upload_flash']="fla,flv,swf";$c['new_file_permissions']="0644";$c['new_folder_permissions']="0755";$c['which_browser']="mcpuk";$c['rb_webuser']="0";$c['rb_base_dir']="C:/OpenServer/domains/enteley.com/assets/";$c['rb_base_url']="assets/";$c['clean_uploaded_filename']="1";$c['strip_image_paths']="1";$c['maxImageWidth']="2600";$c['maxImageHeight']="2200";$c['thumbWidth']="150";$c['thumbHeight']="150";$c['thumbsDir']=".thumbs";$c['jpegQuality']="90";$c['denyZipDownload']="0";$c['denyExtensionRename']="0";$c['showHiddenFiles']="0";$c['lang_code']="ru";$c['sys_files_checksum']="a:4:{s:43:\"C:/OpenServer/domains/enteley.com/index.php\";s:32:\"2b1672b057429276f348f016b2b8ea5c\";s:43:\"C:/OpenServer/domains/enteley.com/.htaccess\";s:32:\"67a10fd6f78754b24eb7a6badc273fe6\";s:51:\"C:/OpenServer/domains/enteley.com/manager/index.php\";s:32:\"153c387763636c16fdd1d9225f89dcf3\";s:65:\"C:/OpenServer/domains/enteley.com/manager/includes/config.inc.php\";s:32:\"c8c7fc79e6b07de49fc0ef3a23ea6285\";}";$this->aliasListing=array();$a=&$this->aliasListing;$d=&$this->documentListing;$m=&$this->documentMap;$a[2]=array('id'=>2,'alias'=>'about','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['about']=2;$m[]=array(0=>2);$a[1]=array('id'=>1,'alias'=>'index','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['index']=1;$m[]=array(0=>1);$a[3]=array('id'=>3,'alias'=>'pravila','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['pravila']=3;$m[]=array(0=>3);$a[4]=array('id'=>4,'alias'=>'oplata-dostavka','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['oplata-dostavka']=4;$m[]=array(0=>4);$a[5]=array('id'=>5,'alias'=>'stock','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['stock']=5;$m[]=array(0=>5);$a[6]=array('id'=>6,'alias'=>'contact','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['contact']=6;$m[]=array(0=>6);$a[7]=array('id'=>7,'alias'=>'doc-not-found','path'=>'','parent'=>0,'isfolder'=>0,'alias_visible'=>1);$d['doc-not-found']=7;$m[]=array(0=>7);$c=&$this->contentTypes;$c=&$this->chunkCache;$c['HEADER']=' 			<header class="header">
                <div class="header__inner inner">
                    <div class="header-logo">
                        <div class="header-logo__image">
                            <a href="index.html" class="header-logo__link">
                                <img src="/assets/templates/enteley_template/img/style/logo.png" alt="Enteley" class="header-logo__img"/>
                            </a>
                        </div>
                    </div>

                    {{MENU_HEADER}}

                    <div class="header-contact">
                        <div class="header-contact__phone">
                            <p class="header-contact__tel">
                                <a href="tel:+7495327-81-20">+7 (495) 327-81-20</a>
                            </p>
                        </div>

                        <a href="[~[*id*]~]#myModal" title="Заказать звонок">
                            <span class="btn header-contact__btn" role="button">
                                <svg class="icon header__icon">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                                Заказать звонок
                            </span>
                        </a>
                    </div>

                    <div class="header-basket">
                        <a href="#" class="header-basket__link">
                            <svg class="icon header-basket__icon">
                                <use xlink:href="#icon-basket"></use>
                            </svg>

                            Ваша корзина
                            <span class="header-basket__sum">5</span>
                        </a>
                        <p class="header-basket__price">
                            на сумму 6860 Р
                        </p>
                    </div>
                </div>
            </header>';$c['mm_rules']='// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php
// example of how PHP is allowed - check that a TV named documentTags exists before creating rule

if ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getFullTableName(\'site_tmplvars\'), "name=\'documentTags\'"))) {
	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV
}
mm_widget_showimagetvs(); // Always give a preview of Image TVs

mm_createTab(\'SEO\', \'seo\', \'\', \'\', \'\', \'\');
mm_moveFieldsToTab(\'titl,keyw,desc,seoOverride,noIndex,sitemap_changefreq,sitemap_priority,sitemap_exclude\', \'seo\', \'\', \'\');
mm_widget_tags(\'keyw\',\',\'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV


//mm_createTab(\'Images\', \'photos\', \'\', \'\', \'\', \'850\');
//mm_moveFieldsToTab(\'images,photos\', \'photos\', \'\', \'\');

//mm_hideFields(\'longtitle,description,link_attributes,menutitle,content\', \'\', \'6,7\');

//mm_hideTemplates(\'0,5,8,9,11,12\', \'2,3\');

//mm_hideTabs(\'settings, access\', \'2\');
';$c['HEAD']='<!DOCTYPE html>
<html lang="[(lang_code)]">
    <head>
        <title>[(site_name)] | [*longtitle*]</title>
        <meta charset="[(modx_charset)]" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
		<meta name="keywords" content="[*keyw*]" />
		<meta name="description" content="[*desc*]" />
		<base href="[(site_url)]"/>
						
		<link href="[(site_url)][[if? &is=[(site_start)]:!=:[*id*] &then=`[~[*id*]~]`]]" rel="canonical">
        <link rel="stylesheet" href="/assets/templates/enteley_template/module/normalize.css" />
        <link rel="stylesheet" href="/assets/templates/enteley_template/css/style.css" />
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
    </head>';$c['MENU_HEADER']='					<nav class="header__menu">
						[[DLMenu?
								&parents=`0`
								&maxDepth=`1`
								&rowTpl=`@CODE:<li[+classes+]><a class="header__menu-link" href="[+url+]">[+title+]</a></li>`
								&outerClass=`header__menu-inner`
								&rowClass=`header__menu-item`
								&firstClass=``
								&lastClass=``
								&levelClass=``
								&oddClass=``
								&evenClass=``
						]]
                    </nav>';$c['MENU_FOOTER']='					<nav class="footer__menu">
                        <ul class="footer__menu-inner">
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~2~]">О компании</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~1~]">Каталог товаров</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~3~]">Памятка покупателю</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~4~]">Оплата и доставка</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~5~]">Акции</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~6~]">Контакты</a>
                            </li>
                        </ul>
                    </nav>					';$c['FOOTER']='            <footer class="footer">
                <div class="footer__inner inner">
                    <div class="footer-copy">
                        <p class="footer-copy__inner">
                            &copy; 2018 &laquoEnteley&raquo <br> Интернет-магазин тканей
                        </p>
                    </div>

                    <div class="footer-contact">
                        <div class="footer-contact__vcard vcard">
                            <div class="footer-contact__phone">
                                <span class="footer-contact__tel tel">
                                    <a href="tel:+7495327-81-20">+7 (495) 327-81-20</a>
                                </span>
                            </div>

                            <div class="footer-contact__adr adr">
                                <span class="footer-contact__locality locality">г. Москва, М. Станкостроительная</span><br>
                                <span class="footer-contact__street-address street-address">ул. Станкостроителей, д. 25</span>
                            </div>

                            <p class="footer-contact__email">
                                <a class="email" href="mailto:example@domain.com">Написать письмо</a>
                            </p>
                        </div>
                    </div>

                    {{MENU_FOOTER}}

                    <div class="footer-share">
                        <p class="footer-share__item">
                            Поделись с друзьями
                        </p>

                        <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,twitter" data-counter=""></div>
                    </div>
                </div>
            </footer>';$c['MAIN_INDEX']='            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">[*pagetitle*]</h1>
                    </header>
                    
                    {{SECTION_PRODUCT_LIST}}
                </section>    
            </main>';$c['WRAPPER_ABOUT']='        <div class="wrapper">           
           	{{HEADER}}
			{{MAIN_ABOUT}}
			{{FOOTER}}
        </div>';$c['MAIN_ABOUT']='            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">[*pagetitle*]</h1>
                    </header>
                    
                    <h2><em>Интернет-магазин Enteley.ru это:</em></h2>
                    
                    <p>- Легендарные ткани класса premium luxe и haute couture</p>
                    <p>- Ежедневное обновление каталога</p>
                    <p>- Лучшие и самые новые коллекции</p>
                    <p>- Эксклюзивные ткани</p>
                    <p>- Регулярные акции и скидки</p>
                    <p>- Помощь в подборе тканей</p>
                    <p>Компания "Enteley" насчитывает почти 20 лет успешной работы. Мы постоянно двигаемся вперед, покоряя всё новые вершины. Присоединяйтесь!</p>      
                </section>    
            </main>';$c['MAIN_OPLATA_DOSTAVKA']='            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">[*pagetitle*]</h1>
                    </header>
					
					<div class="main__two-column">
                        <h4>Оплата</h4>
                        <p><strong>Оплата банковским переводом (квитанцией)</strong></p>
                        <p>Перевод осуществляется по реквизитам через онлайн сервис Вашего банка или по квитанции, которую можно оплатить в любом банке.
                            Обычно банк взимает комиссию 0,5-2% от суммы перевода. Срок зачисления денежных средств 1-2 раб.дня. 
                            Реквизиты для оплаты Вы найдете в разделе Контакты, в назначении платежа указывайте номер заказа.
                            Банковскую квитанцию можно сформировать в личном кабинете в разделе "Мои заказы". 
                            По Вашей просьбе мы можем отправить квитанцию на электронную почту или мессенджерами Viber, WhatsApp</p>
                        
                        <p><strong>Оплата заказа пластиковой картой</strong></p>
                        <p>Оплата происходит через провайдера электронных платежей Ассист, который обеспечивает безопасность. <a href="#"> Подробнее...</a>
                            Оплата происходит мгновенно, без комиссии. Возврат денежных средств производится на карту, с которой был произведен платеж.</p>
                        <p>Доступные платежные системы: <em>Visa</em>, <em>Maestro</em>, <em>MasterCard</em></p>
                        <p><strong>Оплата наложенным платежом</strong></p>
                        <p>Данный метод оплаты доступен только для заказов доставляемых курьерской службой СДЭК, сумма которых не превышает 10 000 руб. 
                            Оплачивается заказ и стоимость доставки во время получения посылки. За услугу наложенный платеж взимается комиссия 3-4% от стоимости заказа.</p>
                        <p><strong>Оплата по счету</strong></p>
                        <p>Данный метод оплаты доступен для юридических лиц. Для выставления счета и заключения договора необходимо выслать на электронную почту example@domain.com реквизиты и документы (свидетельство, устав и т.п.). 
                            При поступлении денежных средств на наш расчетный счет происходит доставка товара на указанный адрес. Вместе с заказом Вы получите накладную и счет-фактуру.</p>
                        
                        <h4>Доставка</h4>
                        <p>Осуществляется из городов Москва или Санкт-Петербург Почтой России или Курьерской Службой СДЭК.
                            Доставка оплачивается при получении.
                            Предварительный расчет:
                            На сайте "Почта России"</p>
                        <p><strong>Самовывоз из салонов </strong></p>
                        <p>По согласованию с менеджером и при 100% предоплате. Все адреса салонов, домов тканей и пунктов выдачи можно посмотреть в разделе Контакты</p>
                        <p><strong>В СНГ и другие страны</strong></p>
                        <p>В Белоруссию и Казахстан доставка осуществляется курьерской службой СДЭК или Почтой России только после предварительной оплаты за товар и доставку, стоимость которой рассчитывает менеджер. Информация по другим странам предоставляется по запросу</p>
                        <p><strong>Сроки и стоимость</strong></p>
                        <p>Срок доставки и стоимость зависит от региона, в котором Вы проживаете, от веса посылки, от тарифов на услуги страхование и наложенного платежа, от способа доставки. Значительную экономию даёт 100% предоплата, так как услуга наложенного платежа составляет 3-4% от стоимости товара.
                            Полный расчет предоставит и согласует с Вами менеджер перед отправкой.</p>
                        <p><strong>Получение</strong></p>
                        <p>Независимо от способа доставки Вы сможете отследить местонахождение свой посылки по номеру отправления в личном кабинете. При получении необходимо удостоверение личности</p>
                        <p><strong>Ответственность</strong></p>
                        <p>Ответственность за доставку и сохранность несет логистическая компания с момента передачи ей товара. Все отправления подлежат обязательному страхованию, стоимость страховки включена в стоимость доставки</p>
                    </div>
                </section>    
            </main>';$c['WRAPPER_PRAVILA']='        <div class="wrapper">           
           	{{HEADER}}
			{{MAIN_PRAVILA}}
			{{FOOTER}}
        </div>';$c['WRAPPER_OPLATA_DOSTAVKA']='        <div class="wrapper">           
           	{{HEADER}}
			{{MAIN_OPLATA_DOSTAVKA}}
			{{FOOTER}}
        </div>';$c['MAIN_PRAVILA']='            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">[*pagetitle*]</h1>
                    </header>
					
					<p>Сомневаетесь, сколько метров ткани купить? Загляните в <a href="#">эту памятку</a>, и Вы никогда не ошибетесь!</p>
                    <p>Узнайте о нашей <a href="#">системе скидок!</a></p>
                    <p>Не понимаете, насколько плотная ткань? Тогда прочитайте информацию <a href="#">о плотности тканей!</a></p>
                    <p>Как продлить жизнь тканям? Прочитайте <a href="#">правила по уходу</a>!</p>
                    <p>Как заказать <a href="#">образцы тканей</a>?</p>
                    <p>Остались ещё вопросы? Ответы на самые распространенные вопросы <a href="#">здесь</a>!</p>
                </section>    
            </main>';$c['WRAPPER_STOCK']='        <div class="wrapper">           
           	{{HEADER}}
			{{MAIN_STOCK}}
			{{FOOTER}}
        </div>';$c['MAIN_STOCK']='            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">[*pagetitle*]</h1>
                    </header>
					
					<h2>What is Lorem Ipsum?</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <h2>Where does it come from?</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </section>    
            </main>';$c['WRAPPER_CONTACT']='        <div class="wrapper">           
           	{{HEADER}}
			{{MAIN_CONTACT}}
			{{FOOTER}}
        </div>';$c['MAIN_CONTACT']='            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">[*pagetitle*]</h1>
                    </header>
					
					<p><strong>+7 (495) 327-81-20</strong></p>
                    <p><em>Понедельник-воскресенье с 9.00 до 20.30</em></p>
                    <p>Елена, Ольга (Москва) - менеджеры по работе с клиентами</p>
                    <p>
                        <a class="email" href="mailto:manager-example@domain.com">manager-example@domain.com</a>
                    </p>
                    <p>
                        г. Москва, М. Станкостроительная ул. Станкостроителей, д. 25
                    </p>
                </section>    
            </main>';$c['LIST_SUMMARY_FASHION']='<div class="main-fashion__image">
	<img src="[+image+]" alt="Enteley" class="main-fashion__img"/>
</div>';$c['CONTACT_FORM_REPORT_TPL']='<p>Это сообщение было отправлено посетителем по имени [+name.value+] с помощью формы заказать звонок.</p>
<p>Отправленная информация:</p>
<ul>
	<li><b>Имя:</b> [+name.value+]</li>
	<li><b>Телефон:</b> [+phone.value+]</li>
</ul>
';$c['WRAPPER_INDEX']='        <div class="wrapper">           
           	{{HEADER}}
			{{MAIN_INDEX}}
			{{FOOTER}}
        </div>';$c['MODAL_CONTACT']='<div id="myModal" class="modal">
	<div class="modal__dialog">
		<div class="modal__content">
			<div class="modal__header">
				<h3 class="modal__title">Заказать звонок</h3>
				<a href="[~[*id*]~]#close" title="Закрыть" class="modal__close">×</a>
			</div>
			<div class="modal__body">
				<div id="frmwrapper">
					[!FormLister?
						&formid=`contact-form`
						&formMethod=`post`
						&rules=`{
									"name": {
										"required":"Введите имя"
									},
									"phone": {
										"required":"Введите номер телефона",
										"phone":"Введите номер телефона правильно"
									}
								}`
						&protectSubmit=`1`
						&to=`kiddrtest@gmail.com`
						&from=`kiddrtest@gmail.com`
						&errorClass=`error`
						&requiredClass=`error`
						&formTpl=`CONTACT_FORM_TPL`
						&messagesOuterTpl=`CONTACT_FORM_MESSAGE_OUTER_TPL`
						&errorTpl=`CONTACT_FORM_ERROR_TPL`
						&subjectTpl=`CONTACT_FORM_SUBJECT_TPL`
						&reportTpl=`CONTACT_FORM_REPORT_TPL`
						&successTpl=`CONTACT_FORM_SUCCESS_TPL`
					!]
				</div>
			</div>
			<div class="modal__footer">
				<a href="[~[*id*]~]#close" title="Закрыть" class="btn modal__close">Закрыть</a>
			</div>
		</div>
	</div>
</div>';$c['LIST_SUMMARY_PRODUCT']='<div class="main-product__item">
	<div class="main-product__image">
		<img src="[+image+]" alt="Enteley" class="main-product__img"/>

		<p class="main-product__price">[+price+] Р</p>
	</div>

	<div class="main-product__description">
		<span class="btn main-product__btn" role="button">
			<svg class="icon header__icon">
				<use xlink:href="#icon-basket"></use>
			</svg>
			Купить товар
		</span>

		<div class="main-product_detail">
			<p>Код: [+code+]</p>
			<p>Производитель: [+manufacturer+]</p>
			<p>Состав: [+composition+]</p>
		</div>
	</div>
</div>';$c['CONTACT_FORM_SUCCESS_TPL']='<h3>Спасибо!</h3>
<p>Наш менеджер свяжется с вами в ближайшее время.</p>';$c['ICONS']='        <div class="hidden">
            <?xml version="1.0" encoding="utf-8"?>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <symbol viewBox="337 -266.1 928.6 786.1" id="icon-basket" xmlns="http://www.w3.org/2000/svg">
                    <path d="M672.8 398c-15.3-12.7-32-19.7-50-21s-34.7 5.7-50 21-22.7 32-22 50 8 34.7 22 50c14 15.3 30.7 22.7 50 22 19.3-.7 36-8 50-22s21-30.7 21-50c1.3-20.6-5.7-37.3-21-50zm500 0c-15.3-12.7-32-19.7-50-21s-34.7 5.7-50 21c-15.3 15.3-22.7 32-22 50 .7 18 8 34.7 22 50 14 15.3 30.7 22.7 50 22 19.3-.7 36-8 50-22s21-30.7 21-50c1.3-20.6-5.7-37.3-21-50zm82-582c-7.3-7.3-15.7-11-25-11h-670c.7-2-.3-6.7-3-14s-3.7-13-3-17-.7-8.7-4-14-6-10-8-14-5.3-7-10-9-10-3-16-3h-143c-8.7-.7-17 2.7-25 10s-11.7 16-11 26c.7 10 4.3 18.3 11 25 6.7 6.7 15 10 25 10h114l98 459c0 2-2.7 7.7-8 17s-9.3 16.7-12 22-5.7 12-9 20-5 14-5 18c0 9.3 3.7 17.7 11 25s15.7 11 25 11h571c11.3 0 20-3.7 26-11s9.3-15.7 10-25c.7-9.3-2.7-17.7-10-25-7.3-7.3-16-11-26-11h-513c8.7-18 13-29.7 13-35 0-4-2.3-17.3-7-40l583-68c8-1.3 15.3-5.3 22-12s10-14.3 10-23v-286c0-9.3-3.7-17.6-11-25z"/>
                </symbol>

                <symbol viewBox="266 -266 786 786" id="icon-phone" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1050.1 343c-2-6.7-16-16.3-42-29-6.7-4-16.7-9.7-30-17s-25.3-14-36-20-20.7-11.7-30-17c-1.3-1.3-6-4.7-14-10s-14.7-9.3-20-12-10.3-4-15-4c-8-.7-17.3 4.7-28 16s-21 23-31 35-20 23.3-30 34-18.3 16-25 16c-4.7.7-9 0-13-2s-7.7-3.7-11-5-8-4-14-8-9.3-6-10-6c-50.7-28-94.3-60.3-131-97s-69-80.7-97-132c0 .7-2.3-2.7-7-10s-7.3-12-8-14-2-5.7-4-11-3-9.7-3-13c.7-6.7 6-15 16-25s21.3-20 34-30 24.3-20.3 35-31 16-19.7 16-27c0-5.3-1.3-10.7-4-16s-6.7-12-12-20-8.7-12.7-10-14c-4.7-10-10.3-20-17-30-6.7-10-13.3-21.7-20-35s-12-23.7-16-31c-13.3-26-23.3-40-30-42-2.7-1.3-6.7-2-12-2-10 0-23 2-39 6s-28.7 8-38 12c-18.7 7.3-38.3 30-59 68-19.3 34.7-29 69.3-29 104-.7 10 0 19.7 2 29s4.3 20 7 32 5.3 20.7 8 26c2.7 5.3 6.7 15.7 12 31s8.7 24.7 10 28c12.7 36 28 68.3 46 97 28.7 48 68.7 97.3 120 148s100.7 90.7 148 120c28.7 18 61 33.7 97 47 4 1.3 13.3 4.7 28 10s25 9 31 11 14.7 4.7 26 8 22 5.7 32 7 20 2 30 2c34 0 68.3-9.3 103-28 38-21.3 60.7-41.3 68-60 4-9.3 8-22 12-38s6-29 6-39c0-5.3-.7-9.3-2-12z"/>
                </symbol>
            </svg>
        </div>';$c['MAIN_SCRIPT']='<script src="http://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="http://yastatic.net/share2/share.js"></script>';$c['SECTION_PRODUCT_LIST']='<section class="main__inner">
	<div class="main-product">
		[!DocLister?
		&controller=`onetable`
		&table=`products`
		&idType=`documents`
		&orderBy=`id DESC`
		&display=`6`
		&ignoreEmpty=`1`
		&tpl=`LIST_SUMMARY_PRODUCT`
		&prepare=`prepare_img_product`
		!]
	</div> 

	<div class="main-fashion">
		[!DocLister?
		&controller=`onetable`
		&table=`fashion`
		&idType=`documents`
		&orderBy=`id DESC`
		&display=`1`
		&tpl=`LIST_SUMMARY_FASHION`
		&ignoreEmpty=`1`
		&prepare=`prepare_img_fashion`
		!]
	</div>
</section>

<section class="main__inner flex-reverse">
	<div class="main-product">
		[!DocLister?
		&controller=`onetable`
		&table=`products`
		&idType=`documents`
		&orderBy=`id DESC`
		&display=`6`							
		&offset=`6`
		&ignoreEmpty=`1`
		&tpl=`LIST_SUMMARY_PRODUCT`
		&prepare=`prepare_img_product`
		!]
	</div>
	<div class="main-fashion">
		[!DocLister?
		&controller=`onetable`
		&table=`fashion`
		&idType=`documents`
		&orderBy=`id DESC`
		&display=`1`
		&offset=`1`
		&tpl=`LIST_SUMMARY_FASHION`
		&ignoreEmpty=`1`
		&prepare=`prepare_img_fashion`
		!]
	</div>
</section>

<section class="main__inner">
	<div class="main-product">
		[!DocLister?
		&controller=`onetable`
		&table=`products`
		&idType=`documents`
		&orderBy=`id DESC`
		&display=`6`
		&offset=`12`
		&ignoreEmpty=`1`
		&tpl=`LIST_SUMMARY_PRODUCT`
		&prepare=`prepare_img_product`
		!]
	</div> 

	<div class="main-fashion">
		[!DocLister?
		&controller=`onetable`
		&table=`fashion`
		&idType=`documents`
		&orderBy=`id DESC`
		&display=`1`
		&offset=`2`
		&tpl=`LIST_SUMMARY_FASHION`
		&ignoreEmpty=`1`
		&prepare=`prepare_img_fashion`
		!]
	</div>
</section>    ';$c['CONTACT_FORM_ERROR_TPL']='<div class="error">[+message+]</div>';$c['CONTACT_FORM_MESSAGE_OUTER_TPL']='<div class="error">[+messages+]</div>';$c['CONTACT_FORM_TPL']='<form action="#myModal" method="post" class="modal__form">
	<input type="hidden" name="formid" value="contact-form" />
	<fieldset class="modal__form-fieldset">
		<legend class="modal__form-legend">Контактная информация</legend>

		<p class="modal__form-inner [+name.errorClass+] [+name.requiredClass+]">
			<label class="modal__form-label" for="name">Имя <em>*</em></label>
			<input type="text" class="modal__form-input" id="name" name="name" placeholder="Введите имя" value="[+name.value+]">
			<span class="error">[+name.error+]</span>
		</p>
		
		<p class="modal__form-inner [+phone.errorClass+] [+phone.requiredClass+]">
			<label class="modal__form-label" for="phone">Телефон <em>*</em></label>
			<input type="tel" class="modal__form-input [+phone.errorClass+] [+phone.requiredClass+]" id="phone" name="phone" placeholder="Введите номер телефона" value="[+phone.value+]">
			<span class="error">[+phone.error+]</span>
		</p>

		<p class="modal__form-inner">
			<input class="btn__lr modal__form-btn" type="submit" value="Отправить">
		</p>
	</fieldset>
	<p>[+form.messages+]</p>
</form>';$s=&$this->snippetCache;$s['DLCrumbs']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLCrumbs.php\';';$s['DLMenu']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLMenu.php\';';$s['DLSitemap']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLSitemap.php\';';$s['DocInfo']='return require MODX_BASE_PATH.\'assets/snippets/docinfo/snippet.docinfo.php\';';$s['DocLister']='return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DocLister.php\';';$s['FormLister']='return require MODX_BASE_PATH.\'assets/snippets/FormLister/snippet.FormLister.php\';';$s['if']='return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';';$s['phpthumb']='return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';';$s['summary']='return require MODX_BASE_PATH.\'assets/snippets/summary/snippet.summary.php\';';$s['prepare_img_product']='$data[\'image\'] = $modx->runSnippet(\'phpthumb\', array(\'input\' => \'/assets/templates/enteley_template/img/material/\'.$data[\'image\'], \'options\'=>\'w=178,h=178,zc=1\'));

return $data;';$s['prepare_img_fashion']='$data[\'image\'] = $modx->runSnippet(\'phpthumb\', array(\'input\' => \'/assets/templates/enteley_template/img/slide/\'.$data[\'image\'], \'options\'=>\'w=378,h=378,zc=1\'));

return $data;';$p=&$this->pluginCache;$p['CodeMirror']='/**
 * CodeMirror
 *
 * JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.33 (released on 21-12-2017)
 *
 * @category    plugin
 * @version     1.5
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @package     evo
 * @internal    @events OnDocFormRender,OnChunkFormRender,OnModFormRender,OnPluginFormRender,OnSnipFormRender,OnTempFormRender,OnRichTextEditorInit
 * @internal    @modx_category Manager and Admin
 * @internal    @properties &theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &darktheme=Dark Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;one-dark &fontSize=Font-size;list;10,11,12,13,14,15,16,17,18;14 &lineHeight=Line-height;list;1,1.1,1.2,1.3,1.4,1.5;1.3 &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250
 * @internal    @installset base
 * @reportissues https://github.com/evolution-cms/evolution/issues/
 * @documentation Official docs https://codemirror.net/doc/manual.html
 * @author      hansek from http://www.modxcms.cz
 * @author      update Mihanik71
 * @author      update Deesen
 * @author      update 64j
 * @lastupdate  08-01-2018
 */

$_CM_BASE = \'assets/plugins/codemirror/\';

$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;

require(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');';$p['CodeMirrorProps']='{"theme":"default","darktheme":"one-dark","fontSize":"14","lineHeight":"1.3","indentUnit":"4","tabSize":"4","lineWrapping":"true","matchBrackets":"true","activeLine":"false","emmet":"true","search":"false","indentWithTabs":"true","undoDepth":"200","historyEventDelay":"1250"}';$p['FileSource']='require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';';$p['Forgot Manager Login']='require MODX_BASE_PATH.\'assets/plugins/forgotmanagerlogin/plugin.forgotmanagerlogin.php\';';$p['ManagerManager']='/**
 * ManagerManager
 *
 * Customize the EVO Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.
 *
 * @category plugin
 * @version 0.6.3
 * @license http://creativecommons.org/licenses/GPL/2.0/ GNU Public License (GPL v2)
 * @internal @properties &remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules
 * @internal @events OnDocFormRender,OnDocFormPrerender,OnBeforeDocFormSave,OnDocFormSave,OnDocDuplicate,OnPluginFormRender,OnTVFormRender
 * @internal @modx_category Manager and Admin
 * @internal @installset base
 * @internal @legacy_names Image TV Preview, Show Image TVs
 * @reportissues https://github.com/DivanDesign/MODXEvo.plugin.ManagerManager/
 * @documentation README [+site_url+]assets/plugins/managermanager/readme.html
 * @documentation Official docs http://code.divandesign.biz/modx/managermanager
 * @link        Latest version http://code.divandesign.biz/modx/managermanager
 * @link        Additional tools http://code.divandesign.biz/modx
 * @link        Full changelog http://code.divandesign.biz/modx/managermanager/changelog
 * @author      Inspired by: HideEditor plugin by Timon Reinhard and Gildas; HideManagerFields by Brett @ The Man Can!
 * @author      DivanDesign studio http://www.DivanDesign.biz
 * @author      Nick Crossland http://www.rckt.co.uk
 * @author      Many others
 * @lastupdate  22/01/2018
 */

// Run the main code
include($modx->config[\'base_path\'].\'assets/plugins/managermanager/mm.inc.php\');';$p['ManagerManagerProps']='{"remove_deprecated_tv_types_pref":"yes","config_chunk":"mm_rules"}';$p['OutdatedExtrasCheck']='/**
 * OutdatedExtrasCheck
 *
 * Check for Outdated critical extras not compatible with EVO 1.4.0
 *
 * @category	plugin
 * @version     1.4.0 
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @package     evo
 * @author      Author: Nicola Lambathakis
 * @internal    @events OnManagerWelcomeHome
 * @internal    @properties &wdgVisibility=Show widget for:;menu;All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly;All &ThisRole=Run only for this role:;string;;;(role id) &ThisUser=Run only for this user:;string;;;(username) &DittoVersion=Min Ditto version:;string;2.1.3 &EformVersion=Min eForm version:;string;1.4.9 &AjaxSearchVersion=Min AjaxSearch version:;string;1.11.0 &WayfinderVersion=Min Wayfinder version:;string;2.0.5 &WebLoginVersion=Min WebLogin version:;string;1.2 &WebSignupVersion=Min WebSignup version:;string;1.1.2 &WebChangePwdVersion=Min WebChangePwd version:;string;1.1.2 &BreadcrumbsVersion=Min Breadcrumbs version:;string;1.0.5 &ReflectVersion=Min Reflect version:;string;2.2 &JotVersion=Min Jot version:;string;1.1.5 &MtvVersion=Min multiTV version:;string;2.0.13 &badthemes=Outdated Manager Themes:;string;MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle
 * @internal    @modx_category Manager and Admin
 * @internal    @installset base
 * @internal    @disabled 0
 */

// get manager role check
$internalKey = $modx->getLoginUserID();
$sid = $modx->sid;
$role = $_SESSION[\'mgrRole\'];
$user = $_SESSION[\'mgrShortname\'];
// show widget only to Admin role 1
if(($role!=1) AND ($wdgVisibility == \'AdminOnly\')) {}
// show widget to all manager users excluded Admin role 1
else if(($role==1) AND ($wdgVisibility == \'AdminExcluded\')) {}
// show widget only to "this" role id
else if(($role!=$ThisRole) AND ($wdgVisibility == \'ThisRoleOnly\')) {}
// show widget only to "this" username
else if(($user!=$ThisUser) AND ($wdgVisibility == \'ThisUserOnly\')) {}
else {
// get plugin id and setting button
$result = $modx->db->select(\'id\', $this->getFullTableName("site_plugins"), "name=\'{$modx->event->activePlugin}\' AND disabled=0");
$pluginid = $modx->db->getValue($result);
if($modx->hasPermission(\'edit_plugin\')) {
$button_pl_config = \'<a data-toggle="tooltip" href="javascript:;" title="\' . $_lang["settings_config"] . \'" class="text-muted pull-right" onclick="parent.modx.popup({url:\\\'\'. MODX_MANAGER_URL.\'?a=102&id=\'.$pluginid.\'&tab=1\\\',title1:\\\'\' . $_lang["settings_config"] . \'\\\',icon:\\\'fa-cog\\\',iframe:\\\'iframe\\\',selector2:\\\'#tabConfig\\\',position:\\\'center center\\\',width:\\\'80%\\\',height:\\\'80%\\\',hide:0,hover:0,overlay:1,overlayclose:1})" ><i class="fa fa-cog fa-spin-hover" style="color:#FFFFFF;"></i> </a>\';
}
$modx->setPlaceholder(\'button_pl_config\', $button_pl_config);
//plugin lang
$_oec_lang = array();
$plugin_path = $modx->config[\'base_path\'] . "assets/plugins/extrascheck/";
include($plugin_path . \'lang/english.php\');
if (file_exists($plugin_path . \'lang/\' . $modx->config[\'manager_language\'] . \'.php\')) {
include($plugin_path . \'lang/\' . $modx->config[\'manager_language\'] . \'.php\');
}
//run the plugin
// get globals
global $modx,$_lang;
//function to extract snippet version from description <strong></strong> tags 
if (!function_exists(\'getver\')) {
function getver($string, $tag)
{
$content ="/<$tag>(.*?)<\\/$tag>/";
preg_match($content, $string, $text);
return $text[1];
	}
}
$e = &$modx->Event;
$EVOversion = $modx->config[\'settings_version\'];
$output = \'\';
//get extras module id for the link
$modtable = $modx->getFullTableName(\'site_modules\');
$getExtra = $modx->db->select( "id, name", $modtable, "name=\'Extras\'" );
while( $row = $modx->db->getRow( $getExtra ) ) {
$ExtrasID = $row[\'id\'];
}
//check outdated files
//ajax index
$indexajax = "../index-ajax.php";
if (file_exists($indexajax)){
    $output .= \'<div class="widget-wrapper alert alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>index-ajax.php</b> \'.$_oec_lang[\'not_used\'].\' <b>Evolution \'.$EVOversion.\'</b>.  \'.$_oec_lang[\'if_dont_use\'].\', \'.$_oec_lang[\'please_delete\'].\'.</div>\';
}
//check outdated default manager themes
$oldthemes = explode(",","$badthemes");
foreach ($oldthemes as $oldtheme){
	if (file_exists(\'media/style/\'.$oldtheme)){
    $output .= \'<div class="widget-wrapper alert alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\'.$oldtheme.\'</b> \'.$_lang["manager_theme"].\',  \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>.   \'.$_oec_lang[\'please_delete\'].\' \'.$_oec_lang[\'from_folder\'].\' \' . MODX_MANAGER_PATH . \'media/style/.</div>\';
}
}	
//get site snippets table
$table = $modx->getFullTableName(\'site_snippets\');
//check ditto
//get min version from config
$minDittoVersion = $DittoVersion;
//search the snippet by name
$CheckDitto = $modx->db->select( "id, name, description", $table, "name=\'Ditto\'" );
if($CheckDitto != \'\'){
while( $row = $modx->db->getRow( $CheckDitto ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_ditto_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_ditto_version,$minDittoVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_ditto_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minDittoVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>DocLister</b></div>\';
		}
	}
} 
//end check ditto

//check eform
//get min version from config
$minEformVersion = $EformVersion;
//search the snippet by name
$CheckEform = $modx->db->select( "id, name, description", $table, "name=\'eForm\'" );
if($CheckEform != \'\'){
while( $row = $modx->db->getRow( $CheckEform ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_Eform_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_Eform_version,$minEformVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_Eform_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minEformVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';
		}
	}
} 
//end check eform
	
//check AjaxSearch
//get min version from config
$minAjaxSearchVersion = $AjaxSearchVersion;
//search the snippet by name
$CheckAjaxSearch = $modx->db->select( "id, name, description", $table, "name=\'AjaxSearch\'" );
if($CheckAjaxSearch != \'\'){
while( $row = $modx->db->getRow( $CheckAjaxSearch ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_AjaxSearch_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_AjaxSearch_version,$minAjaxSearchVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_AjaxSearch_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minAjaxSearchVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';
		}
	}
} 
//end check AjaxSearch	
	
//check Wayfinder
//get min version from config
$minWayfinderVersion = $WayfinderVersion;
//search the snippet by name
$CheckWayfinder = $modx->db->select( "id, name, description", $table, "name=\'Wayfinder\'" );
if($CheckWayfinder != \'\'){
while( $row = $modx->db->getRow( $CheckWayfinder ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_Wayfinder_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_Wayfinder_version,$minWayfinderVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_Wayfinder_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWayfinderVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';
		}
	}
} 
//end check Wayfinder
	
//check WebLogin
//get min version from config
$minWebLoginVersion = $WebLoginVersion;
//search the snippet by name
$CheckWebLogin = $modx->db->select( "id, name, description", $table, "name=\'WebLogin\'" );
if($CheckWebLogin != \'\'){
while( $row = $modx->db->getRow( $CheckWebLogin ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_WebLogin_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_WebLogin_version,$minWebLoginVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_WebLogin_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebLoginVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';
		}
	}
} 
//end check WebLogin

//check WebChangePwd
//get min version from config
$minWebChangePwdVersion = $WebChangePwdVersion;
//search the snippet by name
$CheckWebChangePwd = $modx->db->select( "id, name, description", $table, "name=\'WebChangePwd\'" );
if($CheckWebLogin != \'\'){
while( $row = $modx->db->getRow( $CheckWebChangePwd ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_WebChangePwd_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_WebChangePwd_version,$minWebChangePwdVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_WebChangePwd_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebChangePwdVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';
		}
	}
} 
//end check WebChangePwd
	
//check WebSignup
//get min version from config
$minWebSignupVersion = $WebSignupVersion;
//search the snippet by name
$CheckWebSignup = $modx->db->select( "id, name, description", $table, "name=\'WebSignup\'" );
if($CheckWebSignup != \'\'){
while( $row = $modx->db->getRow( $CheckWebSignup ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_WebSignup_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_WebSignup_version,$minWebSignupVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_WebSignup_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebSignupVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';
		}
	}
} 
//end check WebSignup

//check Breadcrumbs
//get min version from config
$minBreadcrumbsVersion = $BreadcrumbsVersion;
//search the snippet by name
$CheckBreadcrumbs = $modx->db->select( "id, name, description", $table, "name=\'Breadcrumbs\'" );
if($CheckBreadcrumbs != \'\'){
while( $row = $modx->db->getRow( $CheckBreadcrumbs ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_Breadcrumbs_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_Breadcrumbs_version,$minBreadcrumbsVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_Breadcrumbs_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minBreadcrumbsVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';
		}
	}
} 
//end check Breadcrumbs

//check Reflect
//get min version from config
$minReflectVersion = $ReflectVersion;
//search the snippet by name
$CheckReflect = $modx->db->select( "id, name, description", $table, "name=\'Reflect\'" );
if($CheckReflect != \'\'){
while( $row = $modx->db->getRow( $CheckReflect ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_Reflect_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_Reflect_version,$minReflectVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_Reflect_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minReflectVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';
		}
	}
} 
//end check Reflect

//check Jot
//get min version from config
$minJotVersion = $JotVersion;
//search the snippet by name
$CheckJot = $modx->db->select( "id, name, description", $table, "name=\'Jot\'" );
if($CheckJot != \'\'){
while( $row = $modx->db->getRow( $CheckJot ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_Jot_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_Jot_version,$minJotVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_Jot_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minJotVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';
		}
	}
} 
//end check Jot
	
//check Multitv
//get min version from config
$minMtvVersion = $MtvVersion;
//search the snippet by name
$CheckMtv = $modx->db->select( "id, name, description", $table, "name=\'multiTV\'" );
if($CheckMtv != \'\'){
while( $row = $modx->db->getRow( $CheckMtv ) ) {
//extract snippet version from description <strong></strong> tags 
$curr_mtv_version = getver($row[\'description\'],"strong");
//check snippet version and return an alert if outdated
if (version_compare($curr_mtv_version,$minMtvVersion,\'lt\')){
$output .= \'<div class="widget-wrapper alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang["snippet"].\' (version \' . $curr_mtv_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang["to_latest"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minMtvVersion.\') \'.$_oec_lang[\'from\'].\' <a target="main" href="index.php?a=112&id=\'.$ExtrasID.\'">\'.$_oec_lang[\'extras_module\'].\'</a></div>\';
		}
	}
} 
//end check Multitv

if($output != \'\'){
if($e->name == \'OnManagerWelcomeHome\') {
$out = $output;
$wdgTitle = \'EVO \'.$EVOversion.\' - \'.$_oec_lang[\'title\'].\'\';
$widgets[\'xtraCheck\'] = array(
				\'menuindex\' =>\'0\',
				\'id\' => \'xtraCheck\'.$pluginid.\'\',
				\'cols\' => \'col-md-12\',
                \'headAttr\' => \'style="background-color:#B60205; color:#FFFFFF;"\',
				\'bodyAttr\' => \'style="background-color:#FFFFFF; color:#24292E;"\',
				\'icon\' => \'fa-warning\',
				\'title\' => \'\'.$wdgTitle.\' \'.$button_pl_config.\'\',
				\'body\' => \'<div class="card-body">\'.$out.\'</div>\',
				\'hide\' => \'0\'
			);	
            $e->output(serialize($widgets));
return;
		}
	}
}';$p['OutdatedExtrasCheckProps']='{"wdgVisibility":"All","DittoVersion":"2.1.3","EformVersion":"1.4.9","AjaxSearchVersion":"1.11.0","WayfinderVersion":"2.0.5","WebLoginVersion":"1.2","WebSignupVersion":"1.1.2","WebChangePwdVersion":"1.1.2","BreadcrumbsVersion":"1.0.5","ReflectVersion":"2.2","JotVersion":"1.1.5","MtvVersion":"2.0.13","badthemes":"MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle"}';$p['TinyMCE4']='/**
 * TinyMCE4
 *
 * Javascript rich text editor
 *
 * @category    plugin
 * @version     4.7.4
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal    @properties &styleFormats=Custom Style Formats <b>RAW</b><br/><br/><ul><li>leave empty to use below block/inline formats</li><li>allows simple-format: <i>Title,cssClass|Title2,cssClass2</i></li><li>Also accepts full JSON-config as per TinyMCE4 docs / configure / content-formating / style_formats</li></ul>;textarea; &styleFormats_inline=Custom Style Formats <b>INLINE</b><br/><br/><ul><li>will wrap selected text with span-tag + css-class</li><li>simple-format only</li></ul>;textarea;InlineTitle,cssClass1|InlineTitle2,cssClass2 &styleFormats_block=Custom Style Formats <b>BLOCK</b><br/><br/><ul><li>will add css-class to selected block-element</li><li>simple-format only</li></ul>;textarea;BlockTitle,cssClass3|BlockTitle2,cssClass4 &customParams=Custom Parameters<br/><b>(Be careful or leave empty!)</b>;textarea; &entityEncoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &pathOptions=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &resizing=Advanced Resizing;list;true,false;false &disabledButtons=Disabled Buttons;text; &webTheme=Web Theme;test;webuser &webPlugins=Web Plugins;text; &webButtons1=Web Buttons 1;text;bold italic underline strikethrough removeformat alignleft aligncenter alignright &webButtons2=Web Buttons 2;text;link unlink image undo redo &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;400px &introtextRte=<b>Introtext RTE</b><br/>add richtext-features to "introtext";list;enabled,disabled;disabled &inlineMode=<b>Inline-Mode</b>;list;enabled,disabled;disabled &inlineTheme=<b>Inline-Mode</b><br/>Theme;text;inline &browser_spellcheck=<b>Browser Spellcheck</b><br/>At least one dictionary must be installed inside your browser;list;enabled,disabled;disabled &paste_as_text=<b>Force Paste as Text</b>;list;enabled,disabled;disabled
 * @internal    @events OnLoadWebDocument,OnParseDocument,OnWebPagePrerender,OnLoadWebPageCache,OnRichTextEditorRegister,OnRichTextEditorInit,OnInterfaceSettingsRender
 * @internal    @modx_category Manager and Admin
 * @internal    @legacy_names TinyMCE4
 * @internal    @installset base
 * @logo        /assets/plugins/tinymce4/tinymce/logo.png
 * @reportissues https://github.com/extras-evolution/tinymce4-for-modx-evo
 * @documentation Plugin docs https://github.com/extras-evolution/tinymce4-for-modx-evo
 * @documentation Official TinyMCE4-docs https://www.tinymce.com/docs/
 * @author      Deesen
 * @lastupdate  2018-01-17
 */
if (!defined(\'MODX_BASE_PATH\')) { die(\'What are you doing? Get out of here!\'); }

require(MODX_BASE_PATH."assets/plugins/tinymce4/plugin.tinymce.inc.php");';$p['TinyMCE4Props']='{"styleFormats_inline":"InlineTitle,cssClass1|InlineTitle2,cssClass2","styleFormats_block":"BlockTitle,cssClass3|BlockTitle2,cssClass4","entityEncoding":"named","pathOptions":"Site config","resizing":"false","webTheme":"webuser","webButtons1":"bold italic underline strikethrough removeformat alignleft aligncenter alignright","webButtons2":"link unlink image undo redo","webAlign":"ltr","width":"100%","height":"400px","introtextRte":"disabled","inlineMode":"disabled","inlineTheme":"inline","browser_spellcheck":"disabled","paste_as_text":"disabled"}';$p['TransAlias']='require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';';$p['TransAliasProps']='{"table_name":"russian","char_restrict":"lowercase alphanumeric","remove_periods":"No","word_separator":"dash"}';$p['Updater']='require MODX_BASE_PATH.\'assets/plugins/updater/plugin.updater.php\';';$p['UpdaterProps']='{"version":"evolution-cms\\/evolution","wdgVisibility":"All","showButton":"AdminOnly","type":"tags","branch":"develop"}';$e=&$this->pluginEvent;$e['OnBeforeDocFormSave']=array('ManagerManager');$e['OnBeforeManagerLogin']=array('Forgot Manager Login');$e['OnBeforePluginFormSave']=array('FileSource');$e['OnBeforeSnipFormSave']=array('FileSource');$e['OnChunkFormRender']=array('CodeMirror');$e['OnDocDuplicate']=array('ManagerManager');$e['OnDocFormPrerender']=array('ManagerManager');$e['OnDocFormRender']=array('ManagerManager','CodeMirror');$e['OnDocFormSave']=array('ManagerManager');$e['OnInterfaceSettingsRender']=array('TinyMCE4');$e['OnLoadWebDocument']=array('TinyMCE4');$e['OnLoadWebPageCache']=array('TinyMCE4');$e['OnManagerAuthentication']=array('Forgot Manager Login');$e['OnManagerLoginFormRender']=array('Forgot Manager Login');$e['OnManagerWelcomeHome']=array('OutdatedExtrasCheck','Updater');$e['OnModFormRender']=array('CodeMirror');$e['OnPageNotFound']=array('Updater');$e['OnParseDocument']=array('TinyMCE4');$e['OnPluginFormPrerender']=array('FileSource');$e['OnPluginFormRender']=array('FileSource','ManagerManager','CodeMirror');$e['OnRichTextEditorInit']=array('TinyMCE4','CodeMirror');$e['OnRichTextEditorRegister']=array('TinyMCE4');$e['OnSiteRefresh']=array('Updater');$e['OnSnipFormPrerender']=array('FileSource');$e['OnSnipFormRender']=array('CodeMirror','FileSource');$e['OnStripAlias']=array('TransAlias');$e['OnTempFormRender']=array('CodeMirror');$e['OnTVFormRender']=array('ManagerManager');$e['OnWebPagePrerender']=array('TinyMCE4');
