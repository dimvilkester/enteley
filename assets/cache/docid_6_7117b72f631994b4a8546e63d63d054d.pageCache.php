<?php die('Unauthorized access.'); ?>a:37:{s:2:"id";s:1:"6";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:16:"Контакты";s:9:"longtitle";s:16:"Контакты";s:11:"description";s:0:"";s:5:"alias";s:7:"contact";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"0";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:0:"";s:8:"richtext";s:1:"1";s:8:"template";s:1:"8";s:9:"menuindex";s:1:"5";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1525166194";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1525170649";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1525166194";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:16:"Контакты";s:7:"donthit";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Enteley | Контакты</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<base href="http://enteley.com/"/>
						
		<link href="http://enteley.com/[~6~]" rel="canonical">
        <link rel="stylesheet" href="/assets/templates/enteley_template/module/normalize.css" />
        <link rel="stylesheet" href="/assets/templates/enteley_template/css/style.css" />
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
		        <div class="wrapper">           
           	 			<header class="header">
                <div class="header__inner inner">
                    <div class="header-logo">
                        <div class="header-logo__image">
                            <a href="index.html" class="header-logo__link">
                                <img src="/assets/templates/enteley_template/img/style/logo.png" alt="Enteley" class="header-logo__img"/>
                            </a>
                        </div>
                    </div>

                    					<nav class="header__menu">
						<ul class="header__menu-inner"><li class="header__menu-item"><a class="header__menu-link" href="/about.html">О компании</a></li><li class="header__menu-item"><a class="header__menu-link" href="/">Каталог товаров</a></li><li class="header__menu-item"><a class="header__menu-link" href="/pravila.html">Памятка покупателю</a></li><li class="header__menu-item"><a class="header__menu-link" href="/oplata-dostavka.html">Оплата и доставка</a></li><li class="header__menu-item"><a class="header__menu-link" href="/stock.html">Акции</a></li><li class="header__menu-item current"><a class="header__menu-link" href="/contact.html">Контакты</a></li></ul>
                    </nav>

                    <div class="header-contact">
                        <div class="header-contact__phone">
                            <p class="header-contact__tel">
                                <a href="tel:+7495327-81-20">+7 (495) 327-81-20</a>
                            </p>
                        </div>

                        <a href="[~6~]#myModal" title="Заказать звонок">
                            <span class="btn header-contact__btn" role="button">
                                <svg class="icon header__icon">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                                Заказать звонок
                            </span>
                        </a>
                    </div>

                    <div class="header-basket">
                        <a href="#" class="header-basket__link">
                            <svg class="icon header-basket__icon">
                                <use xlink:href="#icon-basket"></use>
                            </svg>

                            Ваша корзина
                            <span class="header-basket__sum">5</span>
                        </a>
                        <p class="header-basket__price">
                            на сумму 6860 Р
                        </p>
                    </div>
                </div>
            </header>
			            <main class="main">
                <section class="main__section inner">
                    <header class="main__header">
                        <h1 class="main__title">Контакты</h1>
                    </header>
					
					<p><strong>+7 (495) 327-81-20</strong></p>
                    <p><em>Понедельник-воскресенье с 9.00 до 20.30</em></p>
                    <p>Елена, Ольга (Москва) - менеджеры по работе с клиентами</p>
                    <p>
                        <a class="email" href="mailto:manager-example@domain.com">manager-example@domain.com</a>
                    </p>
                    <p>
                        г. Москва, М. Станкостроительная ул. Станкостроителей, д. 25
                    </p>
                </section>    
            </main>
			            <footer class="footer">
                <div class="footer__inner inner">
                    <div class="footer-copy">
                        <p class="footer-copy__inner">
                            &copy; 2018 &laquoEnteley&raquo <br> Интернет-магазин тканей
                        </p>
                    </div>

                    <div class="footer-contact">
                        <div class="footer-contact__vcard vcard">
                            <div class="footer-contact__phone">
                                <span class="footer-contact__tel tel">
                                    <a href="tel:+7495327-81-20">+7 (495) 327-81-20</a>
                                </span>
                            </div>

                            <div class="footer-contact__adr adr">
                                <span class="footer-contact__locality locality">г. Москва, М. Станкостроительная</span><br>
                                <span class="footer-contact__street-address street-address">ул. Станкостроителей, д. 25</span>
                            </div>

                            <p class="footer-contact__email">
                                <a class="email" href="mailto:example@domain.com">Написать письмо</a>
                            </p>
                        </div>
                    </div>

                    					<nav class="footer__menu">
                        <ul class="footer__menu-inner">
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~2~]">О компании</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~1~]">Каталог товаров</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~3~]">Памятка покупателю</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~4~]">Оплата и доставка</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~5~]">Акции</a>
                            </li>
                            <li class="footer__menu-item">
                                <a class="footer__menu-link" href="[~6~]">Контакты</a>
                            </li>
                        </ul>
                    </nav>					

                    <div class="footer-share">
                        <p class="footer-share__item">
                            Поделись с друзьями
                        </p>

                        <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,twitter" data-counter=""></div>
                    </div>
                </div>
            </footer>
        </div>
        
		<div id="myModal" class="modal">
	<div class="modal__dialog">
		<div class="modal__content">
			<div class="modal__header">
				<h3 class="modal__title">Заказать звонок</h3>
				<a href="[~6~]#close" title="Закрыть" class="modal__close">×</a>
			</div>
			<div class="modal__body">
				<div id="frmwrapper">
					[!FormLister?
						&formid=`contact-form`
						&formMethod=`post`
						&rules=`{
									"name": {
										"required":"Введите имя"
									},
									"phone": {
										"required":"Введите номер телефона",
										"phone":"Введите номер телефона правильно"
									}
								}`
						&protectSubmit=`1`
						&to=`kiddrtest@gmail.com`
						&from=`kiddrtest@gmail.com`
						&errorClass=`error`
						&requiredClass=`error`
						&formTpl=`CONTACT_FORM_TPL`
						&messagesOuterTpl=`CONTACT_FORM_MESSAGE_OUTER_TPL`
						&errorTpl=`CONTACT_FORM_ERROR_TPL`
						&subjectTpl=`CONTACT_FORM_SUBJECT_TPL`
						&reportTpl=`CONTACT_FORM_REPORT_TPL`
						&successTpl=`CONTACT_FORM_SUCCESS_TPL`
					!]
				</div>
			</div>
			<div class="modal__footer">
				<a href="[~6~]#close" title="Закрыть" class="btn modal__close">Закрыть</a>
			</div>
		</div>
	</div>
</div>

		        <div class="hidden">
            <?xml version="1.0" encoding="utf-8"?>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <symbol viewBox="337 -266.1 928.6 786.1" id="icon-basket" xmlns="http://www.w3.org/2000/svg">
                    <path d="M672.8 398c-15.3-12.7-32-19.7-50-21s-34.7 5.7-50 21-22.7 32-22 50 8 34.7 22 50c14 15.3 30.7 22.7 50 22 19.3-.7 36-8 50-22s21-30.7 21-50c1.3-20.6-5.7-37.3-21-50zm500 0c-15.3-12.7-32-19.7-50-21s-34.7 5.7-50 21c-15.3 15.3-22.7 32-22 50 .7 18 8 34.7 22 50 14 15.3 30.7 22.7 50 22 19.3-.7 36-8 50-22s21-30.7 21-50c1.3-20.6-5.7-37.3-21-50zm82-582c-7.3-7.3-15.7-11-25-11h-670c.7-2-.3-6.7-3-14s-3.7-13-3-17-.7-8.7-4-14-6-10-8-14-5.3-7-10-9-10-3-16-3h-143c-8.7-.7-17 2.7-25 10s-11.7 16-11 26c.7 10 4.3 18.3 11 25 6.7 6.7 15 10 25 10h114l98 459c0 2-2.7 7.7-8 17s-9.3 16.7-12 22-5.7 12-9 20-5 14-5 18c0 9.3 3.7 17.7 11 25s15.7 11 25 11h571c11.3 0 20-3.7 26-11s9.3-15.7 10-25c.7-9.3-2.7-17.7-10-25-7.3-7.3-16-11-26-11h-513c8.7-18 13-29.7 13-35 0-4-2.3-17.3-7-40l583-68c8-1.3 15.3-5.3 22-12s10-14.3 10-23v-286c0-9.3-3.7-17.6-11-25z"/>
                </symbol>

                <symbol viewBox="266 -266 786 786" id="icon-phone" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1050.1 343c-2-6.7-16-16.3-42-29-6.7-4-16.7-9.7-30-17s-25.3-14-36-20-20.7-11.7-30-17c-1.3-1.3-6-4.7-14-10s-14.7-9.3-20-12-10.3-4-15-4c-8-.7-17.3 4.7-28 16s-21 23-31 35-20 23.3-30 34-18.3 16-25 16c-4.7.7-9 0-13-2s-7.7-3.7-11-5-8-4-14-8-9.3-6-10-6c-50.7-28-94.3-60.3-131-97s-69-80.7-97-132c0 .7-2.3-2.7-7-10s-7.3-12-8-14-2-5.7-4-11-3-9.7-3-13c.7-6.7 6-15 16-25s21.3-20 34-30 24.3-20.3 35-31 16-19.7 16-27c0-5.3-1.3-10.7-4-16s-6.7-12-12-20-8.7-12.7-10-14c-4.7-10-10.3-20-17-30-6.7-10-13.3-21.7-20-35s-12-23.7-16-31c-13.3-26-23.3-40-30-42-2.7-1.3-6.7-2-12-2-10 0-23 2-39 6s-28.7 8-38 12c-18.7 7.3-38.3 30-59 68-19.3 34.7-29 69.3-29 104-.7 10 0 19.7 2 29s4.3 20 7 32 5.3 20.7 8 26c2.7 5.3 6.7 15.7 12 31s8.7 24.7 10 28c12.7 36 28 68.3 46 97 28.7 48 68.7 97.3 120 148s100.7 90.7 148 120c28.7 18 61 33.7 97 47 4 1.3 13.3 4.7 28 10s25 9 31 11 14.7 4.7 26 8 22 5.7 32 7 20 2 30 2c34 0 68.3-9.3 103-28 38-21.3 60.7-41.3 68-60 4-9.3 8-22 12-38s6-29 6-39c0-5.3-.7-9.3-2-12z"/>
                </symbol>
            </svg>
        </div>

		<script src="http://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="http://yastatic.net/share2/share.js"></script>
    </body>
</html>