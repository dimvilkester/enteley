#
# Enteley Database Dump
# MODX Version:1.4.2
# 
# Host: 
# Generation Time: 06-05-2018 15:13:37
# Server version: 5.7.13
# PHP Version: 7.0.8
# Database: `enteley_bd`
# Description: 
#

# --------------------------------------------------------

#
# Table structure for table `alv1_active_user_locks`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_active_user_locks`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_active_user_locks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `elementType` int(1) NOT NULL DEFAULT '0',
  `elementId` int(10) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_element_id` (`elementType`,`elementId`,`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=554 DEFAULT CHARSET=utf8 COMMENT='Contains data about locked elements.';

#
# Dumping data for table `alv1_active_user_locks`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_active_user_sessions`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_active_user_sessions`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_active_user_sessions` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data about valid user sessions.';

#
# Dumping data for table `alv1_active_user_sessions`
#

INSERT INTO `alv1_active_user_sessions` VALUES ('929v4gothkponodetv0nhc3o14','1','1525608817','127.0.0.1');


# --------------------------------------------------------

#
# Table structure for table `alv1_active_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_active_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_active_users` (
  `sid` varchar(32) NOT NULL DEFAULT '',
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `action` varchar(10) NOT NULL DEFAULT '',
  `id` int(10) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data about last user action.';

#
# Dumping data for table `alv1_active_users`
#

INSERT INTO `alv1_active_users` VALUES ('c6j3iq89ie38kutq78ntj436d4','1','admin','1525449375','93',NULL);

INSERT INTO `alv1_active_users` VALUES ('a75ggn0pttkijkrbbo8sr8hlm0n5glnm','1','admin','1525430855','93',NULL);

INSERT INTO `alv1_active_users` VALUES ('929v4gothkponodetv0nhc3o14','1','admin','1525608817','93',NULL);


# --------------------------------------------------------

#
# Table structure for table `alv1_categories`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_categories`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL DEFAULT '',
  `rank` int(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Categories to be used snippets,tv,chunks, etc';

#
# Dumping data for table `alv1_categories`
#

INSERT INTO `alv1_categories` VALUES ('1','SEO','0');

INSERT INTO `alv1_categories` VALUES ('2','Templates','0');

INSERT INTO `alv1_categories` VALUES ('3','Js','0');

INSERT INTO `alv1_categories` VALUES ('4','Manager and Admin','0');

INSERT INTO `alv1_categories` VALUES ('5','Content','0');

INSERT INTO `alv1_categories` VALUES ('6','Navigation','0');

INSERT INTO `alv1_categories` VALUES ('7','Блоки сайта','0');

INSERT INTO `alv1_categories` VALUES ('8','Forms','0');

INSERT INTO `alv1_categories` VALUES ('9','Список товаров','0');

INSERT INTO `alv1_categories` VALUES ('10','Подготовка','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_document_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_document_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_document_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_dg_id` (`document_group`,`document`),
  KEY `document` (`document`),
  KEY `document_group` (`document_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `alv1_document_groups`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_documentgroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_documentgroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_documentgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `alv1_documentgroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_event_log`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_event_log`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_event_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventid` int(11) DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- information, 2 - warning, 3- error',
  `user` int(11) NOT NULL DEFAULT '0' COMMENT 'link to user table',
  `usertype` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - manager, 1 - web',
  `source` varchar(50) NOT NULL DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='Stores event and error logs';

#
# Dumping data for table `alv1_event_log`
#

INSERT INTO `alv1_event_log` VALUES ('17','0','1525331580','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/about.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[2] <a href=\"http://enteley.com/about.html\" target=\"_blank\">О компании</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/about.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:13:00</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (6 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0315 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0425 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6034317016602 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 444</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('18','0','1525331586','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/about.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[2] <a href=\"http://enteley.com/about.html\" target=\"_blank\">О компании</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/pravila.htmlg</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:13:06</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0100 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0348 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0448 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6055221557617 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('19','0','1525331593','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:13:13</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0030 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0272 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0302 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('20','0','1525331603','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:13:23</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0417 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0527 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.607551574707 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('21','0','1525331605','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:13:25</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0459 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0569 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('22','0','1525331794','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:16:34</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0297 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0407 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6050415039062 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 446</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('23','0','1525331801','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:16:41</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0391 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0501 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('24','0','1525331815','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/manager/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:16:55</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0020 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0199 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0219 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073684692383 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('25','0','1525331819','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/manager/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:16:59</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0332 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0442 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6072006225586 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('26','0','1525331826','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:17:06</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0415 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0425 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('27','0','1525331828','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:17:08</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0140 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0266 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0406 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('28','0','1525333043','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:37:23</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0060 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0321 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0381 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('29','0','1525333368','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:42:48</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0060 s (5 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0367 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0427 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.605110168457 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('30','0','1525333481','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:44:41</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0020 s (5 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0332 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0352 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.605110168457 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('31','0','1525334145','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:55:45</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0020 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0242 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0262 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073684692383 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('32','0','1525334148','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:55:48</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0223 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0233 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073684692383 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('33','0','1525334151','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[3] <a href=\"http://enteley.com/pravila.html\" target=\"_blank\">Памятка покупателю</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:55:51</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0355 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0365 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6055526733398 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('34','0','1525334154','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:55:54</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0020 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0237 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0257 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.607551574707 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('35','0','1525334202','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:56:42</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0384 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0394 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6073837280273 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('36','0','1525334205','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 10:56:45</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0296 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0306 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.607551574707 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('37','0','1525337164','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:46:04</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0550 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.2098 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.2648 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.607551574707 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('38','0','1525337244','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:47:24</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0300 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.1484 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.1784 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6100997924805 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('39','0','1525337452','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:50:52</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0210 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0373 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0583 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>3.6100997924805 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('40','0','1525337590','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:53:10</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0290 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0562 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0852 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6099319458008 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('41','0','1525337593','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:53:13</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0210 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.2954 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.3164 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6099319458008 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('42','0','1525337597','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/manager/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:53:17</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0310 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0286 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0596 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6099166870117 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('43','0','1525337636','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:53:56</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0330 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0358 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0688 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>3.6130065917969 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('44','0','1525337869','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/manager/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 11:57:49</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0040 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0222 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0262 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6099166870117 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('45','0','1525345971','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[3] <a href=\"http://enteley.com/pravila.html\" target=\"_blank\">Памятка покупателю</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:12:51</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0467 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0577 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6060333251953 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('46','0','1525345977','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[3] <a href=\"http://enteley.com/pravila.html\" target=\"_blank\">Памятка покупателю</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:12:57</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0300 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0310 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6060333251953 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('47','0','1525346018','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[3] <a href=\"http://enteley.com/pravila.html\" target=\"_blank\">Памятка покупателю</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:13:38</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0304 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0414 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6084365844727 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('48','0','1525346025','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:13:45</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0298 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0408 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6099319458008 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('49','0','1525346045','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:14:05</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (5 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.1039 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.1149 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6074905395508 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('50','0','1525346077','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:14:37</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (5 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0218 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0328 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6074905395508 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('51','0','1525346312','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:18:32</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0110 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0372 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0482 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6099319458008 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('52','0','1525346338','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:18:58</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0010 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0387 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0397 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6100997924805 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('53','0','1525346341','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:19:01</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0100 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0468 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0568 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>3.6099319458008 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('54','0','1525346452','3','0','1','Snippet - eForm / PHP Parse Error','<b>Trying to get property \'action\' of non-object</b><br />\n<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">Error : count(): Parameter must be an array or an object that implements Countable</div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>ErrorType[num]</td>\n		<td>WARNING[2]</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>File</td>\n		<td>C:\\OpenServer\\domains\\enteley.com\\assets\\snippets\\eform\\eform.inc.php</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Line</td>\n		<td>707</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Source</td>\n		<td>	$lastitems[count($lastitems)] = &quot;class=\\&quot;\\&quot;&quot;; //removal off empty class attributes\n</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>eForm</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td></td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-03 14:20:52</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0050 s (2 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0262 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0312 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>3.6100997924805 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eForm</strong>(DocumentParser $var1, array $var2)<br />assets/snippets/eform/snippet.eform.php on line 114</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>formMerge</strong>(string $var1, array $var2)<br />assets/snippets/eform/eform.inc.php on line 658</td>\n	</tr>\n</table>\n');

INSERT INTO `alv1_event_log` VALUES ('55','0','1525447120','3','1','0','Системные файлы были изменены.','Вы включили проверку системных файлов на наличие изменений, характерных для взломанных сайтов. Это не значит, что сайт был взломан, но желательно просмотреть измененные файлы.(index.php, .htaccess, manager/index.php, manager/includes/config.inc.php) index.php, .htaccess, manager/index.php, manager/includes/config.inc.php');

INSERT INTO `alv1_event_log` VALUES ('56','0','1525447120','3','1','0','Файл конфигурации все еще доступен для записи','Злоумышленники потенциально могут нанести вред вашему сайту. <strong>Серьёзно.</strong> Пожалуйста, установите права доступа к файлу конфигурации (/manager/includes/config.inc.php) в режим \'Только для чтения\'');

INSERT INTO `alv1_event_log` VALUES ('57','0','1525448035','3','0','1','Snippet - DocLister / Execution of a query to the ','<h2 style=\"color:red\">&laquo; Evo Parse Error &raquo;</h2><h3 style=\"color:red\">Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'ASC LIMIT 0,6\' at line 1</h3><div style=\"font-weight:bold;border:1px solid #ccc;padding:8px;color:#333;background-color:#ffffcd;margin-bottom:15px;\">SQL &gt; <span id=\"sqlHolder\">SELECT * FROM `enteley_bd`.`alv1_products` as `c`    ORDER BY ASC LIMIT 0,6</span></div>\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Error information</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>Current Snippet</td>\n		<td>DocLister</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Basic info</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>REQUEST_URI</td>\n		<td>http://enteley.com/</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Resource</td>\n		<td>[1] <a href=\"http://enteley.com/\" target=\"_blank\">Каталог товаров</a></td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Referer</td>\n		<td>http://enteley.com/pravila.html</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>User Agent</td>\n		<td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>IP</td>\n		<td>127.0.0.1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Current time</td>\n		<td>2018-05-04 18:33:55</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th width=\"100px\" >Benchmarks</th>\n		<th></th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td>MySQL</td>\n		<td>0.0120 s (7 Requests)</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>PHP</td>\n		<td>0.0628 s</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td>Total</td>\n		<td>0.0748 s</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td>Memory</td>\n		<td>1.6465301513672 mb</td>\n	</tr>\n</table>\n<br />\n<table class=\"grid\">\n	<thead>\n	<tr class=\"\">\n		<th>Backtrace</th>\n	</tr>\n	</thead>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->executeParser</strong>()<br />index.php on line 139</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->prepareResponse</strong>()<br />manager/includes/document.parser.class.inc.php on line 2828</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->outputContent</strong>()<br />manager/includes/document.parser.class.inc.php on line 2952</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->parseDocumentSource</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 897</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippets</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 2679</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocumentParser->_get_snip_result</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php on line 1989</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DocumentParser->evalSnippet</strong>(string $var1, array $var2)<br />manager/includes/document.parser.class.inc.php on line 2077</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>eval</strong>()<br />manager/includes/document.parser.class.inc.php on line 1926</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>require</strong>(string $var1)<br />manager/includes/document.parser.class.inc.php(1926) : eval()\'d code on line 1</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>onetableDocLister->getDocs</strong>()<br />assets/snippets/DocLister/snippet.DocLister.php on line 35</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>onetableDocLister->getDocList</strong>()<br />assets/snippets/DocLister/core/controller/onetable.php on line 38</td>\n	</tr>\n	<tr class=\"gridAltItem\">\n		<td><strong>DocLister->dbQuery</strong>(string $var1)<br />assets/snippets/DocLister/core/controller/onetable.php on line 249</td>\n	</tr>\n	<tr class=\"gridItem\">\n		<td><strong>DBAPI->query</strong>(string $var1)<br />assets/snippets/DocLister/core/DocLister.abstract.php on line 1801</td>\n	</tr>\n</table>\n');


# --------------------------------------------------------

#
# Table structure for table `alv1_fashion`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_fashion`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_fashion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Dumping data for table `alv1_fashion`
#

INSERT INTO `alv1_fashion` VALUES ('1','slide-1.jpg');

INSERT INTO `alv1_fashion` VALUES ('2','slide-2.jpg');

INSERT INTO `alv1_fashion` VALUES ('3','slide-3.jpg');


# --------------------------------------------------------

#
# Table structure for table `alv1_manager_log`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_manager_log`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_manager_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` int(20) NOT NULL DEFAULT '0',
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `username` varchar(255) DEFAULT NULL,
  `action` int(10) NOT NULL DEFAULT '0',
  `itemid` varchar(10) DEFAULT '0',
  `itemname` varchar(255) DEFAULT NULL,
  `message` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1222 DEFAULT CHARSET=utf8 COMMENT='Contains a record of user interaction.';

#
# Dumping data for table `alv1_manager_log`
#

INSERT INTO `alv1_manager_log` VALUES ('1','1525087609','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('2','1525087609','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('3','1525087704','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('4','1525087710','1','admin','27','1','Evolution CMS Install Success','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('5','1525090342','1','admin','3','1','Evolution CMS Install Success','Viewing data for resource');

INSERT INTO `alv1_manager_log` VALUES ('6','1525090365','1','admin','27','1','Evolution CMS Install Success','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('7','1525090505','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('8','1525090505','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('9','1525090514','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('10','1525090514','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('11','1525090526','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('12','1525090547','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('13','1525090551','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('14','1525090598','1','admin','19','-','Новый шаблон','Creating a new template');

INSERT INTO `alv1_manager_log` VALUES ('15','1525090967','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('16','1525090976','1','admin','16','3','Minimal Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('17','1525091823','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('18','1525091823','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('19','1525091829','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('20','1525091940','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('21','1525091940','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('22','1525091995','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('23','1525091995','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('24','1525092002','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('25','1525092002','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('26','1525092052','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('27','1525092052','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('28','1525092104','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('29','1525092106','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('30','1525092106','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('31','1525092113','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('32','1525092117','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('33','1525092117','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('34','1525092129','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('35','1525092130','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('36','1525092144','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('37','1525092144','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('38','1525162643','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('39','1525162656','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('40','1525162886','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('41','1525162886','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('42','1525162894','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('43','1525162977','1','admin','79','-','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('44','1525162977','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('45','1525163016','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('46','1525163077','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('47','1525163077','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('48','1525163110','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('49','1525163122','1','admin','78','1','header','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('50','1525163168','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('51','1525163168','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('52','1525163230','1','admin','78','2','mm_rules','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('53','1525163254','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('54','1525163280','1','admin','79','-','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('55','1525163281','1','admin','78','4','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('56','1525163342','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('57','1525163419','1','admin','79','-','MAIN','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('58','1525163419','1','admin','78','5','MAIN','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('59','1525163449','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('60','1525163595','1','admin','53','-','-','Viewing system info');

INSERT INTO `alv1_manager_log` VALUES ('61','1525163621','1','admin','13','-','-','Viewing logging');

INSERT INTO `alv1_manager_log` VALUES ('62','1525163631','1','admin','114','-','-','View event log');

INSERT INTO `alv1_manager_log` VALUES ('63','1525163635','1','admin','70','-','-','Viewing site schedule');

INSERT INTO `alv1_manager_log` VALUES ('64','1525163689','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('65','1525163743','1','admin','79','-','WRAPPER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('66','1525163743','1','admin','78','6','WRAPPER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('67','1525163747','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('68','1525163747','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('69','1525163785','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('70','1525163933','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('71','1525163986','1','admin','79','-','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('72','1525163986','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('73','1525163989','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('74','1525163990','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('75','1525164023','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('76','1525164045','1','admin','79','-','ICONS','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('77','1525164045','1','admin','78','8','ICONS','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('78','1525164049','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('79','1525164049','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('80','1525164056','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('81','1525164056','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('82','1525164159','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('83','1525164253','1','admin','79','-','MAIN_SCRIPT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('84','1525164253','1','admin','78','9','MAIN_SCRIPT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('85','1525164257','1','admin','20','3','Enteley Template','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('86','1525164257','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('87','1525164275','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('88','1525165784','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('89','1525165848','1','admin','5','-','О компании','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('90','1525165848','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('91','1525165851','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('92','1525165855','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('93','1525165855','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('94','1525165899','1','admin','94','1','Копия Каталог товаров','Duplicate resource');

INSERT INTO `alv1_manager_log` VALUES ('95','1525165899','1','admin','3','3','Копия Каталог товаров','Viewing data for resource');

INSERT INTO `alv1_manager_log` VALUES ('96','1525165900','1','admin','27','3','Копия Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('97','1525165964','1','admin','5','3','Памятка покупателю','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('98','1525165964','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('99','1525165992','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('100','1525166060','1','admin','5','-','Оплата и доставка','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('101','1525166060','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('102','1525166061','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('103','1525166079','1','admin','5','1','Каталог товаров','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('104','1525166079','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('105','1525166087','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('106','1525166108','1','admin','5','3','Памятка покупателю','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('107','1525166108','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('108','1525166112','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('109','1525166115','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('110','1525166118','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('111','1525166123','1','admin','5','2','О компании','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('112','1525166123','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('113','1525166127','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('114','1525166161','1','admin','5','-','Акции','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('115','1525166161','1','admin','27','5','Акции','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('116','1525166166','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('117','1525166194','1','admin','5','-','Контакты','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('118','1525166194','1','admin','27','6','Контакты','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('119','1525166197','1','admin','27','5','Акции','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('120','1525166199','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('121','1525166201','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('122','1525166202','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('123','1525166203','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('124','1525166314','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('125','1525166391','1','admin','5','-','404 - Документ не найден','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('126','1525166391','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('127','1525166538','1','admin','5','7','404 - Документ не найден','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('128','1525166538','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('129','1525166550','1','admin','3','7','404 - Документ не найден','Viewing data for resource');

INSERT INTO `alv1_manager_log` VALUES ('130','1525166560','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('131','1525166634','1','admin','27','1','Каталог товаров','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('132','1525166649','1','admin','16','3','Enteley Template','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('133','1525166777','1','admin','78','5','MAIN','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('134','1525166922','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('135','1525167010','1','admin','79','-','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('136','1525167010','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('137','1525167217','1','admin','79','5','MAIN','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('138','1525167217','1','admin','78','5','MAIN','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('139','1525167250','1','admin','78','6','WRAPPER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('140','1525167317','1','admin','20','3','Каталог товаров','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('141','1525167317','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('142','1525167331','1','admin','78','5','MAIN','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('143','1525167413','1','admin','79','5','MAIN_INDEX','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('144','1525167413','1','admin','78','5','MAIN_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('145','1525167442','1','admin','78','6','WRAPPER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('146','1525167468','1','admin','79','6','WRAPPER_INDEX','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('147','1525167468','1','admin','78','6','WRAPPER_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('148','1525167491','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('149','1525167499','1','admin','20','3','Каталог товаров','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('150','1525167499','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('151','1525167520','1','admin','20','3','Каталог товаров','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('152','1525167520','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('153','1525167531','1','admin','19','-','Новый шаблон','Creating a new template');

INSERT INTO `alv1_manager_log` VALUES ('154','1525167565','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('155','1525167596','1','admin','20','-','О компании','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('156','1525167596','1','admin','16','4','О компании','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('157','1525167609','1','admin','78','6','WRAPPER_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('158','1525167612','1','admin','97','6','WRAPPER_INDEX Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('159','1525167612','1','admin','78','11','WRAPPER_INDEX Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('160','1525167655','1','admin','79','11','WRAPPER_ABOUT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('161','1525167655','1','admin','78','11','WRAPPER_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('162','1525167667','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('163','1525167675','1','admin','78','5','MAIN_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('164','1525167696','1','admin','79','5','MAIN_INDEX','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('165','1525167696','1','admin','78','5','MAIN_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('166','1525167754','1','admin','79','-','MAIN_ABOUT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('167','1525167754','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('168','1525167891','1','admin','79','12','MAIN_ABOUT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('169','1525167891','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('170','1525167899','1','admin','20','4','О компании','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('171','1525167899','1','admin','16','4','О компании','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('172','1525167914','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('173','1525167963','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('174','1525168009','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('175','1525168015','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('176','1525168017','1','admin','5','7','404 - Документ не найден','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('177','1525168017','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('178','1525168056','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('179','1525168059','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('180','1525168061','1','admin','5','2','О компании','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('181','1525168061','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('182','1525168236','1','admin','16','4','О компании','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('183','1525168240','1','admin','96','4','О компании Копия','Duplicate Template');

INSERT INTO `alv1_manager_log` VALUES ('184','1525168240','1','admin','16','5','О компании Копия','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('185','1525168305','1','admin','20','5','Памятка покупателю','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('186','1525168305','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('187','1525168314','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('188','1525168318','1','admin','97','12','MAIN_ABOUT Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('189','1525168318','1','admin','78','13','MAIN_ABOUT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('190','1525168327','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('191','1525168330','1','admin','97','12','MAIN_ABOUT Копия 2','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('192','1525168330','1','admin','78','14','MAIN_ABOUT Копия 2','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('193','1525168414','1','admin','79','14','WRAPPER_PRAVILA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('194','1525168414','1','admin','78','14','WRAPPER_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('195','1525168420','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('196','1525168423','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('197','1525168427','1','admin','5','3','Памятка покупателю','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('198','1525168427','1','admin','27','3','Памятка покупателю','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('199','1525168434','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('200','1525168457','1','admin','78','13','MAIN_ABOUT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('201','1525168553','1','admin','79','13','MAIN_PRAVILA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('202','1525168553','1','admin','78','13','MAIN_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('203','1525168599','1','admin','78','6','WRAPPER_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('204','1525168642','1','admin','79','14','WRAPPER_PRAVILA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('205','1525168642','1','admin','78','14','WRAPPER_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('206','1525169668','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('207','1525169671','1','admin','96','5','Памятка покупателю Копия','Duplicate Template');

INSERT INTO `alv1_manager_log` VALUES ('208','1525169671','1','admin','16','6','Памятка покупателю Копия','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('209','1525169734','1','admin','20','6','Оплата и доставка','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('210','1525169734','1','admin','16','6','Оплата и доставка','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('211','1525169741','1','admin','78','14','WRAPPER_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('212','1525169744','1','admin','97','14','WRAPPER_PRAVILA Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('213','1525169744','1','admin','78','15','WRAPPER_PRAVILA Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('214','1525169780','1','admin','79','15','WRAPPER_OPLATA_DOSTAVKA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('215','1525169780','1','admin','78','15','WRAPPER_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('216','1525169784','1','admin','78','13','MAIN_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('217','1525169788','1','admin','79','15','WRAPPER_OPLATA_DOSTAVKA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('218','1525169788','1','admin','78','15','WRAPPER_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('219','1525169859','1','admin','79','13','MAIN_OPLATA_DOSTAVKA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('220','1525169859','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('221','1525169885','1','admin','79','15','WRAPPER_OPLATA_DOSTAVKA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('222','1525169885','1','admin','78','15','WRAPPER_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('223','1525169890','1','admin','20','6','Оплата и доставка','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('224','1525169890','1','admin','16','6','Оплата и доставка','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('225','1525169895','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('226','1525169901','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('227','1525169903','1','admin','5','4','Оплата и доставка','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('228','1525169903','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('229','1525169941','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('230','1525169955','1','admin','97','13','MAIN_OPLATA_DOSTAVKA Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('231','1525169955','1','admin','78','16','MAIN_OPLATA_DOSTAVKA Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('232','1525169981','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('233','1525169998','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('234','1525170004','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('235','1525170010','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('236','1525170013','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('237','1525170032','1','admin','27','4','Оплата и доставка','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('238','1525170044','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('239','1525170050','1','admin','16','6','Оплата и доставка','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('240','1525170095','1','admin','78','16','MAIN_OPLATA_DOSTAVKA Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('241','1525170182','1','admin','79','16','MAIN_PRAVILA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('242','1525170182','1','admin','78','16','MAIN_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('243','1525170200','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('244','1525170217','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('245','1525170229','1','admin','79','12','MAIN_ABOUT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('246','1525170229','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('247','1525170255','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('248','1525170263','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('249','1525170266','1','admin','96','5','Памятка покупателю Копия','Duplicate Template');

INSERT INTO `alv1_manager_log` VALUES ('250','1525170266','1','admin','16','7','Памятка покупателю Копия','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('251','1525170301','1','admin','20','7','Акции','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('252','1525170301','1','admin','16','7','Акции','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('253','1525170310','1','admin','27','5','Акции','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('254','1525170312','1','admin','27','5','Акции','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('255','1525170314','1','admin','5','5','Акции','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('256','1525170314','1','admin','27','5','Акции','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('257','1525170320','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('258','1525170334','1','admin','78','11','WRAPPER_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('259','1525170337','1','admin','97','11','WRAPPER_ABOUT Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('260','1525170337','1','admin','78','17','WRAPPER_ABOUT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('261','1525170358','1','admin','79','17','WRAPPER_STOCK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('262','1525170359','1','admin','78','17','WRAPPER_STOCK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('263','1525170363','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('264','1525170374','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('265','1525170376','1','admin','97','12','MAIN_ABOUT Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('266','1525170376','1','admin','78','18','MAIN_ABOUT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('267','1525170428','1','admin','79','18','MAIN_STOCK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('268','1525170428','1','admin','78','18','MAIN_STOCK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('269','1525170441','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('270','1525170451','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('271','1525170454','1','admin','96','5','Памятка покупателю Копия','Duplicate Template');

INSERT INTO `alv1_manager_log` VALUES ('272','1525170454','1','admin','16','8','Памятка покупателю Копия','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('273','1525170481','1','admin','20','8','Контакты','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('274','1525170481','1','admin','16','8','Контакты','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('275','1525170490','1','admin','78','11','WRAPPER_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('276','1525170494','1','admin','97','11','WRAPPER_ABOUT Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('277','1525170494','1','admin','78','19','WRAPPER_ABOUT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('278','1525170524','1','admin','79','19','WRAPPER_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('279','1525170524','1','admin','78','19','WRAPPER_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('280','1525170528','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('281','1525170533','1','admin','78','18','MAIN_STOCK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('282','1525170536','1','admin','97','18','MAIN_STOCK Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('283','1525170536','1','admin','78','20','MAIN_STOCK Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('284','1525170546','1','admin','78','20','MAIN_STOCK Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('285','1525170628','1','admin','79','20','MAIN_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('286','1525170628','1','admin','78','20','MAIN_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('287','1525170632','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('288','1525170645','1','admin','27','6','Контакты','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('289','1525170648','1','admin','27','6','Контакты','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('290','1525170649','1','admin','5','6','Контакты','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('291','1525170649','1','admin','27','6','Контакты','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('292','1525170683','1','admin','27','2','О компании','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('293','1525170691','1','admin','19','-','Новый шаблон','Creating a new template');

INSERT INTO `alv1_manager_log` VALUES ('294','1525170721','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('295','1525170778','1','admin','16','7','Акции','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('296','1525170819','1','admin','78','11','WRAPPER_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('297','1525170849','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('298','1525170940','1','admin','20','-','404 - Документ не найден','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('299','1525170940','1','admin','16','9','404 - Документ не найден','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('300','1525170952','1','admin','5','7','404 - Документ не найден','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('301','1525170952','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('302','1525170956','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('303','1525170957','1','admin','5','7','404 - Документ не найден','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('304','1525170957','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('305','1525171043','1','admin','20','9','404 - Документ не найден','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('306','1525171043','1','admin','16','9','404 - Документ не найден','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('307','1525171172','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('308','1525171208','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('309','1525171228','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('310','1525171228','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('311','1525171245','1','admin','79','-','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('312','1525171245','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('313','1525171312','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('314','1525171312','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('315','1525171334','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('316','1525171335','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('317','1525171353','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('318','1525171353','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('319','1525171369','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('320','1525171369','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('321','1525252840','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('322','1525253054','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('323','1525253054','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('324','1525253082','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('325','1525253082','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('326','1525253169','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('327','1525253169','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('328','1525253189','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('329','1525253189','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('330','1525253213','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('331','1525253213','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('332','1525253282','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('333','1525253282','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('334','1525253649','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('335','1525253649','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('336','1525253690','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('337','1525253690','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('338','1525253762','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('339','1525253762','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('340','1525253786','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('341','1525253804','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('342','1525253804','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('343','1525253859','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('344','1525253867','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('345','1525253867','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('346','1525253871','1','admin','79','-','Untitled chunk','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('347','1525253871','1','admin','78','22','Untitled chunk','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('348','1525253882','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('349','1525254080','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('350','1525254080','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('351','1525254165','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('352','1525254165','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('353','1525254255','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('354','1525254255','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('355','1525254322','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('356','1525254322','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('357','1525254341','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('358','1525254341','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('359','1525254380','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('360','1525254380','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('361','1525254479','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('362','1525254479','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('363','1525254539','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('364','1525254539','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('365','1525254621','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('366','1525254621','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('367','1525254666','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('368','1525254666','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('369','1525254717','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('370','1525254717','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('371','1525254815','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('372','1525254815','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('373','1525254838','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('374','1525254838','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('375','1525254869','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('376','1525254869','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('377','1525254939','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('378','1525254939','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('379','1525254987','1','admin','22','2','DLMenu','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('380','1525255728','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('381','1525255728','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('382','1525255756','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('383','1525255756','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('384','1525255785','1','admin','78','11','WRAPPER_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('385','1525255789','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('386','1525255795','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('387','1525255807','1','admin','79','12','MAIN_ABOUT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('388','1525255807','1','admin','78','12','MAIN_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('389','1525255812','1','admin','78','20','MAIN_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('390','1525255818','1','admin','79','20','MAIN_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('391','1525255818','1','admin','78','20','MAIN_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('392','1525255826','1','admin','78','5','MAIN_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('393','1525255833','1','admin','79','5','MAIN_INDEX','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('394','1525255833','1','admin','78','5','MAIN_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('395','1525255843','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('396','1525255849','1','admin','79','13','MAIN_OPLATA_DOSTAVKA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('397','1525255849','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('398','1525255858','1','admin','78','16','MAIN_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('399','1525255864','1','admin','79','16','MAIN_PRAVILA','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('400','1525255864','1','admin','78','16','MAIN_PRAVILA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('401','1525255872','1','admin','78','18','MAIN_STOCK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('402','1525255876','1','admin','79','18','MAIN_STOCK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('403','1525255876','1','admin','78','18','MAIN_STOCK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('404','1525255915','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('405','1525255915','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('406','1525255941','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('407','1525255941','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('408','1525256013','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('409','1525256013','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('410','1525256022','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('411','1525256022','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('412','1525256161','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('413','1525256161','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('414','1525256361','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('415','1525256361','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('416','1525256399','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('417','1525256399','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('418','1525256450','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('419','1525256450','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('420','1525257525','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('421','1525257525','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('422','1525257556','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('423','1525257556','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('424','1525257581','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('425','1525257582','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('426','1525257589','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('427','1525257589','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('428','1525258327','1','admin','22','2','DLMenu','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('429','1525258392','1','admin','23','-','Новый сниппет','Creating a new Snippet');

INSERT INTO `alv1_manager_log` VALUES ('430','1525264622','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('431','1525264622','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('432','1525268018','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('433','1525268018','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('434','1525268021','1','admin','97','21','MENU_HEADER Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('435','1525268021','1','admin','78','23','MENU_HEADER Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('436','1525268167','1','admin','78','4','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('437','1525268192','1','admin','79','4','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('438','1525268192','1','admin','78','4','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('439','1525268271','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('440','1525268271','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('441','1525268408','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('442','1525268408','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('443','1525268430','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('444','1525268430','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('445','1525268446','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('446','1525268446','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('447','1525268589','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('448','1525268589','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('449','1525268654','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('450','1525268654','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('451','1525268684','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('452','1525268684','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('453','1525268695','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('454','1525268695','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('455','1525268699','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('456','1525268699','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('457','1525268788','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('458','1525268788','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('459','1525268797','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('460','1525268797','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('461','1525268879','1','admin','79','4','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('462','1525268879','1','admin','78','4','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('463','1525268894','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('464','1525268894','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('465','1525268915','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('466','1525268915','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('467','1525268969','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('468','1525268969','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('469','1525268992','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('470','1525268992','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('471','1525269031','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('472','1525269031','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('473','1525269048','1','admin','79','23','MENU_FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('474','1525269048','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('475','1525269059','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('476','1525269130','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('477','1525269130','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('478','1525269185','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('479','1525269185','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('480','1525269246','1','admin','79','21','MENU_HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('481','1525269246','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('482','1525269379','1','admin','78','22','Untitled chunk','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('483','1525269383','1','admin','80','22','Untitled chunk','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('484','1525269383','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('485','1525269389','1','admin','27','7','404 - Документ не найден','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('486','1525269421','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('487','1525269631','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('488','1525269631','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('489','1525269673','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('490','1525269673','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('491','1525269698','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('492','1525269707','1','admin','78','23','MENU_FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('493','1525269722','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('494','1525269722','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('495','1525269751','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('496','1525269751','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('497','1525269782','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('498','1525269783','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('499','1525269928','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('500','1525269928','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('501','1525270343','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('502','1525270343','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('503','1525270347','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('504','1525270412','1','admin','79','-','MODAL_FORM','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('505','1525270412','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('506','1525270702','1','admin','22','6','FormLister','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('507','1525271686','1','admin','23','-','Новый сниппет','Creating a new Snippet');

INSERT INTO `alv1_manager_log` VALUES ('508','1525271729','1','admin','22','2','DLMenu','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('509','1525272115','1','admin','24','-','eForm','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('510','1525272115','1','admin','22','10','eForm','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('511','1525272145','1','admin','24','10','eForm','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('512','1525272145','1','admin','22','10','eForm','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('513','1525272314','1','admin','78','21','MENU_HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('514','1525272682','1','admin','79','24','MODAL_FORM','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('515','1525272682','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('516','1525272699','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('517','1525272826','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('518','1525272890','1','admin','79','-','FORM_CALL_REPORT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('519','1525272890','1','admin','78','25','FORM_CALL_REPORT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('520','1525273727','1','admin','79','24','MODAL_FORM','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('521','1525273727','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('522','1525273761','1','admin','79','24','MODAL_FORM','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('523','1525273761','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('524','1525273792','1','admin','79','24','MODAL_FORM','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('525','1525273792','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('526','1525273809','1','admin','79','24','MODAL_FORM','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('527','1525273809','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('528','1525273825','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('529','1525273825','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('530','1525273827','1','admin','79','25','FORM_CALL_REPORT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('531','1525273827','1','admin','78','25','FORM_CALL_REPORT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('532','1525274020','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('533','1525274020','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('534','1525274045','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('535','1525274045','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('536','1525274062','1','admin','22','10','eForm','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('537','1525274075','1','admin','22','2','DLMenu','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('538','1525274492','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('539','1525274492','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('540','1525274532','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('541','1525274643','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('542','1525274643','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('543','1525274646','1','admin','79','-','Untitled chunk','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('544','1525274646','1','admin','78','26','Untitled chunk','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('545','1525274711','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('546','1525274723','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('547','1525274723','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('548','1525274871','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('549','1525274906','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('550','1525275078','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('551','1525275267','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('552','1525275354','1','admin','78','25','FORM_CALL_REPORT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('553','1525275362','1','admin','78','25','FORM_CALL_REPORT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('554','1525275366','1','admin','80','25','FORM_CALL_REPORT','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('555','1525275366','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('556','1525275381','1','admin','78','26','Untitled chunk','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('557','1525275383','1','admin','80','26','Untitled chunk','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('558','1525275383','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('559','1525275388','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('560','1525275403','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('561','1525275412','1','admin','78','24','MODAL_FORM','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('562','1525275420','1','admin','80','24','MODAL_FORM','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('563','1525275420','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('564','1525275914','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('565','1525275914','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('566','1525275938','1','admin','78','28','eFeedbackReport','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('567','1525276040','1','admin','79','28','eFeedbackReport','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('568','1525276040','1','admin','78','28','eFeedbackReport','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('569','1525276094','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('570','1525276094','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('571','1525276170','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('572','1525276170','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('573','1525276243','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('574','1525276243','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('575','1525276320','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('576','1525276320','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('577','1525276519','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('578','1525276519','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('579','1525277113','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('580','1525277113','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('581','1525277196','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('582','1525277196','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('583','1525277316','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('584','1525277316','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('585','1525277351','1','admin','79','28','eFeedbackReport','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('586','1525277352','1','admin','78','28','eFeedbackReport','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('587','1525277488','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('588','1525277488','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('589','1525277703','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('590','1525277703','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('591','1525277706','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('592','1525277882','1','admin','22','10','eForm','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('593','1525278069','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('594','1525278087','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('595','1525278343','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('596','1525278411','1','admin','79','-','eModalForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('597','1525278411','1','admin','78','30','eModalForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('598','1525278418','1','admin','5','-',' Заказ звонка принят','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('599','1525278418','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('600','1525278519','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('601','1525278520','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('602','1525278581','1','admin','5','8',' Заказ звонка принят','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('603','1525278581','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('604','1525278587','1','admin','19','-','Новый шаблон','Creating a new template');

INSERT INTO `alv1_manager_log` VALUES ('605','1525278671','1','admin','20','-','eForm модального окна','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('606','1525278671','1','admin','16','10','eForm модального окна','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('607','1525278701','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('608','1525278703','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('609','1525278707','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('610','1525278714','1','admin','5','8',' Заказ звонка принят','Saving resource');

INSERT INTO `alv1_manager_log` VALUES ('611','1525278714','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('612','1525278835','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('613','1525278835','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('614','1525279071','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('615','1525279072','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('616','1525279123','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('617','1525279124','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('618','1525279137','1','admin','78','30','eModalForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('619','1525279147','1','admin','79','30','eModalForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('620','1525279147','1','admin','78','30','eModalForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('621','1525279172','1','admin','79','30','eModalForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('622','1525279172','1','admin','78','30','eModalForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('623','1525279179','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('624','1525279213','1','admin','79','30','eModalForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('625','1525279213','1','admin','78','30','eModalForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('626','1525279253','1','admin','78','29','feedback','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('627','1525279287','1','admin','80','30','eModalForm','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('628','1525279287','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('629','1525279375','1','admin','79','29','feedback','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('630','1525279375','1','admin','78','29','feedback','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('631','1525279384','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('632','1525279384','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('633','1525279404','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('634','1525279498','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('635','1525279538','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('636','1525279540','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('637','1525279551','1','admin','16','10','eForm модального окна','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('638','1525279563','1','admin','78','29','feedback','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('639','1525279576','1','admin','20','10','eForm модального окна','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('640','1525279576','1','admin','16','10','eForm модального окна','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('641','1525279618','1','admin','79','29','feedback','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('642','1525279618','1','admin','78','29','feedback','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('643','1525279931','1','admin','79','29','feedback','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('644','1525279931','1','admin','78','29','feedback','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('645','1525279949','1','admin','79','29','feedback','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('646','1525279949','1','admin','78','29','feedback','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('647','1525279963','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('648','1525280204','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('649','1525280209','1','admin','27','8',' Заказ звонка принят','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('650','1525280216','1','admin','6','8',' Заказ звонка принят','Deleting resource');

INSERT INTO `alv1_manager_log` VALUES ('651','1525280216','1','admin','3','8',' Заказ звонка принят','Viewing data for resource');

INSERT INTO `alv1_manager_log` VALUES ('652','1525280224','1','admin','16','10','eForm модального окна','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('653','1525280228','1','admin','21','10','eForm модального окна','Deleting template');

INSERT INTO `alv1_manager_log` VALUES ('654','1525280228','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('655','1525280242','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('656','1525280263','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('657','1525280295','1','admin','66','-','-','Sending a message');

INSERT INTO `alv1_manager_log` VALUES ('658','1525280295','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('659','1525280300','1','admin','10','1','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('660','1525280308','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('661','1525280378','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('662','1525280394','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('663','1525280412','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('664','1525280425','1','admin','66','-','-','Sending a message');

INSERT INTO `alv1_manager_log` VALUES ('665','1525280425','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('666','1525280432','1','admin','10','2','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('667','1525280435','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('668','1525280659','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('669','1525280672','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('670','1525280711','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('671','1525280711','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('672','1525280736','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('673','1525280737','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('674','1525280761','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('675','1525280761','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('676','1525280782','1','admin','79','27','eFeedbackForm','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('677','1525280782','1','admin','78','27','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('678','1525281190','1','admin','95','-','-','Importing resources from HTML');

INSERT INTO `alv1_manager_log` VALUES ('679','1525281201','1','admin','95','-','-','Importing resources from HTML');

INSERT INTO `alv1_manager_log` VALUES ('680','1525281210','1','admin','27','9','index','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('681','1525281222','1','admin','6','9','index','Deleting resource');

INSERT INTO `alv1_manager_log` VALUES ('682','1525281222','1','admin','3','9','index','Viewing data for resource');

INSERT INTO `alv1_manager_log` VALUES ('683','1525281225','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('684','1525281236','1','admin','27','9','index','Editing resource');

INSERT INTO `alv1_manager_log` VALUES ('685','1525281249','1','admin','64','-','-','Removing deleted content');

INSERT INTO `alv1_manager_log` VALUES ('686','1525281262','1','admin','83','-','-','Exporting a resource to HTML');

INSERT INTO `alv1_manager_log` VALUES ('687','1525281289','1','admin','93','-','-','Backup Manager');

INSERT INTO `alv1_manager_log` VALUES ('688','1525328778','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('689','1525328838','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('690','1525328909','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('691','1525328918','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('692','1525328931','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('693','1525329008','1','admin','106','-','-','Viewing Modules');

INSERT INTO `alv1_manager_log` VALUES ('694','1525329022','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('695','1525329035','1','admin','120','-','-','Idle (unknown)');

INSERT INTO `alv1_manager_log` VALUES ('696','1525329056','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('697','1525329067','1','admin','25','10','eForm','Deleting Snippet');

INSERT INTO `alv1_manager_log` VALUES ('698','1525329067','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('699','1525329081','1','admin','22','10','-','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('700','1525329121','1','admin','106','-','-','Viewing Modules');

INSERT INTO `alv1_manager_log` VALUES ('701','1525329141','1','admin','80','27','eFeedbackForm','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('702','1525329142','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('703','1525329145','1','admin','80','28','eFeedbackReport','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('704','1525329145','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('705','1525329148','1','admin','80','29','feedback','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('706','1525329148','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('707','1525329166','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('708','1525329177','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('709','1525329177','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('710','1525329180','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('711','1525329299','1','admin','22','6','FormLister','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('712','1525329331','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('713','1525329358','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('714','1525329367','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('715','1525329371','1','admin','10','1','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('716','1525329373','1','admin','65','1','-','Deleting a message');

INSERT INTO `alv1_manager_log` VALUES ('717','1525329373','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('718','1525329375','1','admin','10','2','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('719','1525329376','1','admin','65','2','-','Deleting a message');

INSERT INTO `alv1_manager_log` VALUES ('720','1525329376','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('721','1525329481','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('722','1525329493','1','admin','23','-','Новый сниппет','Creating a new Snippet');

INSERT INTO `alv1_manager_log` VALUES ('723','1525329496','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('724','1525329538','1','admin','71','-','-','Searching');

INSERT INTO `alv1_manager_log` VALUES ('725','1525329563','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('726','1525329575','1','admin','114','-','-','View event log');

INSERT INTO `alv1_manager_log` VALUES ('727','1525329580','1','admin','116','-','-','Delete event log');

INSERT INTO `alv1_manager_log` VALUES ('728','1525329581','1','admin','114','-','-','View event log');

INSERT INTO `alv1_manager_log` VALUES ('729','1525329584','1','admin','13','-','-','Viewing logging');

INSERT INTO `alv1_manager_log` VALUES ('730','1525329591','1','admin','53','-','-','Viewing system info');

INSERT INTO `alv1_manager_log` VALUES ('731','1525329610','1','admin','9','-','-','Viewing help');

INSERT INTO `alv1_manager_log` VALUES ('732','1525330740','1','admin','112','1','Doc Manager','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('733','1525330746','1','admin','106','-','-','Viewing Modules');

INSERT INTO `alv1_manager_log` VALUES ('734','1525330751','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('735','1525331015','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('736','1525331015','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('737','1525331101','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('738','1525331411','1','admin','79','-','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('739','1525331411','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('740','1525331558','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('741','1525331558','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('742','1525331561','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('743','1525331561','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('744','1525331760','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('745','1525331760','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('746','1525331833','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('747','1525331833','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('748','1525332002','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('749','1525332002','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('750','1525332075','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('751','1525332075','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('752','1525332232','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('753','1525332232','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('754','1525333051','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('755','1525333051','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('756','1525333194','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('757','1525333194','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('758','1525333281','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('759','1525333281','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('760','1525333345','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('761','1525333345','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('762','1525333492','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('763','1525333492','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('764','1525333641','1','admin','4','-','Новый ресурс','Creating a resource');

INSERT INTO `alv1_manager_log` VALUES ('765','1525333666','1','admin','19','-','Новый шаблон','Creating a new template');

INSERT INTO `alv1_manager_log` VALUES ('766','1525333817','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('767','1525333848','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('768','1525333848','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('769','1525333850','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('770','1525333850','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('771','1525333933','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('772','1525334079','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('773','1525334079','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('774','1525334158','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('775','1525334158','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('776','1525334189','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('777','1525334189','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('778','1525336868','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('779','1525336868','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('780','1525336934','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('781','1525336934','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('782','1525336997','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('783','1525336997','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('784','1525337016','1','admin','78','9','MAIN_SCRIPT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('785','1525337037','1','admin','79','9','MAIN_SCRIPT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('786','1525337037','1','admin','78','9','MAIN_SCRIPT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('787','1525337043','1','admin','79','34','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('788','1525337043','1','admin','78','34','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('789','1525337046','1','admin','79','7','MODAL_CALLBACK','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('790','1525337046','1','admin','78','7','MODAL_CALLBACK','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('791','1525337052','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('792','1525337059','1','admin','112','1','Doc Manager','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('793','1525337066','1','admin','106','-','-','Viewing Modules');

INSERT INTO `alv1_manager_log` VALUES ('794','1525337077','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('795','1525337084','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('796','1525337086','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('797','1525337099','1','admin','31','-','C:/OpenServer/domains/enteley.com/assets/templates/enteley_template/about.html','Deleted File');

INSERT INTO `alv1_manager_log` VALUES ('798','1525337102','1','admin','31','-','C:/OpenServer/domains/enteley.com/assets/templates/enteley_template/contact.html','Deleted File');

INSERT INTO `alv1_manager_log` VALUES ('799','1525337105','1','admin','31','-','C:/OpenServer/domains/enteley.com/assets/templates/enteley_template/index.html','Deleted File');

INSERT INTO `alv1_manager_log` VALUES ('800','1525337108','1','admin','31','-','C:/OpenServer/domains/enteley.com/assets/templates/enteley_template/oplata-dostavka.html','Deleted File');

INSERT INTO `alv1_manager_log` VALUES ('801','1525337111','1','admin','31','-','C:/OpenServer/domains/enteley.com/assets/templates/enteley_template/pravila.html','Deleted File');

INSERT INTO `alv1_manager_log` VALUES ('802','1525337114','1','admin','31','-','C:/OpenServer/domains/enteley.com/assets/templates/enteley_template/stock.html','Deleted File');

INSERT INTO `alv1_manager_log` VALUES ('803','1525337117','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('804','1525337120','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('805','1525337125','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('806','1525337127','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('807','1525337130','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('808','1525337132','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('809','1525337134','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('810','1525337136','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('811','1525337142','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('812','1525337151','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('813','1525337206','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('814','1525337213','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('815','1525337424','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('816','1525337429','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('817','1525337434','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('818','1525337435','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('819','1525337603','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('820','1525337606','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('821','1525337653','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('822','1525337659','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('823','1525337661','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('824','1525337760','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('825','1525337807','1','admin','8','-','-','Logged out');

INSERT INTO `alv1_manager_log` VALUES ('826','1525337867','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('827','1525337913','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('828','1525337925','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('829','1525337931','1','admin','25','11','eForm','Deleting Snippet');

INSERT INTO `alv1_manager_log` VALUES ('830','1525337931','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('831','1525337962','1','admin','80','31','eFeedbackForm','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('832','1525337962','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('833','1525337964','1','admin','80','32','eFeedbackReport','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('834','1525337965','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('835','1525337967','1','admin','80','33','feedback','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('836','1525337967','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('837','1525337982','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('838','1525337984','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('839','1525338010','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('840','1525338015','1','admin','80','34','CONTACT_FORM_TPL','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('841','1525338015','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('842','1525338033','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('843','1525338055','1','admin','80','7','MODAL_CALLBACK','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('844','1525338055','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('845','1525338060','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('846','1525338064','1','admin','8','-','-','Logged out');

INSERT INTO `alv1_manager_log` VALUES ('847','1525338135','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('848','1525338149','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('849','1525338258','1','admin','79','-','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('850','1525338258','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('851','1525338266','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('852','1525338366','1','admin','79','-','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('853','1525338366','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('854','1525338416','1','admin','78','5','MAIN_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('855','1525338428','1','admin','78','11','WRAPPER_ABOUT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('856','1525338448','1','admin','78','4','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('857','1525338487','1','admin','78','6','WRAPPER_INDEX','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('858','1525338513','1','admin','78','8','ICONS','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('859','1525338540','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('860','1525338563','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('861','1525338594','1','admin','20','3','Каталог товаров','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('862','1525338594','1','admin','16','3','Каталог товаров','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('863','1525338603','1','admin','16','7','Акции','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('864','1525338610','1','admin','20','7','Акции','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('865','1525338610','1','admin','16','7','Акции','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('866','1525338616','1','admin','16','8','Контакты','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('867','1525338627','1','admin','20','8','Контакты','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('868','1525338627','1','admin','16','8','Контакты','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('869','1525338632','1','admin','16','4','О компании','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('870','1525338639','1','admin','20','4','О компании','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('871','1525338639','1','admin','16','4','О компании','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('872','1525338643','1','admin','16','6','Оплата и доставка','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('873','1525338647','1','admin','20','6','Оплата и доставка','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('874','1525338648','1','admin','16','6','Оплата и доставка','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('875','1525338652','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('876','1525338658','1','admin','20','5','Памятка покупателю','Saving template');

INSERT INTO `alv1_manager_log` VALUES ('877','1525338658','1','admin','16','5','Памятка покупателю','Editing template');

INSERT INTO `alv1_manager_log` VALUES ('878','1525338733','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('879','1525339428','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('880','1525339428','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('881','1525339500','1','admin','79','1','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('882','1525339500','1','admin','78','1','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('883','1525339525','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('884','1525339525','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('885','1525339875','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('886','1525339881','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('887','1525340074','1','admin','78','37','eFeedbackForm','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('888','1525340461','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('889','1525340615','1','admin','79','-','THANK_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('890','1525340615','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('891','1525340676','1','admin','97','40','THANK_TPL Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('892','1525340676','1','admin','78','41','THANK_TPL Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('893','1525340852','1','admin','79','41','REPORT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('894','1525340852','1','admin','78','41','REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('895','1525340887','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('896','1525341163','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('897','1525341163','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('898','1525345877','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('899','1525345877','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('900','1525346010','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('901','1525346010','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('902','1525346031','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('903','1525346031','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('904','1525346289','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('905','1525346289','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('906','1525346300','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('907','1525346300','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('908','1525346344','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('909','1525346344','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('910','1525350521','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('911','1525350753','1','admin','79','3','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('912','1525350753','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('913','1525350789','1','admin','79','3','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('914','1525350790','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('915','1525350821','1','admin','79','3','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('916','1525350821','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('917','1525350904','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('918','1525350929','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('919','1525350943','1','admin','97','35','MODAL_CONTACT Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('920','1525350943','1','admin','78','42','MODAL_CONTACT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('921','1525350946','1','admin','79','42','MODAL_CONTACT Копия','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('922','1525350946','1','admin','78','42','MODAL_CONTACT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('923','1525350951','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('924','1525351199','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('925','1525351205','1','admin','78','41','REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('926','1525351881','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('927','1525351881','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('928','1525352045','1','admin','79','41','REPORT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('929','1525352045','1','admin','78','41','REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('930','1525352367','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('931','1525352367','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('932','1525352370','1','admin','79','40','THANK_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('933','1525352370','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('934','1525352373','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('935','1525352373','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('936','1525352480','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('937','1525352480','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('938','1525352483','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('939','1525352522','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('940','1525352522','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('941','1525352546','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('942','1525352546','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('943','1525352650','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('944','1525352650','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('945','1525352656','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('946','1525353075','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('947','1525353075','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('948','1525353150','1','admin','79','36','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('949','1525353150','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('950','1525411053','1','admin','78','36','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('951','1525411296','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('952','1525411336','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('953','1525411374','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('954','1525411402','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('955','1525411419','1','admin','25','12','eForm','Deleting Snippet');

INSERT INTO `alv1_manager_log` VALUES ('956','1525411419','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('957','1525411428','1','admin','80','37','eFeedbackForm','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('958','1525411428','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('959','1525411432','1','admin','80','38','eFeedbackReport','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('960','1525411432','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('961','1525411435','1','admin','80','39','feedback','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('962','1525411436','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('963','1525411448','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('964','1525411474','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('965','1525411493','1','admin','8','-','-','Logged out');

INSERT INTO `alv1_manager_log` VALUES ('966','1525411522','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('967','1525411549','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('968','1525411571','1','admin','78','42','MODAL_CONTACT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('969','1525411576','1','admin','80','42','MODAL_CONTACT Копия','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('970','1525411576','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('971','1525411583','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('972','1525411782','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('973','1525411813','1','admin','78','13','MAIN_OPLATA_DOSTAVKA','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('974','1525411824','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('975','1525411845','1','admin','78','41','REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('976','1525411860','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('977','1525411880','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('978','1525411897','1','admin','97','36','CONTACT_FORM_TPL Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('979','1525411897','1','admin','78','43','CONTACT_FORM_TPL Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('980','1525411903','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('981','1525411909','1','admin','80','36','CONTACT_FORM_TPL','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('982','1525411909','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('983','1525411945','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('984','1525411945','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('985','1525411963','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('986','1525411963','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('987','1525411968','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('988','1525411982','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('989','1525411985','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('990','1525411985','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('991','1525412029','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('992','1525412057','1','admin','78','41','REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('993','1525412072','1','admin','79','41','CONTACT_FORM_REPORT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('994','1525412072','1','admin','78','41','CONTACT_FORM_REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('995','1525412116','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('996','1525412121','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('997','1525412142','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('998','1525412155','1','admin','78','40','THANK_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('999','1525412219','1','admin','97','40','THANK_TPL Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1000','1525412219','1','admin','78','44','THANK_TPL Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1001','1525412297','1','admin','79','44','CONTACT_FORM_OUTER','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1002','1525412298','1','admin','78','44','CONTACT_FORM_OUTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1003','1525412328','1','admin','97','44','CONTACT_FORM_OUTER Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1004','1525412328','1','admin','78','45','CONTACT_FORM_OUTER Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1005','1525413067','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1006','1525413067','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1007','1525413073','1','admin','79','45','CONTACT_FORM_ERROR_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1008','1525413073','1','admin','78','45','CONTACT_FORM_ERROR_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1009','1525413081','1','admin','97','45','CONTACT_FORM_ERROR_TPL Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1010','1525413081','1','admin','78','46','CONTACT_FORM_ERROR_TPL Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1011','1525413126','1','admin','79','46','CONTACT_FORM_SUBJECT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1012','1525413126','1','admin','78','46','CONTACT_FORM_SUBJECT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1013','1525413162','1','admin','79','46','CONTACT_FORM_SUBJECT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1014','1525413162','1','admin','78','46','CONTACT_FORM_SUBJECT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1015','1525413322','1','admin','79','41','CONTACT_FORM_REPORT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1016','1525413322','1','admin','78','41','CONTACT_FORM_REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1017','1525413403','1','admin','79','46','CONTACT_FORM_SUBJECT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1018','1525413404','1','admin','78','46','CONTACT_FORM_SUBJECT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1019','1525413406','1','admin','97','46','CONTACT_FORM_SUBJECT_TPL Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1020','1525413406','1','admin','78','47','CONTACT_FORM_SUBJECT_TPL Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1021','1525413491','1','admin','79','47','CONTACT_FORM_SUCCESS_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1022','1525413492','1','admin','78','47','CONTACT_FORM_SUCCESS_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1023','1525413527','1','admin','79','47','CONTACT_FORM_SUCCESS_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1024','1525413527','1','admin','78','47','CONTACT_FORM_SUCCESS_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1025','1525413922','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1026','1525413989','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1027','1525413992','1','admin','78','45','CONTACT_FORM_ERROR_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1028','1525414033','1','admin','79','45','CONTACT_FORM_ERROR_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1029','1525414033','1','admin','78','45','CONTACT_FORM_ERROR_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1030','1525414153','1','admin','97','45','CONTACT_FORM_ERROR_TPL Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1031','1525414153','1','admin','78','48','CONTACT_FORM_ERROR_TPL Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1032','1525414275','1','admin','79','48','CONTACT_FORM_MESSAGE_OUTER_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1033','1525414275','1','admin','78','48','CONTACT_FORM_MESSAGE_OUTER_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1034','1525414352','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1035','1525414381','1','admin','80','46','CONTACT_FORM_SUBJECT_TPL','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1036','1525414381','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1037','1525414396','1','admin','78','45','CONTACT_FORM_ERROR_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1038','1525414490','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1039','1525414490','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1040','1525414495','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1041','1525414510','1','admin','78','44','CONTACT_FORM_OUTER','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1042','1525415059','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1043','1525415068','1','admin','80','44','CONTACT_FORM_OUTER','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1044','1525415068','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1045','1525415080','1','admin','80','40','THANK_TPL','Deleting Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1046','1525415080','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1047','1525415163','1','admin','79','41','CONTACT_FORM_REPORT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1048','1525415163','1','admin','78','41','CONTACT_FORM_REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1049','1525415166','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1050','1525415166','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1051','1525415167','1','admin','79','47','CONTACT_FORM_SUCCESS_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1052','1525415167','1','admin','78','47','CONTACT_FORM_SUCCESS_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1053','1525415170','1','admin','79','48','CONTACT_FORM_MESSAGE_OUTER_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1054','1525415170','1','admin','78','48','CONTACT_FORM_MESSAGE_OUTER_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1055','1525415174','1','admin','79','45','CONTACT_FORM_ERROR_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1056','1525415174','1','admin','78','45','CONTACT_FORM_ERROR_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1057','1525415200','1','admin','79','48','CONTACT_FORM_MESSAGE_OUTER_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1058','1525415200','1','admin','78','48','CONTACT_FORM_MESSAGE_OUTER_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1059','1525415477','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1060','1525415477','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1061','1525415521','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1062','1525415521','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1063','1525415525','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1064','1525415559','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1065','1525415559','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1066','1525415626','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1067','1525415626','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1068','1525415660','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1069','1525415660','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1070','1525416792','1','admin','79','41','CONTACT_FORM_REPORT_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1071','1525416792','1','admin','78','41','CONTACT_FORM_REPORT_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1072','1525416795','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1073','1525416795','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1074','1525416797','1','admin','79','35','MODAL_CONTACT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1075','1525416797','1','admin','78','35','MODAL_CONTACT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1076','1525417306','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1077','1525417306','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1078','1525417376','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1079','1525417376','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1080','1525417475','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1081','1525417475','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1082','1525417726','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1083','1525417727','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1084','1525417735','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1085','1525417736','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1086','1525417740','1','admin','8','-','-','Logged out');

INSERT INTO `alv1_manager_log` VALUES ('1087','1525419612','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('1088','1525419625','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1089','1525419661','1','admin','79','3','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1090','1525419661','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1091','1525419666','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1092','1525419848','1','admin','79','3','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1093','1525419848','1','admin','78','3','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1094','1525419854','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1095','1525420661','1','admin','10','-','-','Viewing/ composing messages');

INSERT INTO `alv1_manager_log` VALUES ('1096','1525422590','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1097','1525422771','1','admin','112','1','Doc Manager','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('1098','1525422785','1','admin','31','-','-','Using file manager');

INSERT INTO `alv1_manager_log` VALUES ('1099','1525422788','1','admin','120','-','-','Idle (unknown)');

INSERT INTO `alv1_manager_log` VALUES ('1100','1525422803','1','admin','106','-','-','Viewing Modules');

INSERT INTO `alv1_manager_log` VALUES ('1101','1525422814','1','admin','93','-','-','Backup Manager');

INSERT INTO `alv1_manager_log` VALUES ('1102','1525426213','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1103','1525426213','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1104','1525426253','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1105','1525426253','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1106','1525426461','1','admin','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1107','1525426537','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1108','1525426537','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1109','1525426868','1','admin','79','-','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1110','1525426868','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1111','1525426969','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1112','1525426969','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1113','1525427010','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1114','1525427010','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1115','1525427295','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1116','1525427295','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1117','1525427298','1','admin','97','49','LIST_SUMMARY_PRODUCT Копия','Duplicate Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1118','1525427298','1','admin','78','50','LIST_SUMMARY_PRODUCT Копия','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1119','1525427421','1','admin','79','50','LIST_SUMMARY_FASHION','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1120','1525427421','1','admin','78','50','LIST_SUMMARY_FASHION','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1121','1525427446','1','admin','76','-','-','Element management');

INSERT INTO `alv1_manager_log` VALUES ('1122','1525427460','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1123','1525427555','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1124','1525427555','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1125','1525427990','1','admin','23','-','Новый сниппет','Creating a new Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1126','1525428301','1','admin','24','-','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1127','1525428301','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1128','1525429382','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1129','1525429390','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1130','1525429390','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1131','1525429396','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1132','1525429396','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1133','1525429451','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1134','1525429451','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1135','1525429454','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1136','1525429454','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1137','1525429457','1','admin','79','50','LIST_SUMMARY_FASHION','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1138','1525429457','1','admin','78','50','LIST_SUMMARY_FASHION','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1139','1525429460','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1140','1525429461','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1141','1525429463','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1142','1525429463','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1143','1525429542','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1144','1525429542','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1145','1525429557','1','admin','112','2','Extras','Execute module');

INSERT INTO `alv1_manager_log` VALUES ('1146','1525429619','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1147','1525429619','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1148','1525429632','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1149','1525429632','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1150','1525429719','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1151','1525429719','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1152','1525429911','1','admin','24','13','prepare_img_product','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1153','1525429911','1','admin','22','13','prepare_img_product','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1154','1525429918','1','admin','98','13','prepare_img_product Копия','Duplicate Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1155','1525429918','1','admin','22','14','prepare_img_product Копия','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1156','1525430002','1','admin','79','50','LIST_SUMMARY_FASHION','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1157','1525430002','1','admin','78','50','LIST_SUMMARY_FASHION','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1158','1525430023','1','admin','24','14','prepare_img_fashion','Saving Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1159','1525430023','1','admin','22','14','prepare_img_fashion','Editing Snippet');

INSERT INTO `alv1_manager_log` VALUES ('1160','1525430035','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1161','1525430035','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1162','1525430038','1','admin','79','50','LIST_SUMMARY_FASHION','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1163','1525430038','1','admin','78','50','LIST_SUMMARY_FASHION','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1164','1525430168','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1165','1525430168','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1166','1525430509','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1167','1525430509','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1168','1525430766','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1169','1525430766','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1170','1525430801','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1171','1525430801','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1172','1525430822','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1173','1525430822','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1174','1525430825','1','admin','79','50','LIST_SUMMARY_FASHION','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1175','1525430826','1','admin','78','50','LIST_SUMMARY_FASHION','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1176','1525430828','1','admin','79','49','LIST_SUMMARY_PRODUCT','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1177','1525430828','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1178','1525430849','1','admin','93','-','-','Backup Manager');

INSERT INTO `alv1_manager_log` VALUES ('1179','1525447120','1','admin','58','-','MODX','Logged in');

INSERT INTO `alv1_manager_log` VALUES ('1180','1525447143','1','admin','17','-','-','Editing settings');

INSERT INTO `alv1_manager_log` VALUES ('1181','1525447211','1','admin','30','-','-','Saving settings');

INSERT INTO `alv1_manager_log` VALUES ('1182','1525447340','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1183','1525447354','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1184','1525447381','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1185','1525447429','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1186','1525447437','1','admin','79','43','CONTACT_FORM_TPL','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1187','1525447437','1','admin','78','43','CONTACT_FORM_TPL','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1188','1525447635','1','admin','78','49','LIST_SUMMARY_PRODUCT','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1189','1525447643','1','admin','78','50','LIST_SUMMARY_FASHION','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1190','1525447677','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1191','1525447847','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1192','1525447847','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1193','1525447856','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1194','1525447856','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1195','1525448030','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1196','1525448030','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1197','1525448077','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1198','1525448077','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1199','1525448104','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1200','1525448104','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1201','1525448311','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1202','1525448311','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1203','1525448331','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1204','1525448331','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1205','1525448358','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1206','1525448358','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1207','1525448904','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1208','1525448904','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1209','1525449341','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1210','1525449341','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1211','1525449360','1','admin','83','-','-','Exporting a resource to HTML');

INSERT INTO `alv1_manager_log` VALUES ('1212','1525449365','1','admin','93','-','-','Backup Manager');

INSERT INTO `alv1_manager_log` VALUES ('1213','1525608651','1','admin','93','-','-','Backup Manager');

INSERT INTO `alv1_manager_log` VALUES ('1214','1525608677','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1215','1525608740','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1216','1525608794','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1217','1525608794','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1218','1525608807','1','admin','79','10','SECTION_PRODUCT_LIST','Saving Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1219','1525608807','1','admin','78','10','SECTION_PRODUCT_LIST','Editing Chunk (HTML Snippet)');

INSERT INTO `alv1_manager_log` VALUES ('1220','1525608810','1','admin','26','-','-','Refreshing site');

INSERT INTO `alv1_manager_log` VALUES ('1221','1525608812','1','admin','93','-','-','Backup Manager');


# --------------------------------------------------------

#
# Table structure for table `alv1_manager_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_manager_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_manager_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Contains login information for backend users.';

#
# Dumping data for table `alv1_manager_users`
#

INSERT INTO `alv1_manager_users` VALUES ('1','admin','$P$BiiEkoXdm9PK6GtQ9OcLTnHMeDWC7./');


# --------------------------------------------------------

#
# Table structure for table `alv1_member_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_member_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_member_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_group` int(10) NOT NULL DEFAULT '0',
  `member` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_group_member` (`user_group`,`member`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `alv1_member_groups`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_membergroup_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_membergroup_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_membergroup_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `membergroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `alv1_membergroup_access`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_membergroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_membergroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_membergroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `alv1_membergroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_products`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_products`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `composition` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

#
# Dumping data for table `alv1_products`
#

INSERT INTO `alv1_products` VALUES ('1','material-1.jpg','4700','26697','Jakob schlaepfer (Швейцария)','Шелк 100%');

INSERT INTO `alv1_products` VALUES ('2','material-2.jpg','3700','12345','Jakob schlaepfer (Швейцария)','Шерсть');

INSERT INTO `alv1_products` VALUES ('3','material-3.jpg','3400','12346','Kengu (Австралия)','Хлопок');

INSERT INTO `alv1_products` VALUES ('4','material-4.jpg','2300','3456','Kengu (Австралия)','Хлопок');

INSERT INTO `alv1_products` VALUES ('5','material-5.jpg','5690','56789','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('6','material-6.jpg','2345','54678','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('7','material-7.jpg','2300','3456','Kengu (Австралия)','Хлопок');

INSERT INTO `alv1_products` VALUES ('8','material-8.jpg','2345','54678','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('9','material-9.jpg','2300','3456','Kengu (Австралия)','Хлопок');

INSERT INTO `alv1_products` VALUES ('10','material-10.jpg','3700','12345','Jakob schlaepfer (Швейцария)','Шерсть');

INSERT INTO `alv1_products` VALUES ('11','material-11.jpg','4700','26697','Jakob schlaepfer (Швейцария)','Шелк 100%');

INSERT INTO `alv1_products` VALUES ('12','material-12.jpg','2300','3456','Kengu (Австралия)','Хлопок');

INSERT INTO `alv1_products` VALUES ('13','material-13.jpg','5690','56789','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('14','material-14.jpg','567','678','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('15','material-15.jpg','6543','2233','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('16','material-16.jpg','456678','456','Dazz (Германия)','Акрил 10% Шерсть 90%');

INSERT INTO `alv1_products` VALUES ('17','material-17.jpg','1200','5679','Kengu (Австралия)','Хлопок');

INSERT INTO `alv1_products` VALUES ('18','material-18.jpg','4000','2000','Jakob schlaepfer (Швейцария)','Шелк 100%');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_content`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_content`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `contentType` varchar(50) NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(245) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `published` int(1) NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` int(1) NOT NULL DEFAULT '0',
  `introtext` text COMMENT 'Used to provide quick summary of the document',
  `content` mediumtext,
  `richtext` tinyint(1) NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `cacheable` int(1) NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0' COMMENT 'Date the document was published',
  `publishedby` int(10) NOT NULL DEFAULT '0' COMMENT 'ID of user who published the document',
  `menutitle` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title',
  `donthit` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Disable page hit count',
  `privateweb` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private web document',
  `privatemgr` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private manager document',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-inline, 1-attachment',
  `hidemenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Hide document from menu',
  `alias_visible` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `parent` (`parent`),
  KEY `aliasidx` (`alias`),
  KEY `typeidx` (`type`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`description`,`content`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Contains the site document tree.';

#
# Dumping data for table `alv1_site_content`
#

INSERT INTO `alv1_site_content` VALUES ('1','document','text/html','Каталог товаров','Каталог товаров','','index','','1','0','0','0','0','','','1','3','1','1','1','1','1130304721','1','1525166079','0','0','0','1130304721','1','Каталог товаров','0','0','0','0','0','1');

INSERT INTO `alv1_site_content` VALUES ('2','document','text/html','О компании','О компании','','about','','1','0','0','0','0','','','1','4','0','1','1','1','1525165848','1','1525168061','0','0','0','1525165848','1','О компании','0','0','0','0','0','1');

INSERT INTO `alv1_site_content` VALUES ('3','document','text/html','Памятка покупателю','Памятка покупателю','','pravila','','1','0','0','0','0','','','1','5','2','1','1','1','1525165899','1','1525168427','0','0','0','1525165963','1','Памятка покупателю','0','0','0','0','0','1');

INSERT INTO `alv1_site_content` VALUES ('4','document','text/html','Оплата и доставка','Оплата и доставка','','oplata-dostavka','','1','0','0','0','0','','','1','6','3','1','1','1','1525166060','1','1525169903','0','0','0','1525166060','1','Оплата и доставка','0','0','0','0','0','1');

INSERT INTO `alv1_site_content` VALUES ('5','document','text/html','Акции','Акции','','stock','','1','0','0','0','0','','','1','7','4','1','1','1','1525166161','1','1525170314','0','0','0','1525166161','1','Акции','0','0','0','0','0','1');

INSERT INTO `alv1_site_content` VALUES ('6','document','text/html','Контакты','Контакты','','contact','','1','0','0','0','0','','','1','8','5','1','1','1','1525166194','1','1525170649','0','0','0','1525166194','1','Контакты','0','0','0','0','0','1');

INSERT INTO `alv1_site_content` VALUES ('7','document','text/html','404 - Документ не найден','Ох ... это 404! (Страница не найдена)','','doc-not-found','','1','0','0','0','0','','','1','9','6','1','1','1','1525166391','1','1525170957','0','0','0','1525166391','1','','0','0','0','0','1','1');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_htmlsnippets`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_htmlsnippets`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_htmlsnippets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `editor_name` varchar(50) NOT NULL DEFAULT 'none',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the snippet',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='Contains the site chunks.';

#
# Dumping data for table `alv1_site_htmlsnippets`
#

INSERT INTO `alv1_site_htmlsnippets` VALUES ('1','HEADER','Шапка сайта','2','none','7','0',' 			<header class=\"header\">\n                <div class=\"header__inner inner\">\n                    <div class=\"header-logo\">\n                        <div class=\"header-logo__image\">\n                            <a href=\"index.html\" class=\"header-logo__link\">\n                                <img src=\"/assets/templates/enteley_template/img/style/logo.png\" alt=\"Enteley\" class=\"header-logo__img\"/>\n                            </a>\n                        </div>\n                    </div>\n\n                    {{MENU_HEADER}}\n\n                    <div class=\"header-contact\">\n                        <div class=\"header-contact__phone\">\n                            <p class=\"header-contact__tel\">\n                                <a href=\"tel:+7495327-81-20\">+7 (495) 327-81-20</a>\n                            </p>\n                        </div>\n\n                        <a href=\"[~[*id*]~]#myModal\" title=\"Заказать звонок\">\n                            <span class=\"btn header-contact__btn\" role=\"button\">\n                                <svg class=\"icon header__icon\">\n                                    <use xlink:href=\"#icon-phone\"></use>\n                                </svg>\n                                Заказать звонок\n                            </span>\n                        </a>\n                    </div>\n\n                    <div class=\"header-basket\">\n                        <a href=\"#\" class=\"header-basket__link\">\n                            <svg class=\"icon header-basket__icon\">\n                                <use xlink:href=\"#icon-basket\"></use>\n                            </svg>\n\n                            Ваша корзина\n                            <span class=\"header-basket__sum\">5</span>\n                        </a>\n                        <p class=\"header-basket__price\">\n                            на сумму 6860 Р\n                        </p>\n                    </div>\n                </div>\n            </header>','0','0','1525339500','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('2','mm_rules','Default ManagerManager rules.','0','none','3','0','// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php\n// example of how PHP is allowed - check that a TV named documentTags exists before creating rule\n\nif ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getFullTableName(\'site_tmplvars\'), \"name=\'documentTags\'\"))) {\n	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV\n}\nmm_widget_showimagetvs(); // Always give a preview of Image TVs\n\nmm_createTab(\'SEO\', \'seo\', \'\', \'\', \'\', \'\');\nmm_moveFieldsToTab(\'titl,keyw,desc,seoOverride,noIndex,sitemap_changefreq,sitemap_priority,sitemap_exclude\', \'seo\', \'\', \'\');\nmm_widget_tags(\'keyw\',\',\'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV\n\n\n//mm_createTab(\'Images\', \'photos\', \'\', \'\', \'\', \'850\');\n//mm_moveFieldsToTab(\'images,photos\', \'photos\', \'\', \'\');\n\n//mm_hideFields(\'longtitle,description,link_attributes,menutitle,content\', \'\', \'6,7\');\n\n//mm_hideTemplates(\'0,5,8,9,11,12\', \'2,3\');\n\n//mm_hideTabs(\'settings, access\', \'2\');\n','0','0','0','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('3','HEAD','Служебные заголовки','2','none','7','0','<!DOCTYPE html>\n<html lang=\"[(lang_code)]\">\n    <head>\n        <title>[(site_name)] | [*longtitle*]</title>\n        <meta charset=\"[(modx_charset)]\" />\n        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge, chrome=1\" />\n		<meta name=\"keywords\" content=\"[*keyw*]\" />\n		<meta name=\"description\" content=\"[*desc*]\" />\n		<base href=\"[(site_url)]\"/>\n						\n		<link href=\"[(site_url)][[if? &is=[(site_start)]:!=:[*id*] &then=`[~[*id*]~]`]]\" rel=\"canonical\">\n        <link rel=\"stylesheet\" href=\"/assets/templates/enteley_template/module/normalize.css\" />\n        <link rel=\"stylesheet\" href=\"/assets/templates/enteley_template/css/style.css\" />\n        <!--[if lt IE 9]>\n            <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\n        <![endif]-->\n    </head>','0','1525162977','1525419848','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('21','MENU_HEADER','Меню шапки сайта','2','none','7','0','					<nav class=\"header__menu\">\n						[[DLMenu?\n								&parents=`0`\n								&maxDepth=`1`\n								&rowTpl=`@CODE:<li[+classes+]><a class=\"header__menu-link\" href=\"[+url+]\">[+title+]</a></li>`\n								&outerClass=`header__menu-inner`\n								&rowClass=`header__menu-item`\n								&firstClass=``\n								&lastClass=``\n								&levelClass=``\n								&oddClass=``\n								&evenClass=``\n						]]\n                    </nav>','0','1525171245','1525269246','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('23','MENU_FOOTER','Меню подвала сайта','2','none','7','0','					<nav class=\"footer__menu\">\n                        <ul class=\"footer__menu-inner\">\n                            <li class=\"footer__menu-item\">\n                                <a class=\"footer__menu-link\" href=\"[~2~]\">О компании</a>\n                            </li>\n                            <li class=\"footer__menu-item\">\n                                <a class=\"footer__menu-link\" href=\"[~1~]\">Каталог товаров</a>\n                            </li>\n                            <li class=\"footer__menu-item\">\n                                <a class=\"footer__menu-link\" href=\"[~3~]\">Памятка покупателю</a>\n                            </li>\n                            <li class=\"footer__menu-item\">\n                                <a class=\"footer__menu-link\" href=\"[~4~]\">Оплата и доставка</a>\n                            </li>\n                            <li class=\"footer__menu-item\">\n                                <a class=\"footer__menu-link\" href=\"[~5~]\">Акции</a>\n                            </li>\n                            <li class=\"footer__menu-item\">\n                                <a class=\"footer__menu-link\" href=\"[~6~]\">Контакты</a>\n                            </li>\n                        </ul>\n                    </nav>					','0','0','1525269048','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('4','FOOTER','Подвал сайта','2','none','7','0','            <footer class=\"footer\">\n                <div class=\"footer__inner inner\">\n                    <div class=\"footer-copy\">\n                        <p class=\"footer-copy__inner\">\n                            &copy; 2018 &laquoEnteley&raquo <br> Интернет-магазин тканей\n                        </p>\n                    </div>\n\n                    <div class=\"footer-contact\">\n                        <div class=\"footer-contact__vcard vcard\">\n                            <div class=\"footer-contact__phone\">\n                                <span class=\"footer-contact__tel tel\">\n                                    <a href=\"tel:+7495327-81-20\">+7 (495) 327-81-20</a>\n                                </span>\n                            </div>\n\n                            <div class=\"footer-contact__adr adr\">\n                                <span class=\"footer-contact__locality locality\">г. Москва, М. Станкостроительная</span><br>\n                                <span class=\"footer-contact__street-address street-address\">ул. Станкостроителей, д. 25</span>\n                            </div>\n\n                            <p class=\"footer-contact__email\">\n                                <a class=\"email\" href=\"mailto:example@domain.com\">Написать письмо</a>\n                            </p>\n                        </div>\n                    </div>\n\n                    {{MENU_FOOTER}}\n\n                    <div class=\"footer-share\">\n                        <p class=\"footer-share__item\">\n                            Поделись с друзьями\n                        </p>\n\n                        <div class=\"ya-share2\" data-services=\"collections,vkontakte,facebook,odnoklassniki,twitter\" data-counter=\"\"></div>\n                    </div>\n                </div>\n            </footer>','0','1525163280','1525268879','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('5','MAIN_INDEX','Основное содержимое сайта главной страницы','2','none','7','0','            <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">[*pagetitle*]</h1>\n                    </header>\n                    \n                    {{SECTION_PRODUCT_LIST}}\n                </section>    \n            </main>','0','1525163419','1525255833','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('11','WRAPPER_ABOUT','Обертка содержимого страницы о компании','2','none','7','0','        <div class=\"wrapper\">           \n           	{{HEADER}}\n			{{MAIN_ABOUT}}\n			{{FOOTER}}\n        </div>','0','0','1525167655','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('12','MAIN_ABOUT','Основное содержимое сайта о компании','2','none','7','0','            <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">[*pagetitle*]</h1>\n                    </header>\n                    \n                    <h2><em>Интернет-магазин Enteley.ru это:</em></h2>\n                    \n                    <p>- Легендарные ткани класса premium luxe и haute couture</p>\n                    <p>- Ежедневное обновление каталога</p>\n                    <p>- Лучшие и самые новые коллекции</p>\n                    <p>- Эксклюзивные ткани</p>\n                    <p>- Регулярные акции и скидки</p>\n                    <p>- Помощь в подборе тканей</p>\n                    <p>Компания \"Enteley\" насчитывает почти 20 лет успешной работы. Мы постоянно двигаемся вперед, покоряя всё новые вершины. Присоединяйтесь!</p>      \n                </section>    \n            </main>','0','1525167754','1525255807','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('13','MAIN_OPLATA_DOSTAVKA','Основное содержимое сайта оплата и доставка','2','none','7','0','            <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">[*pagetitle*]</h1>\n                    </header>\n					\n					<div class=\"main__two-column\">\n                        <h4>Оплата</h4>\n                        <p><strong>Оплата банковским переводом (квитанцией)</strong></p>\n                        <p>Перевод осуществляется по реквизитам через онлайн сервис Вашего банка или по квитанции, которую можно оплатить в любом банке.\n                            Обычно банк взимает комиссию 0,5-2% от суммы перевода. Срок зачисления денежных средств 1-2 раб.дня. \n                            Реквизиты для оплаты Вы найдете в разделе Контакты, в назначении платежа указывайте номер заказа.\n                            Банковскую квитанцию можно сформировать в личном кабинете в разделе \"Мои заказы\". \n                            По Вашей просьбе мы можем отправить квитанцию на электронную почту или мессенджерами Viber, WhatsApp</p>\n                        \n                        <p><strong>Оплата заказа пластиковой картой</strong></p>\n                        <p>Оплата происходит через провайдера электронных платежей Ассист, который обеспечивает безопасность. <a href=\"#\"> Подробнее...</a>\n                            Оплата происходит мгновенно, без комиссии. Возврат денежных средств производится на карту, с которой был произведен платеж.</p>\n                        <p>Доступные платежные системы: <em>Visa</em>, <em>Maestro</em>, <em>MasterCard</em></p>\n                        <p><strong>Оплата наложенным платежом</strong></p>\n                        <p>Данный метод оплаты доступен только для заказов доставляемых курьерской службой СДЭК, сумма которых не превышает 10 000 руб. \n                            Оплачивается заказ и стоимость доставки во время получения посылки. За услугу наложенный платеж взимается комиссия 3-4% от стоимости заказа.</p>\n                        <p><strong>Оплата по счету</strong></p>\n                        <p>Данный метод оплаты доступен для юридических лиц. Для выставления счета и заключения договора необходимо выслать на электронную почту example@domain.com реквизиты и документы (свидетельство, устав и т.п.). \n                            При поступлении денежных средств на наш расчетный счет происходит доставка товара на указанный адрес. Вместе с заказом Вы получите накладную и счет-фактуру.</p>\n                        \n                        <h4>Доставка</h4>\n                        <p>Осуществляется из городов Москва или Санкт-Петербург Почтой России или Курьерской Службой СДЭК.\n                            Доставка оплачивается при получении.\n                            Предварительный расчет:\n                            На сайте \"Почта России\"</p>\n                        <p><strong>Самовывоз из салонов </strong></p>\n                        <p>По согласованию с менеджером и при 100% предоплате. Все адреса салонов, домов тканей и пунктов выдачи можно посмотреть в разделе Контакты</p>\n                        <p><strong>В СНГ и другие страны</strong></p>\n                        <p>В Белоруссию и Казахстан доставка осуществляется курьерской службой СДЭК или Почтой России только после предварительной оплаты за товар и доставку, стоимость которой рассчитывает менеджер. Информация по другим странам предоставляется по запросу</p>\n                        <p><strong>Сроки и стоимость</strong></p>\n                        <p>Срок доставки и стоимость зависит от региона, в котором Вы проживаете, от веса посылки, от тарифов на услуги страхование и наложенного платежа, от способа доставки. Значительную экономию даёт 100% предоплата, так как услуга наложенного платежа составляет 3-4% от стоимости товара.\n                            Полный расчет предоставит и согласует с Вами менеджер перед отправкой.</p>\n                        <p><strong>Получение</strong></p>\n                        <p>Независимо от способа доставки Вы сможете отследить местонахождение свой посылки по номеру отправления в личном кабинете. При получении необходимо удостоверение личности</p>\n                        <p><strong>Ответственность</strong></p>\n                        <p>Ответственность за доставку и сохранность несет логистическая компания с момента передачи ей товара. Все отправления подлежат обязательному страхованию, стоимость страховки включена в стоимость доставки</p>\n                    </div>\n                </section>    \n            </main>','0','0','1525255849','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('14','WRAPPER_PRAVILA','Обертка содержимого памятка покупателю','2','none','7','0','        <div class=\"wrapper\">           \n           	{{HEADER}}\n			{{MAIN_PRAVILA}}\n			{{FOOTER}}\n        </div>','0','0','1525168642','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('15','WRAPPER_OPLATA_DOSTAVKA','Обертка содержимого оплата и доставка','2','none','7','0','        <div class=\"wrapper\">           \n           	{{HEADER}}\n			{{MAIN_OPLATA_DOSTAVKA}}\n			{{FOOTER}}\n        </div>','0','0','1525169885','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('16','MAIN_PRAVILA','Основное содержимое сайта памятка покупателю','2','none','7','0','            <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">[*pagetitle*]</h1>\n                    </header>\n					\n					<p>Сомневаетесь, сколько метров ткани купить? Загляните в <a href=\"#\">эту памятку</a>, и Вы никогда не ошибетесь!</p>\n                    <p>Узнайте о нашей <a href=\"#\">системе скидок!</a></p>\n                    <p>Не понимаете, насколько плотная ткань? Тогда прочитайте информацию <a href=\"#\">о плотности тканей!</a></p>\n                    <p>Как продлить жизнь тканям? Прочитайте <a href=\"#\">правила по уходу</a>!</p>\n                    <p>Как заказать <a href=\"#\">образцы тканей</a>?</p>\n                    <p>Остались ещё вопросы? Ответы на самые распространенные вопросы <a href=\"#\">здесь</a>!</p>\n                </section>    \n            </main>','0','0','1525255864','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('17','WRAPPER_STOCK','Обертка содержимого страницы акции','2','none','7','0','        <div class=\"wrapper\">           \n           	{{HEADER}}\n			{{MAIN_STOCK}}\n			{{FOOTER}}\n        </div>','0','0','1525170358','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('18','MAIN_STOCK','Основное содержимое сайта акции','2','none','7','0','            <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">[*pagetitle*]</h1>\n                    </header>\n					\n					<h2>What is Lorem Ipsum?</h2>\n                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n                    Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.\n                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                    <h2>Where does it come from?</h2>\n                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n                    Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.\n                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n                </section>    \n            </main>','0','0','1525255876','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('19','WRAPPER_CONTACT','Обертка содержимого страницы контакты','2','none','7','0','        <div class=\"wrapper\">           \n           	{{HEADER}}\n			{{MAIN_CONTACT}}\n			{{FOOTER}}\n        </div>','0','0','1525170524','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('20','MAIN_CONTACT','Основное содержимое сайта контакты','2','none','7','0','            <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">[*pagetitle*]</h1>\n                    </header>\n					\n					<p><strong>+7 (495) 327-81-20</strong></p>\n                    <p><em>Понедельник-воскресенье с 9.00 до 20.30</em></p>\n                    <p>Елена, Ольга (Москва) - менеджеры по работе с клиентами</p>\n                    <p>\n                        <a class=\"email\" href=\"mailto:manager-example@domain.com\">manager-example@domain.com</a>\n                    </p>\n                    <p>\n                        г. Москва, М. Станкостроительная ул. Станкостроителей, д. 25\n                    </p>\n                </section>    \n            </main>','0','0','1525255818','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('50','LIST_SUMMARY_FASHION','Шаблон для вывода списка товаров (мода)','2','none','9','0','<div class=\"main-fashion__image\">\n	<img src=\"[+image+]\" alt=\"Enteley\" class=\"main-fashion__img\"/>\n</div>','0','0','1525430825','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('41','CONTACT_FORM_REPORT_TPL','Основной шаблон письма заказать звонок на почту','2','none','8','0','<p>Это сообщение было отправлено посетителем по имени [+name.value+] с помощью формы заказать звонок.</p>\n<p>Отправленная информация:</p>\n<ul>\n	<li><b>Имя:</b> [+name.value+]</li>\n	<li><b>Телефон:</b> [+phone.value+]</li>\n</ul>\n','0','0','1525416792','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('6','WRAPPER_INDEX','Обертка содержимого главной страницы','2','none','7','0','        <div class=\"wrapper\">           \n           	{{HEADER}}\n			{{MAIN_INDEX}}\n			{{FOOTER}}\n        </div>','0','1525163743','1525167468','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('35','MODAL_CONTACT','Модальное окно заказать звонок','2','none','8','0','<div id=\"myModal\" class=\"modal\">\n	<div class=\"modal__dialog\">\n		<div class=\"modal__content\">\n			<div class=\"modal__header\">\n				<h3 class=\"modal__title\">Заказать звонок</h3>\n				<a href=\"[~[*id*]~]#close\" title=\"Закрыть\" class=\"modal__close\">×</a>\n			</div>\n			<div class=\"modal__body\">\n				<div id=\"frmwrapper\">\n					[!FormLister?\n						&formid=`contact-form`\n						&formMethod=`post`\n						&rules=`{\n									\"name\": {\n										\"required\":\"Введите имя\"\n									},\n									\"phone\": {\n										\"required\":\"Введите номер телефона\",\n										\"phone\":\"Введите номер телефона правильно\"\n									}\n								}`\n						&protectSubmit=`1`\n						&to=`kiddrtest@gmail.com`\n						&from=`kiddrtest@gmail.com`\n						&errorClass=`error`\n						&requiredClass=`error`\n						&formTpl=`CONTACT_FORM_TPL`\n						&messagesOuterTpl=`CONTACT_FORM_MESSAGE_OUTER_TPL`\n						&errorTpl=`CONTACT_FORM_ERROR_TPL`\n						&subjectTpl=`CONTACT_FORM_SUBJECT_TPL`\n						&reportTpl=`CONTACT_FORM_REPORT_TPL`\n						&successTpl=`CONTACT_FORM_SUCCESS_TPL`\n					!]\n				</div>\n			</div>\n			<div class=\"modal__footer\">\n				<a href=\"[~[*id*]~]#close\" title=\"Закрыть\" class=\"btn modal__close\">Закрыть</a>\n			</div>\n		</div>\n	</div>\n</div>','0','1525338258','1525416797','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('49','LIST_SUMMARY_PRODUCT','Шаблон для вывода списка товаров','2','none','9','0','<div class=\"main-product__item\">\n	<div class=\"main-product__image\">\n		<img src=\"[+image+]\" alt=\"Enteley\" class=\"main-product__img\"/>\n\n		<p class=\"main-product__price\">[+price+] Р</p>\n	</div>\n\n	<div class=\"main-product__description\">\n		<span class=\"btn main-product__btn\" role=\"button\">\n			<svg class=\"icon header__icon\">\n				<use xlink:href=\"#icon-basket\"></use>\n			</svg>\n			Купить товар\n		</span>\n\n		<div class=\"main-product_detail\">\n			<p>Код: [+code+]</p>\n			<p>Производитель: [+manufacturer+]</p>\n			<p>Состав: [+composition+]</p>\n		</div>\n	</div>\n</div>','0','1525426868','1525430828','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('47','CONTACT_FORM_SUCCESS_TPL','Шаблон сообщения об успешной отправке писем заказать звонок','2','none','8','0','<h3>Спасибо!</h3>\n<p>Наш менеджер свяжется с вами в ближайшее время.</p>','0','0','1525415167','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('8','ICONS','Иконки для сайта','2','none','7','0','        <div class=\"hidden\">\n            <?xml version=\"1.0\" encoding=\"utf-8\"?>\n            <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                <symbol viewBox=\"337 -266.1 928.6 786.1\" id=\"icon-basket\" xmlns=\"http://www.w3.org/2000/svg\">\n                    <path d=\"M672.8 398c-15.3-12.7-32-19.7-50-21s-34.7 5.7-50 21-22.7 32-22 50 8 34.7 22 50c14 15.3 30.7 22.7 50 22 19.3-.7 36-8 50-22s21-30.7 21-50c1.3-20.6-5.7-37.3-21-50zm500 0c-15.3-12.7-32-19.7-50-21s-34.7 5.7-50 21c-15.3 15.3-22.7 32-22 50 .7 18 8 34.7 22 50 14 15.3 30.7 22.7 50 22 19.3-.7 36-8 50-22s21-30.7 21-50c1.3-20.6-5.7-37.3-21-50zm82-582c-7.3-7.3-15.7-11-25-11h-670c.7-2-.3-6.7-3-14s-3.7-13-3-17-.7-8.7-4-14-6-10-8-14-5.3-7-10-9-10-3-16-3h-143c-8.7-.7-17 2.7-25 10s-11.7 16-11 26c.7 10 4.3 18.3 11 25 6.7 6.7 15 10 25 10h114l98 459c0 2-2.7 7.7-8 17s-9.3 16.7-12 22-5.7 12-9 20-5 14-5 18c0 9.3 3.7 17.7 11 25s15.7 11 25 11h571c11.3 0 20-3.7 26-11s9.3-15.7 10-25c.7-9.3-2.7-17.7-10-25-7.3-7.3-16-11-26-11h-513c8.7-18 13-29.7 13-35 0-4-2.3-17.3-7-40l583-68c8-1.3 15.3-5.3 22-12s10-14.3 10-23v-286c0-9.3-3.7-17.6-11-25z\"/>\n                </symbol>\n\n                <symbol viewBox=\"266 -266 786 786\" id=\"icon-phone\" xmlns=\"http://www.w3.org/2000/svg\">\n                    <path d=\"M1050.1 343c-2-6.7-16-16.3-42-29-6.7-4-16.7-9.7-30-17s-25.3-14-36-20-20.7-11.7-30-17c-1.3-1.3-6-4.7-14-10s-14.7-9.3-20-12-10.3-4-15-4c-8-.7-17.3 4.7-28 16s-21 23-31 35-20 23.3-30 34-18.3 16-25 16c-4.7.7-9 0-13-2s-7.7-3.7-11-5-8-4-14-8-9.3-6-10-6c-50.7-28-94.3-60.3-131-97s-69-80.7-97-132c0 .7-2.3-2.7-7-10s-7.3-12-8-14-2-5.7-4-11-3-9.7-3-13c.7-6.7 6-15 16-25s21.3-20 34-30 24.3-20.3 35-31 16-19.7 16-27c0-5.3-1.3-10.7-4-16s-6.7-12-12-20-8.7-12.7-10-14c-4.7-10-10.3-20-17-30-6.7-10-13.3-21.7-20-35s-12-23.7-16-31c-13.3-26-23.3-40-30-42-2.7-1.3-6.7-2-12-2-10 0-23 2-39 6s-28.7 8-38 12c-18.7 7.3-38.3 30-59 68-19.3 34.7-29 69.3-29 104-.7 10 0 19.7 2 29s4.3 20 7 32 5.3 20.7 8 26c2.7 5.3 6.7 15.7 12 31s8.7 24.7 10 28c12.7 36 28 68.3 46 97 28.7 48 68.7 97.3 120 148s100.7 90.7 148 120c28.7 18 61 33.7 97 47 4 1.3 13.3 4.7 28 10s25 9 31 11 14.7 4.7 26 8 22 5.7 32 7 20 2 30 2c34 0 68.3-9.3 103-28 38-21.3 60.7-41.3 68-60 4-9.3 8-22 12-38s6-29 6-39c0-5.3-.7-9.3-2-12z\"/>\n                </symbol>\n            </svg>\n        </div>','0','1525164045','1525164045','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('9','MAIN_SCRIPT','Скрипты вызова модального окна. Скрипты кнопок \"Поделиться\"','2','none','7','0','<script src=\"http://yastatic.net/es5-shims/0.0.2/es5-shims.min.js\"></script>\n<script src=\"http://yastatic.net/share2/share.js\"></script>','0','1525164253','1525337037','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('10','SECTION_PRODUCT_LIST','Список товаров','2','none','7','0','<section class=\"main__inner\">\n	<div class=\"main-product\">\n		[!DocLister?\n		&controller=`onetable`\n		&table=`products`\n		&idType=`documents`\n		&orderBy=`id DESC`\n		&display=`6`\n		&ignoreEmpty=`1`\n		&tpl=`LIST_SUMMARY_PRODUCT`\n		&prepare=`prepare_img_product`\n		!]\n	</div> \n\n	<div class=\"main-fashion\">\n		[!DocLister?\n		&controller=`onetable`\n		&table=`fashion`\n		&idType=`documents`\n		&orderBy=`id DESC`\n		&display=`1`\n		&tpl=`LIST_SUMMARY_FASHION`\n		&ignoreEmpty=`1`\n		&prepare=`prepare_img_fashion`\n		!]\n	</div>\n</section>\n\n<section class=\"main__inner flex-reverse\">\n	<div class=\"main-product\">\n		[!DocLister?\n		&controller=`onetable`\n		&table=`products`\n		&idType=`documents`\n		&orderBy=`id DESC`\n		&display=`6`							\n		&offset=`6`\n		&ignoreEmpty=`1`\n		&tpl=`LIST_SUMMARY_PRODUCT`\n		&prepare=`prepare_img_product`\n		!]\n	</div>\n	<div class=\"main-fashion\">\n		[!DocLister?\n		&controller=`onetable`\n		&table=`fashion`\n		&idType=`documents`\n		&orderBy=`id DESC`\n		&display=`1`\n		&offset=`1`\n		&tpl=`LIST_SUMMARY_FASHION`\n		&ignoreEmpty=`1`\n		&prepare=`prepare_img_fashion`\n		!]\n	</div>\n</section>\n\n<section class=\"main__inner\">\n	<div class=\"main-product\">\n		[!DocLister?\n		&controller=`onetable`\n		&table=`products`\n		&idType=`documents`\n		&orderBy=`id DESC`\n		&display=`6`\n		&offset=`12`\n		&ignoreEmpty=`1`\n		&tpl=`LIST_SUMMARY_PRODUCT`\n		&prepare=`prepare_img_product`\n		!]\n	</div> \n\n	<div class=\"main-fashion\">\n		[!DocLister?\n		&controller=`onetable`\n		&table=`fashion`\n		&idType=`documents`\n		&orderBy=`id DESC`\n		&display=`1`\n		&offset=`2`\n		&tpl=`LIST_SUMMARY_FASHION`\n		&ignoreEmpty=`1`\n		&prepare=`prepare_img_fashion`\n		!]\n	</div>\n</section>    ','0','1525167010','1525608807','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('45','CONTACT_FORM_ERROR_TPL','Шаблон для вывода сообщений валидатора форма заказать звонок','2','none','8','0','<div class=\"error\">[+message+]</div>','0','0','1525415173','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('48','CONTACT_FORM_MESSAGE_OUTER_TPL','Шаблон-обертка для группы произвольных сообщений форма заказать звонок','2','none','8','0','<div class=\"error\">[+messages+]</div>','0','0','1525415200','0');

INSERT INTO `alv1_site_htmlsnippets` VALUES ('43','CONTACT_FORM_TPL','Шаблон формы заказать звонок','2','none','8','0','<form action=\"#myModal\" method=\"post\" class=\"modal__form\">\n	<input type=\"hidden\" name=\"formid\" value=\"contact-form\" />\n	<fieldset class=\"modal__form-fieldset\">\n		<legend class=\"modal__form-legend\">Контактная информация</legend>\n\n		<p class=\"modal__form-inner [+name.errorClass+] [+name.requiredClass+]\">\n			<label class=\"modal__form-label\" for=\"name\">Имя <em>*</em></label>\n			<input type=\"text\" class=\"modal__form-input\" id=\"name\" name=\"name\" placeholder=\"Введите имя\" value=\"[+name.value+]\">\n			<span class=\"error\">[+name.error+]</span>\n		</p>\n		\n		<p class=\"modal__form-inner [+phone.errorClass+] [+phone.requiredClass+]\">\n			<label class=\"modal__form-label\" for=\"phone\">Телефон <em>*</em></label>\n			<input type=\"tel\" class=\"modal__form-input [+phone.errorClass+] [+phone.requiredClass+]\" id=\"phone\" name=\"phone\" placeholder=\"Введите номер телефона\" value=\"[+phone.value+]\">\n			<span class=\"error\">[+phone.error+]</span>\n		</p>\n\n		<p class=\"modal__form-inner\">\n			<input class=\"btn__lr modal__form-btn\" type=\"submit\" value=\"Отправить\">\n		</p>\n	</fieldset>\n	<p>[+form.messages+]</p>\n</form>','0','0','1525447437','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_module_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_module_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_module_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` int(11) NOT NULL DEFAULT '0',
  `usergroup` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Module users group access permission';

#
# Dumping data for table `alv1_site_module_access`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_site_module_depobj`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_module_depobj`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_module_depobj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` int(11) NOT NULL DEFAULT '0',
  `resource` int(11) NOT NULL DEFAULT '0',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '10-chunks, 20-docs, 30-plugins, 40-snips, 50-tpls, 60-tvs',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Module Dependencies';

#
# Dumping data for table `alv1_site_module_depobj`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_site_modules`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_modules`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '0',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `wrap` tinyint(4) NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'url to module icon',
  `enable_resource` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'enables the resource file feature',
  `resourcefile` varchar(255) NOT NULL DEFAULT '' COMMENT 'a physical link to a resource file',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `guid` varchar(32) NOT NULL DEFAULT '' COMMENT 'globally unique identifier',
  `enable_sharedparams` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text,
  `modulecode` mediumtext COMMENT 'module boot up code',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Site Modules';

#
# Dumping data for table `alv1_site_modules`
#

INSERT INTO `alv1_site_modules` VALUES ('1','Doc Manager','<strong>1.1</strong> Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions','0','0','4','0','0','','0','','0','0','docman435243542tf542t5t','1','',' \n/**\n * Doc Manager\n * \n * Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions\n * \n * @category	module\n * @version 	1.1\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@guid docman435243542tf542t5t	\n * @internal	@shareparams 1\n * @internal	@dependencies requires files located at /assets/modules/docmanager/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  09/04/2016\n */\n\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/docmanager.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_frontend.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_backend.class.php\');\n\n$dm = new DocManager($modx);\n$dmf = new DocManagerFrontend($dm, $modx);\n$dmb = new DocManagerBackend($dm, $modx);\n\n$dm->ph = $dm->getLang();\n$dm->ph[\'theme\'] = $dm->getTheme();\n$dm->ph[\'ajax.endpoint\'] = MODX_SITE_URL.\'assets/modules/docmanager/tv.ajax.php\';\n$dm->ph[\'datepicker.offset\'] = $modx->config[\'datepicker_offset\'];\n$dm->ph[\'datetime.format\'] = $modx->config[\'datetime_format\'];\n\nif (isset($_POST[\'tabAction\'])) {\n    $dmb->handlePostback();\n} else {\n    $dmf->getViews();\n    echo $dm->parseTemplate(\'main.tpl\', $dm->ph);\n}');

INSERT INTO `alv1_site_modules` VALUES ('2','Extras','<strong>0.1.3</strong> first repository for Evolution CMS','0','0','4','0','0','','0','','0','0','store435243542tf542t5t','1','',' \n/**\n * Extras\n * \n * first repository for Evolution CMS\n * \n * @category	module\n * @version 	0.1.3\n * @internal	@properties\n * @internal	@guid store435243542tf542t5t	\n * @internal	@shareparams 1\n * @internal	@dependencies requires files located at /assets/modules/store/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  25/11/2016\n */\n\n//AUTHORS: Bumkaka & Dmi3yy \ninclude_once(\'../assets/modules/store/core.php\');');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_plugin_events`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_plugin_events`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_plugin_events` (
  `pluginid` int(10) NOT NULL,
  `evtid` int(10) NOT NULL DEFAULT '0',
  `priority` int(10) NOT NULL DEFAULT '0' COMMENT 'determines plugin run order',
  PRIMARY KEY (`pluginid`,`evtid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Links to system events';

#
# Dumping data for table `alv1_site_plugin_events`
#

INSERT INTO `alv1_site_plugin_events` VALUES ('1','23','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('1','29','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('1','35','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('1','41','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('1','47','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('1','73','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('1','88','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','25','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','27','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','37','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','39','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','43','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','45','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','49','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','51','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','55','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','57','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','75','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','77','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','206','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','210','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('2','211','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('3','34','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('3','35','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('3','36','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('3','40','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('3','41','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('3','42','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('4','80','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('4','81','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('4','93','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','28','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','29','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','30','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','31','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','35','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','53','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('5','205','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('6','202','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('7','3','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('7','13','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('7','28','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('7','31','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('7','92','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','3','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','20','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','85','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','87','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','88','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','91','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('8','92','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('9','100','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('10','70','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('10','202','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('10','1000','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('11','4','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('11','79','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('11','90','0');

INSERT INTO `alv1_site_plugin_events` VALUES ('11','1000','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_plugins`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_plugins`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_plugins` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Plugin',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `plugincode` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COMMENT 'Default Properties',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the plugin',
  `moduleguid` varchar(32) NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Contains the site plugins.';

#
# Dumping data for table `alv1_site_plugins`
#

INSERT INTO `alv1_site_plugins` VALUES ('1','CodeMirror','<strong>1.5</strong> JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.33 (released on 21-12-2017)','0','4','0','\n/**\n * CodeMirror\n *\n * JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.33 (released on 21-12-2017)\n *\n * @category    plugin\n * @version     1.5\n * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @package     evo\n * @internal    @events OnDocFormRender,OnChunkFormRender,OnModFormRender,OnPluginFormRender,OnSnipFormRender,OnTempFormRender,OnRichTextEditorInit\n * @internal    @modx_category Manager and Admin\n * @internal    @properties &theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &darktheme=Dark Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;one-dark &fontSize=Font-size;list;10,11,12,13,14,15,16,17,18;14 &lineHeight=Line-height;list;1,1.1,1.2,1.3,1.4,1.5;1.3 &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250\n * @internal    @installset base\n * @reportissues https://github.com/evolution-cms/evolution/issues/\n * @documentation Official docs https://codemirror.net/doc/manual.html\n * @author      hansek from http://www.modxcms.cz\n * @author      update Mihanik71\n * @author      update Deesen\n * @author      update 64j\n * @lastupdate  08-01-2018\n */\n\n$_CM_BASE = \'assets/plugins/codemirror/\';\n\n$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;\n\nrequire(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');','0','{\"theme\":[{\"label\":\"Theme\",\"type\":\"list\",\"value\":\"default\",\"options\":\"default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light\",\"default\":\"default\",\"desc\":\"\"}],\"darktheme\":[{\"label\":\"Dark Theme\",\"type\":\"list\",\"value\":\"one-dark\",\"options\":\"default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,one-dark,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light\",\"default\":\"one-dark\",\"desc\":\"\"}],\"fontSize\":[{\"label\":\"Font-size\",\"type\":\"list\",\"value\":\"14\",\"options\":\"10,11,12,13,14,15,16,17,18\",\"default\":\"14\",\"desc\":\"\"}],\"lineHeight\":[{\"label\":\"Line-height\",\"type\":\"list\",\"value\":\"1.3\",\"options\":\"1,1.1,1.2,1.3,1.4,1.5\",\"default\":\"1.3\",\"desc\":\"\"}],\"indentUnit\":[{\"label\":\"Indent unit\",\"type\":\"int\",\"value\":\"4\",\"default\":\"4\",\"desc\":\"\"}],\"tabSize\":[{\"label\":\"The width of a tab character\",\"type\":\"int\",\"value\":\"4\",\"default\":\"4\",\"desc\":\"\"}],\"lineWrapping\":[{\"label\":\"lineWrapping\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"matchBrackets\":[{\"label\":\"matchBrackets\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"activeLine\":[{\"label\":\"activeLine\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"emmet\":[{\"label\":\"emmet\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"search\":[{\"label\":\"search\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"indentWithTabs\":[{\"label\":\"indentWithTabs\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"undoDepth\":[{\"label\":\"undoDepth\",\"type\":\"int\",\"value\":\"200\",\"default\":\"200\",\"desc\":\"\"}],\"historyEventDelay\":[{\"label\":\"historyEventDelay\",\"type\":\"int\",\"value\":\"1250\",\"default\":\"1250\",\"desc\":\"\"}]}','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('2','ElementsInTree','<strong>1.5.9</strong> Get access to all Elements and Modules inside Manager sidebar','0','4','0','require MODX_BASE_PATH.\'assets/plugins/elementsintree/plugin.elementsintree.php\';\n','0','{\"adminRoleOnly\":[{\"label\":\"Administrators only\",\"type\":\"list\",\"value\":\"yes\",\"options\":\"yes,no\",\"default\":\"yes\",\"desc\":\"\"}],\"treeButtonsInTab\":[{\"label\":\"Tree buttons in tab\",\"type\":\"list\",\"value\":\"yes\",\"options\":\"yes,no\",\"default\":\"yes\",\"desc\":\"\"}]}','1','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('3','FileSource','<strong>0.1</strong> Save snippet and plugins to file','0','4','0','require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';','0','','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('4','Forgot Manager Login','<strong>1.1.7</strong> Resets your manager login when you forget your password via email confirmation','0','4','0','require MODX_BASE_PATH.\'assets/plugins/forgotmanagerlogin/plugin.forgotmanagerlogin.php\';','0','','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('5','ManagerManager','<strong>0.6.3</strong> Customize the EVO Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.','0','4','0','\n/**\n * ManagerManager\n *\n * Customize the EVO Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.\n *\n * @category plugin\n * @version 0.6.3\n * @license http://creativecommons.org/licenses/GPL/2.0/ GNU Public License (GPL v2)\n * @internal @properties &remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules\n * @internal @events OnDocFormRender,OnDocFormPrerender,OnBeforeDocFormSave,OnDocFormSave,OnDocDuplicate,OnPluginFormRender,OnTVFormRender\n * @internal @modx_category Manager and Admin\n * @internal @installset base\n * @internal @legacy_names Image TV Preview, Show Image TVs\n * @reportissues https://github.com/DivanDesign/MODXEvo.plugin.ManagerManager/\n * @documentation README [+site_url+]assets/plugins/managermanager/readme.html\n * @documentation Official docs http://code.divandesign.biz/modx/managermanager\n * @link        Latest version http://code.divandesign.biz/modx/managermanager\n * @link        Additional tools http://code.divandesign.biz/modx\n * @link        Full changelog http://code.divandesign.biz/modx/managermanager/changelog\n * @author      Inspired by: HideEditor plugin by Timon Reinhard and Gildas; HideManagerFields by Brett @ The Man Can!\n * @author      DivanDesign studio http://www.DivanDesign.biz\n * @author      Nick Crossland http://www.rckt.co.uk\n * @author      Many others\n * @lastupdate  22/01/2018\n */\n\n// Run the main code\ninclude($modx->config[\'base_path\'].\'assets/plugins/managermanager/mm.inc.php\');\n','0','{\"remove_deprecated_tv_types_pref\":[{\"label\":\"Remove deprecated TV types\",\"type\":\"list\",\"value\":\"yes\",\"options\":\"yes,no\",\"default\":\"yes\",\"desc\":\"\"}],\"config_chunk\":[{\"label\":\"Configuration Chunk\",\"type\":\"text\",\"value\":\"mm_rules\",\"default\":\"mm_rules\",\"desc\":\"\"}]}','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('6','OutdatedExtrasCheck','<strong>1.4.0</strong> Check for Outdated critical extras not compatible with EVO 1.4.0','0','4','0','/**\n * OutdatedExtrasCheck\n *\n * Check for Outdated critical extras not compatible with EVO 1.4.0\n *\n * @category	plugin\n * @version     1.4.0 \n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @package     evo\n * @author      Author: Nicola Lambathakis\n * @internal    @events OnManagerWelcomeHome\n * @internal    @properties &wdgVisibility=Show widget for:;menu;All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly;All &ThisRole=Run only for this role:;string;;;(role id) &ThisUser=Run only for this user:;string;;;(username) &DittoVersion=Min Ditto version:;string;2.1.3 &EformVersion=Min eForm version:;string;1.4.9 &AjaxSearchVersion=Min AjaxSearch version:;string;1.11.0 &WayfinderVersion=Min Wayfinder version:;string;2.0.5 &WebLoginVersion=Min WebLogin version:;string;1.2 &WebSignupVersion=Min WebSignup version:;string;1.1.2 &WebChangePwdVersion=Min WebChangePwd version:;string;1.1.2 &BreadcrumbsVersion=Min Breadcrumbs version:;string;1.0.5 &ReflectVersion=Min Reflect version:;string;2.2 &JotVersion=Min Jot version:;string;1.1.5 &MtvVersion=Min multiTV version:;string;2.0.13 &badthemes=Outdated Manager Themes:;string;MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle\n * @internal    @modx_category Manager and Admin\n * @internal    @installset base\n * @internal    @disabled 0\n */\n\n// get manager role check\n$internalKey = $modx->getLoginUserID();\n$sid = $modx->sid;\n$role = $_SESSION[\'mgrRole\'];\n$user = $_SESSION[\'mgrShortname\'];\n// show widget only to Admin role 1\nif(($role!=1) AND ($wdgVisibility == \'AdminOnly\')) {}\n// show widget to all manager users excluded Admin role 1\nelse if(($role==1) AND ($wdgVisibility == \'AdminExcluded\')) {}\n// show widget only to \"this\" role id\nelse if(($role!=$ThisRole) AND ($wdgVisibility == \'ThisRoleOnly\')) {}\n// show widget only to \"this\" username\nelse if(($user!=$ThisUser) AND ($wdgVisibility == \'ThisUserOnly\')) {}\nelse {\n// get plugin id and setting button\n$result = $modx->db->select(\'id\', $this->getFullTableName(\"site_plugins\"), \"name=\'{$modx->event->activePlugin}\' AND disabled=0\");\n$pluginid = $modx->db->getValue($result);\nif($modx->hasPermission(\'edit_plugin\')) {\n$button_pl_config = \'<a data-toggle=\"tooltip\" href=\"javascript:;\" title=\"\' . $_lang[\"settings_config\"] . \'\" class=\"text-muted pull-right\" onclick=\"parent.modx.popup({url:\\\'\'. MODX_MANAGER_URL.\'?a=102&id=\'.$pluginid.\'&tab=1\\\',title1:\\\'\' . $_lang[\"settings_config\"] . \'\\\',icon:\\\'fa-cog\\\',iframe:\\\'iframe\\\',selector2:\\\'#tabConfig\\\',position:\\\'center center\\\',width:\\\'80%\\\',height:\\\'80%\\\',hide:0,hover:0,overlay:1,overlayclose:1})\" ><i class=\"fa fa-cog fa-spin-hover\" style=\"color:#FFFFFF;\"></i> </a>\';\n}\n$modx->setPlaceholder(\'button_pl_config\', $button_pl_config);\n//plugin lang\n$_oec_lang = array();\n$plugin_path = $modx->config[\'base_path\'] . \"assets/plugins/extrascheck/\";\ninclude($plugin_path . \'lang/english.php\');\nif (file_exists($plugin_path . \'lang/\' . $modx->config[\'manager_language\'] . \'.php\')) {\ninclude($plugin_path . \'lang/\' . $modx->config[\'manager_language\'] . \'.php\');\n}\n//run the plugin\n// get globals\nglobal $modx,$_lang;\n//function to extract snippet version from description <strong></strong> tags \nif (!function_exists(\'getver\')) {\nfunction getver($string, $tag)\n{\n$content =\"/<$tag>(.*?)<\\/$tag>/\";\npreg_match($content, $string, $text);\nreturn $text[1];\n	}\n}\n$e = &$modx->Event;\n$EVOversion = $modx->config[\'settings_version\'];\n$output = \'\';\n//get extras module id for the link\n$modtable = $modx->getFullTableName(\'site_modules\');\n$getExtra = $modx->db->select( \"id, name\", $modtable, \"name=\'Extras\'\" );\nwhile( $row = $modx->db->getRow( $getExtra ) ) {\n$ExtrasID = $row[\'id\'];\n}\n//check outdated files\n//ajax index\n$indexajax = \"../index-ajax.php\";\nif (file_exists($indexajax)){\n    $output .= \'<div class=\"widget-wrapper alert alert-danger\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>index-ajax.php</b> \'.$_oec_lang[\'not_used\'].\' <b>Evolution \'.$EVOversion.\'</b>.  \'.$_oec_lang[\'if_dont_use\'].\', \'.$_oec_lang[\'please_delete\'].\'.</div>\';\n}\n//check outdated default manager themes\n$oldthemes = explode(\",\",\"$badthemes\");\nforeach ($oldthemes as $oldtheme){\n	if (file_exists(\'media/style/\'.$oldtheme)){\n    $output .= \'<div class=\"widget-wrapper alert alert-danger\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\'.$oldtheme.\'</b> \'.$_lang[\"manager_theme\"].\',  \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>.   \'.$_oec_lang[\'please_delete\'].\' \'.$_oec_lang[\'from_folder\'].\' \' . MODX_MANAGER_PATH . \'media/style/.</div>\';\n}\n}	\n//get site snippets table\n$table = $modx->getFullTableName(\'site_snippets\');\n//check ditto\n//get min version from config\n$minDittoVersion = $DittoVersion;\n//search the snippet by name\n$CheckDitto = $modx->db->select( \"id, name, description\", $table, \"name=\'Ditto\'\" );\nif($CheckDitto != \'\'){\nwhile( $row = $modx->db->getRow( $CheckDitto ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_ditto_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_ditto_version,$minDittoVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_ditto_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minDittoVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>DocLister</b></div>\';\n		}\n	}\n} \n//end check ditto\n\n//check eform\n//get min version from config\n$minEformVersion = $EformVersion;\n//search the snippet by name\n$CheckEform = $modx->db->select( \"id, name, description\", $table, \"name=\'eForm\'\" );\nif($CheckEform != \'\'){\nwhile( $row = $modx->db->getRow( $CheckEform ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Eform_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Eform_version,$minEformVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Eform_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minEformVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check eform\n	\n//check AjaxSearch\n//get min version from config\n$minAjaxSearchVersion = $AjaxSearchVersion;\n//search the snippet by name\n$CheckAjaxSearch = $modx->db->select( \"id, name, description\", $table, \"name=\'AjaxSearch\'\" );\nif($CheckAjaxSearch != \'\'){\nwhile( $row = $modx->db->getRow( $CheckAjaxSearch ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_AjaxSearch_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_AjaxSearch_version,$minAjaxSearchVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_AjaxSearch_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minAjaxSearchVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check AjaxSearch	\n	\n//check Wayfinder\n//get min version from config\n$minWayfinderVersion = $WayfinderVersion;\n//search the snippet by name\n$CheckWayfinder = $modx->db->select( \"id, name, description\", $table, \"name=\'Wayfinder\'\" );\nif($CheckWayfinder != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWayfinder ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Wayfinder_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Wayfinder_version,$minWayfinderVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Wayfinder_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWayfinderVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Wayfinder\n	\n//check WebLogin\n//get min version from config\n$minWebLoginVersion = $WebLoginVersion;\n//search the snippet by name\n$CheckWebLogin = $modx->db->select( \"id, name, description\", $table, \"name=\'WebLogin\'\" );\nif($CheckWebLogin != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWebLogin ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_WebLogin_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_WebLogin_version,$minWebLoginVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_WebLogin_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebLoginVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check WebLogin\n\n//check WebChangePwd\n//get min version from config\n$minWebChangePwdVersion = $WebChangePwdVersion;\n//search the snippet by name\n$CheckWebChangePwd = $modx->db->select( \"id, name, description\", $table, \"name=\'WebChangePwd\'\" );\nif($CheckWebLogin != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWebChangePwd ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_WebChangePwd_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_WebChangePwd_version,$minWebChangePwdVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_WebChangePwd_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebChangePwdVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check WebChangePwd\n	\n//check WebSignup\n//get min version from config\n$minWebSignupVersion = $WebSignupVersion;\n//search the snippet by name\n$CheckWebSignup = $modx->db->select( \"id, name, description\", $table, \"name=\'WebSignup\'\" );\nif($CheckWebSignup != \'\'){\nwhile( $row = $modx->db->getRow( $CheckWebSignup ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_WebSignup_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_WebSignup_version,$minWebSignupVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_WebSignup_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minWebSignupVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a> \'.$_oec_lang[\'or_move_to\'].\' <b>FormLister</b></div>\';\n		}\n	}\n} \n//end check WebSignup\n\n//check Breadcrumbs\n//get min version from config\n$minBreadcrumbsVersion = $BreadcrumbsVersion;\n//search the snippet by name\n$CheckBreadcrumbs = $modx->db->select( \"id, name, description\", $table, \"name=\'Breadcrumbs\'\" );\nif($CheckBreadcrumbs != \'\'){\nwhile( $row = $modx->db->getRow( $CheckBreadcrumbs ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Breadcrumbs_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Breadcrumbs_version,$minBreadcrumbsVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Breadcrumbs_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minBreadcrumbsVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Breadcrumbs\n\n//check Reflect\n//get min version from config\n$minReflectVersion = $ReflectVersion;\n//search the snippet by name\n$CheckReflect = $modx->db->select( \"id, name, description\", $table, \"name=\'Reflect\'\" );\nif($CheckReflect != \'\'){\nwhile( $row = $modx->db->getRow( $CheckReflect ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Reflect_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Reflect_version,$minReflectVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Reflect_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minReflectVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Reflect\n\n//check Jot\n//get min version from config\n$minJotVersion = $JotVersion;\n//search the snippet by name\n$CheckJot = $modx->db->select( \"id, name, description\", $table, \"name=\'Jot\'\" );\nif($CheckJot != \'\'){\nwhile( $row = $modx->db->getRow( $CheckJot ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_Jot_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_Jot_version,$minJotVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_Jot_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minJotVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a>.</div>\';\n		}\n	}\n} \n//end check Jot\n	\n//check Multitv\n//get min version from config\n$minMtvVersion = $MtvVersion;\n//search the snippet by name\n$CheckMtv = $modx->db->select( \"id, name, description\", $table, \"name=\'multiTV\'\" );\nif($CheckMtv != \'\'){\nwhile( $row = $modx->db->getRow( $CheckMtv ) ) {\n//extract snippet version from description <strong></strong> tags \n$curr_mtv_version = getver($row[\'description\'],\"strong\");\n//check snippet version and return an alert if outdated\nif (version_compare($curr_mtv_version,$minMtvVersion,\'lt\')){\n$output .= \'<div class=\"widget-wrapper alert alert-warning\"><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> <b>\' . $row[\'name\'] . \'</b> \'.$_lang[\"snippet\"].\' (version \' . $curr_mtv_version . \') \'.$_oec_lang[\'isoutdated\'].\' <b>Evolution \'.$EVOversion.\'</b>. \'.$_oec_lang[\'please_update\'].\' <b>\' . $row[\'name\'] . \'</b> \'.$_oec_lang[\"to_latest\"].\' (\'.$_oec_lang[\'min _required\'].\' \'.$minMtvVersion.\') \'.$_oec_lang[\'from\'].\' <a target=\"main\" href=\"index.php?a=112&id=\'.$ExtrasID.\'\">\'.$_oec_lang[\'extras_module\'].\'</a></div>\';\n		}\n	}\n} \n//end check Multitv\n\nif($output != \'\'){\nif($e->name == \'OnManagerWelcomeHome\') {\n$out = $output;\n$wdgTitle = \'EVO \'.$EVOversion.\' - \'.$_oec_lang[\'title\'].\'\';\n$widgets[\'xtraCheck\'] = array(\n				\'menuindex\' =>\'0\',\n				\'id\' => \'xtraCheck\'.$pluginid.\'\',\n				\'cols\' => \'col-md-12\',\n                \'headAttr\' => \'style=\"background-color:#B60205; color:#FFFFFF;\"\',\n				\'bodyAttr\' => \'style=\"background-color:#FFFFFF; color:#24292E;\"\',\n				\'icon\' => \'fa-warning\',\n				\'title\' => \'\'.$wdgTitle.\' \'.$button_pl_config.\'\',\n				\'body\' => \'<div class=\"card-body\">\'.$out.\'</div>\',\n				\'hide\' => \'0\'\n			);	\n            $e->output(serialize($widgets));\nreturn;\n		}\n	}\n}','0','{\"wdgVisibility\":[{\"label\":\"Show widget for:\",\"type\":\"menu\",\"value\":\"All\",\"options\":\"All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly\",\"default\":\"All\",\"desc\":\"\"}],\"ThisRole\":[{\"label\":\"Run only for this role:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"ThisUser\":[{\"label\":\"Run only for this user:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"DittoVersion\":[{\"label\":\"Min Ditto version:\",\"type\":\"string\",\"value\":\"2.1.3\",\"default\":\"2.1.3\",\"desc\":\"\"}],\"EformVersion\":[{\"label\":\"Min eForm version:\",\"type\":\"string\",\"value\":\"1.4.9\",\"default\":\"1.4.9\",\"desc\":\"\"}],\"AjaxSearchVersion\":[{\"label\":\"Min AjaxSearch version:\",\"type\":\"string\",\"value\":\"1.11.0\",\"default\":\"1.11.0\",\"desc\":\"\"}],\"WayfinderVersion\":[{\"label\":\"Min Wayfinder version:\",\"type\":\"string\",\"value\":\"2.0.5\",\"default\":\"2.0.5\",\"desc\":\"\"}],\"WebLoginVersion\":[{\"label\":\"Min WebLogin version:\",\"type\":\"string\",\"value\":\"1.2\",\"default\":\"1.2\",\"desc\":\"\"}],\"WebSignupVersion\":[{\"label\":\"Min WebSignup version:\",\"type\":\"string\",\"value\":\"1.1.2\",\"default\":\"1.1.2\",\"desc\":\"\"}],\"WebChangePwdVersion\":[{\"label\":\"Min WebChangePwd version:\",\"type\":\"string\",\"value\":\"1.1.2\",\"default\":\"1.1.2\",\"desc\":\"\"}],\"BreadcrumbsVersion\":[{\"label\":\"Min Breadcrumbs version:\",\"type\":\"string\",\"value\":\"1.0.5\",\"default\":\"1.0.5\",\"desc\":\"\"}],\"ReflectVersion\":[{\"label\":\"Min Reflect version:\",\"type\":\"string\",\"value\":\"2.2\",\"default\":\"2.2\",\"desc\":\"\"}],\"JotVersion\":[{\"label\":\"Min Jot version:\",\"type\":\"string\",\"value\":\"1.1.5\",\"default\":\"1.1.5\",\"desc\":\"\"}],\"MtvVersion\":[{\"label\":\"Min multiTV version:\",\"type\":\"string\",\"value\":\"2.0.13\",\"default\":\"2.0.13\",\"desc\":\"\"}],\"badthemes\":[{\"label\":\"Outdated Manager Themes:\",\"type\":\"string\",\"value\":\"MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle\",\"default\":\"MODxRE2_DropdownMenu,MODxRE2,MODxRE,MODxCarbon,D3X,MODxFLAT,wMOD,ScienceStyle\",\"desc\":\"\"}]}','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('7','Quick Manager+','<strong>1.5.10</strong> Enables QuickManager+ front end content editing support','0','4','0','\n/**\n * Quick Manager+\n * \n * Enables QuickManager+ front end content editing support\n *\n * @category 	plugin\n * @version 	1.5.10\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties &jqpath=Path to jQuery;text;assets/js/jquery.min.js &loadmanagerjq=Load jQuery in manager;list;true,false;false &loadfrontendjq=Load jQuery in front-end;list;true,false;false &noconflictjq=jQuery noConflict mode in front-end;list;true,false;false &loadfa=Load Font Awesome css in front-end;list;true,false;true &loadtb=Load modal box in front-end;list;true,false;true &tbwidth=Modal box window width;text;80% &tbheight=Modal box window height;text;90% &hidefields=Hide document fields from front-end editors;text;parent &hidetabs=Hide document tabs from front-end editors;text; &hidesections=Hide document sections from front-end editors;text; &addbutton=Show add document here button;list;true,false;true &tpltype=New document template type;list;parent,id,selected;parent &tplid=New document template id;int;3 &custombutton=Custom buttons;textarea; &managerbutton=Show go to manager button;list;true,false;true &logout=Logout to;list;manager,front-end;manager &disabled=Plugin disabled on documents;text; &autohide=Autohide toolbar;list;true,false;true &position= Toolbar position;list;top,right,bottom,left,before;top &editbuttons=Inline edit buttons;list;true,false;false &editbclass=Edit button CSS class;text;qm-edit &newbuttons=Inline new resource buttons;list;true,false;false &newbclass=New resource button CSS class;text;qm-new &tvbuttons=Inline template variable buttons;list;true,false;false &tvbclass=Template variable button CSS class;text;qm-tv &removeBg=Remove toolbar background;list;yes,no;no &buttonStyle=QuickManager buttons CSS stylesheet;list;actionButtons,navButtons;navButtons  \n * @internal	@events OnParseDocument,OnWebPagePrerender,OnDocFormPrerender,OnDocFormSave,OnManagerLogout \n * @internal	@modx_category Manager and Admin\n * @internal    @legacy_names QM+,QuickEdit\n * @internal    @installset base, sample\n * @internal    @disabled 1\n * @reportissues https://github.com/modxcms/evolution\n * @documentation Official docs [+site_url+]assets/plugins/qm/readme.html\n * @link        http://www.maagit.fi/modx/quickmanager-plus\n * @author      Mikko Lammi\n * @author      Since 2011: yama, dmi3yy, segr, Nicola1971.\n * @lastupdate  02/02/2018 \n */\n\n// In manager\nif (!$modx->checkSession()) return;\n\n$show = TRUE;\n\nif ($disabled  != \'\') {\n    $arr = array_filter(array_map(\'intval\', explode(\',\', $disabled)));\n    if (in_array($modx->documentIdentifier, $arr)) {\n        $show = FALSE;\n    }\n}\n\nif ($show) {\n    // Replace [*#tv*] with QM+ edit TV button placeholders\n    if ($tvbuttons == \'true\') {\n        if ($modx->event->name == \'OnParseDocument\') {\n             $output = &$modx->documentOutput;\n             $output = preg_replace(\'~\\[\\*#(.*?)\\*\\]~\', \'<!-- \'.$tvbclass.\' $1 -->[*$1*]\', $output);\n             $modx->documentOutput = $output;\n         }\n     }\n    include_once($modx->config[\'base_path\'].\'assets/plugins/qm/qm.inc.php\');\n    $qm = new Qm($modx, $jqpath, $loadmanagerjq, $loadfrontendjq, $noconflictjq, $loadfa, $loadtb, $tbwidth, $tbheight, $hidefields, $hidetabs, $hidesections, $addbutton, $tpltype, $tplid, $custombutton, $managerbutton, $logout, $autohide, $position, $editbuttons, $editbclass, $newbuttons, $newbclass, $tvbuttons, $tvbclass, $buttonStyle, $removeBg);\n}\n','0','{\"jqpath\":[{\"label\":\"Path to jQuery\",\"type\":\"text\",\"value\":\"assets\\/js\\/jquery.min.js\",\"default\":\"assets\\/js\\/jquery.min.js\",\"desc\":\"\"}],\"loadmanagerjq\":[{\"label\":\"Load jQuery in manager\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"loadfrontendjq\":[{\"label\":\"Load jQuery in front-end\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"noconflictjq\":[{\"label\":\"jQuery noConflict mode in front-end\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"loadfa\":[{\"label\":\"Load Font Awesome css in front-end\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"loadtb\":[{\"label\":\"Load modal box in front-end\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"tbwidth\":[{\"label\":\"Modal box window width\",\"type\":\"text\",\"value\":\"80%\",\"default\":\"80%\",\"desc\":\"\"}],\"tbheight\":[{\"label\":\"Modal box window height\",\"type\":\"text\",\"value\":\"90%\",\"default\":\"90%\",\"desc\":\"\"}],\"hidefields\":[{\"label\":\"Hide document fields from front-end editors\",\"type\":\"text\",\"value\":\"parent\",\"default\":\"parent\",\"desc\":\"\"}],\"hidetabs\":[{\"label\":\"Hide document tabs from front-end editors\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"hidesections\":[{\"label\":\"Hide document sections from front-end editors\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"addbutton\":[{\"label\":\"Show add document here button\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"tpltype\":[{\"label\":\"New document template type\",\"type\":\"list\",\"value\":\"parent\",\"options\":\"parent,id,selected\",\"default\":\"parent\",\"desc\":\"\"}],\"tplid\":[{\"label\":\"New document template id\",\"type\":\"int\",\"value\":\"3\",\"default\":\"3\",\"desc\":\"\"}],\"custombutton\":[{\"label\":\"Custom buttons\",\"type\":\"textarea\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"managerbutton\":[{\"label\":\"Show go to manager button\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"logout\":[{\"label\":\"Logout to\",\"type\":\"list\",\"value\":\"manager\",\"options\":\"manager,front-end\",\"default\":\"manager\",\"desc\":\"\"}],\"disabled\":[{\"label\":\"Plugin disabled on documents\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"autohide\":[{\"label\":\"Autohide toolbar\",\"type\":\"list\",\"value\":\"true\",\"options\":\"true,false\",\"default\":\"true\",\"desc\":\"\"}],\"position\":[{\"label\":\"Toolbar position\",\"type\":\"list\",\"value\":\"top\",\"options\":\"top,right,bottom,left,before\",\"default\":\"top\",\"desc\":\"\"}],\"editbuttons\":[{\"label\":\"Inline edit buttons\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"editbclass\":[{\"label\":\"Edit button CSS class\",\"type\":\"text\",\"value\":\"qm-edit\",\"default\":\"qm-edit\",\"desc\":\"\"}],\"newbuttons\":[{\"label\":\"Inline new resource buttons\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"newbclass\":[{\"label\":\"New resource button CSS class\",\"type\":\"text\",\"value\":\"qm-new\",\"default\":\"qm-new\",\"desc\":\"\"}],\"tvbuttons\":[{\"label\":\"Inline template variable buttons\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"tvbclass\":[{\"label\":\"Template variable button CSS class\",\"type\":\"text\",\"value\":\"qm-tv\",\"default\":\"qm-tv\",\"desc\":\"\"}],\"removeBg\":[{\"label\":\"Remove toolbar background\",\"type\":\"list\",\"value\":\"no\",\"options\":\"yes,no\",\"default\":\"no\",\"desc\":\"\"}],\"buttonStyle\":[{\"label\":\"QuickManager buttons CSS stylesheet\",\"type\":\"list\",\"value\":\"navButtons\",\"options\":\"actionButtons,navButtons\",\"default\":\"navButtons\",\"desc\":\"\"}]}','1','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('8','TinyMCE4','<strong>4.7.4</strong> Javascript rich text editor','0','4','0','\n/**\n * TinyMCE4\n *\n * Javascript rich text editor\n *\n * @category    plugin\n * @version     4.7.4\n * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal    @properties &styleFormats=Custom Style Formats <b>RAW</b><br/><br/><ul><li>leave empty to use below block/inline formats</li><li>allows simple-format: <i>Title,cssClass|Title2,cssClass2</i></li><li>Also accepts full JSON-config as per TinyMCE4 docs / configure / content-formating / style_formats</li></ul>;textarea; &styleFormats_inline=Custom Style Formats <b>INLINE</b><br/><br/><ul><li>will wrap selected text with span-tag + css-class</li><li>simple-format only</li></ul>;textarea;InlineTitle,cssClass1|InlineTitle2,cssClass2 &styleFormats_block=Custom Style Formats <b>BLOCK</b><br/><br/><ul><li>will add css-class to selected block-element</li><li>simple-format only</li></ul>;textarea;BlockTitle,cssClass3|BlockTitle2,cssClass4 &customParams=Custom Parameters<br/><b>(Be careful or leave empty!)</b>;textarea; &entityEncoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &pathOptions=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &resizing=Advanced Resizing;list;true,false;false &disabledButtons=Disabled Buttons;text; &webTheme=Web Theme;test;webuser &webPlugins=Web Plugins;text; &webButtons1=Web Buttons 1;text;bold italic underline strikethrough removeformat alignleft aligncenter alignright &webButtons2=Web Buttons 2;text;link unlink image undo redo &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;400px &introtextRte=<b>Introtext RTE</b><br/>add richtext-features to \"introtext\";list;enabled,disabled;disabled &inlineMode=<b>Inline-Mode</b>;list;enabled,disabled;disabled &inlineTheme=<b>Inline-Mode</b><br/>Theme;text;inline &browser_spellcheck=<b>Browser Spellcheck</b><br/>At least one dictionary must be installed inside your browser;list;enabled,disabled;disabled &paste_as_text=<b>Force Paste as Text</b>;list;enabled,disabled;disabled\n * @internal    @events OnLoadWebDocument,OnParseDocument,OnWebPagePrerender,OnLoadWebPageCache,OnRichTextEditorRegister,OnRichTextEditorInit,OnInterfaceSettingsRender\n * @internal    @modx_category Manager and Admin\n * @internal    @legacy_names TinyMCE4\n * @internal    @installset base\n * @logo        /assets/plugins/tinymce4/tinymce/logo.png\n * @reportissues https://github.com/extras-evolution/tinymce4-for-modx-evo\n * @documentation Plugin docs https://github.com/extras-evolution/tinymce4-for-modx-evo\n * @documentation Official TinyMCE4-docs https://www.tinymce.com/docs/\n * @author      Deesen\n * @lastupdate  2018-01-17\n */\nif (!defined(\'MODX_BASE_PATH\')) { die(\'What are you doing? Get out of here!\'); }\n\nrequire(MODX_BASE_PATH.\"assets/plugins/tinymce4/plugin.tinymce.inc.php\");','0','{\"styleFormats\":[{\"label\":\"Custom Style Formats <b>RAW<\\/b><br\\/><br\\/><ul><li>leave empty to use below block\\/inline formats<\\/li><li>allows simple-format: <i>Title,cssClass|Title2,cssClass2<\\/i><\\/li><li>Also accepts full JSON-config as per TinyMCE4 docs \\/ configure \\/ content-formating \\/ style_formats<\\/li><\\/ul>\",\"type\":\"textarea\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"styleFormats_inline\":[{\"label\":\"Custom Style Formats <b>INLINE<\\/b><br\\/><br\\/><ul><li>will wrap selected text with span-tag + css-class<\\/li><li>simple-format only<\\/li><\\/ul>\",\"type\":\"textarea\",\"value\":\"InlineTitle,cssClass1|InlineTitle2,cssClass2\",\"default\":\"InlineTitle,cssClass1|InlineTitle2,cssClass2\",\"desc\":\"\"}],\"styleFormats_block\":[{\"label\":\"Custom Style Formats <b>BLOCK<\\/b><br\\/><br\\/><ul><li>will add css-class to selected block-element<\\/li><li>simple-format only<\\/li><\\/ul>\",\"type\":\"textarea\",\"value\":\"BlockTitle,cssClass3|BlockTitle2,cssClass4\",\"default\":\"BlockTitle,cssClass3|BlockTitle2,cssClass4\",\"desc\":\"\"}],\"customParams\":[{\"label\":\"Custom Parameters<br\\/><b>(Be careful or leave empty!)<\\/b>\",\"type\":\"textarea\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"entityEncoding\":[{\"label\":\"Entity Encoding\",\"type\":\"list\",\"value\":\"named\",\"options\":\"named,numeric,raw\",\"default\":\"named\",\"desc\":\"\"}],\"entities\":[{\"label\":\"Entities\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"pathOptions\":[{\"label\":\"Path Options\",\"type\":\"list\",\"value\":\"Site config\",\"options\":\"Site config,Absolute path,Root relative,URL,No convert\",\"default\":\"Site config\",\"desc\":\"\"}],\"resizing\":[{\"label\":\"Advanced Resizing\",\"type\":\"list\",\"value\":\"false\",\"options\":\"true,false\",\"default\":\"false\",\"desc\":\"\"}],\"disabledButtons\":[{\"label\":\"Disabled Buttons\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webTheme\":[{\"label\":\"Web Theme\",\"type\":\"test\",\"value\":\"webuser\",\"default\":\"webuser\",\"desc\":\"\"}],\"webPlugins\":[{\"label\":\"Web Plugins\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webButtons1\":[{\"label\":\"Web Buttons 1\",\"type\":\"text\",\"value\":\"bold italic underline strikethrough removeformat alignleft aligncenter alignright\",\"default\":\"bold italic underline strikethrough removeformat alignleft aligncenter alignright\",\"desc\":\"\"}],\"webButtons2\":[{\"label\":\"Web Buttons 2\",\"type\":\"text\",\"value\":\"link unlink image undo redo\",\"default\":\"link unlink image undo redo\",\"desc\":\"\"}],\"webButtons3\":[{\"label\":\"Web Buttons 3\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webButtons4\":[{\"label\":\"Web Buttons 4\",\"type\":\"text\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"webAlign\":[{\"label\":\"Web Toolbar Alignment\",\"type\":\"list\",\"value\":\"ltr\",\"options\":\"ltr,rtl\",\"default\":\"ltr\",\"desc\":\"\"}],\"width\":[{\"label\":\"Width\",\"type\":\"text\",\"value\":\"100%\",\"default\":\"100%\",\"desc\":\"\"}],\"height\":[{\"label\":\"Height\",\"type\":\"text\",\"value\":\"400px\",\"default\":\"400px\",\"desc\":\"\"}],\"introtextRte\":[{\"label\":\"<b>Introtext RTE<\\/b><br\\/>add richtext-features to \\\"introtext\\\"\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}],\"inlineMode\":[{\"label\":\"<b>Inline-Mode<\\/b>\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}],\"inlineTheme\":[{\"label\":\"<b>Inline-Mode<\\/b><br\\/>Theme\",\"type\":\"text\",\"value\":\"inline\",\"default\":\"inline\",\"desc\":\"\"}],\"browser_spellcheck\":[{\"label\":\"<b>Browser Spellcheck<\\/b><br\\/>At least one dictionary must be installed inside your browser\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}],\"paste_as_text\":[{\"label\":\"<b>Force Paste as Text<\\/b>\",\"type\":\"list\",\"value\":\"disabled\",\"options\":\"enabled,disabled\",\"default\":\"disabled\",\"desc\":\"\"}]}','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('9','TransAlias','<strong>1.0.4</strong> Human readible URL translation supporting multiple languages and overrides','0','4','0','require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';','0','{\"table_name\":[{\"label\":\"Trans table\",\"type\":\"list\",\"value\":\"russian\",\"options\":\"common,russian,dutch,german,czech,utf8,utf8lowercase\",\"default\":\"russian\",\"desc\":\"\"}],\"char_restrict\":[{\"label\":\"Restrict alias to\",\"type\":\"list\",\"value\":\"lowercase alphanumeric\",\"options\":\"lowercase alphanumeric,alphanumeric,legal characters\",\"default\":\"lowercase alphanumeric\",\"desc\":\"\"}],\"remove_periods\":[{\"label\":\"Remove Periods\",\"type\":\"list\",\"value\":\"No\",\"options\":\"Yes,No\",\"default\":\"No\",\"desc\":\"\"}],\"word_separator\":[{\"label\":\"Word Separator\",\"type\":\"list\",\"value\":\"dash\",\"options\":\"dash,underscore,none\",\"default\":\"dash\",\"desc\":\"\"}],\"override_tv\":[{\"label\":\"Override TV name\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}]}','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('10','Updater','<strong>0.8.4</strong> show message about outdated CMS version','0','4','0','require MODX_BASE_PATH.\'assets/plugins/updater/plugin.updater.php\';\n\n\n','0','{\"version\":[{\"label\":\"Version:\",\"type\":\"text\",\"value\":\"evolution-cms\\/evolution\",\"default\":\"evolution-cms\\/evolution\",\"desc\":\"\"}],\"wdgVisibility\":[{\"label\":\"Show widget for:\",\"type\":\"menu\",\"value\":\"All\",\"options\":\"All,AdminOnly,AdminExcluded,ThisRoleOnly,ThisUserOnly\",\"default\":\"All\",\"desc\":\"\"}],\"ThisRole\":[{\"label\":\"Show only to this role id:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"ThisUser\":[{\"label\":\"Show only to this username:\",\"type\":\"string\",\"value\":\"\",\"default\":\"\",\"desc\":\"\"}],\"showButton\":[{\"label\":\"Show Update Button:\",\"type\":\"menu\",\"value\":\"AdminOnly\",\"options\":\"show,hide,AdminOnly\",\"default\":\"AdminOnly\",\"desc\":\"\"}],\"type\":[{\"label\":\"Type:\",\"type\":\"menu\",\"value\":\"tags\",\"options\":\"tags,releases,commits\",\"default\":\"tags\",\"desc\":\"\"}],\"branch\":[{\"label\":\"Commit branch:\",\"type\":\"text\",\"value\":\"develop\",\"default\":\"develop\",\"desc\":\"\"}]}','0','','0','0');

INSERT INTO `alv1_site_plugins` VALUES ('11','userHelper','<strong>1.7.18</strong> addition to FormLister','0','5','0','\n/**\n * userHelper\n * \n * addition to FormLister\n * \n * @category    plugin\n * @version     1.7.18\n * @internal    @properties &logoutKey=Request key;text;logout &cookieName=Cookie Name;text;WebLoginPE &cookieLifetime=Cookie Lifetime, seconds;text;157680000 &maxFails=Max failed logins;text;3 &blockTime=Block for, seconds;text;3600\n * @internal    @events OnWebAuthentication,OnWebPageInit,OnPageNotFound,OnWebLogin\n * @internal    @modx_category Content\n * @internal    @disabled 1\n**/\n\nrequire MODX_BASE_PATH.\'assets/snippets/FormLister/plugin.userHelper.php\';\n','0','{\"logoutKey\":[{\"label\":\"Request key\",\"type\":\"text\",\"value\":\"logout\",\"default\":\"logout\",\"desc\":\"\"}],\"cookieName\":[{\"label\":\"Cookie Name\",\"type\":\"text\",\"value\":\"WebLoginPE\",\"default\":\"WebLoginPE\",\"desc\":\"\"}],\"cookieLifetime\":[{\"label\":\"Cookie Lifetime, seconds\",\"type\":\"text\",\"value\":\"157680000\",\"default\":\"157680000\",\"desc\":\"\"}],\"maxFails\":[{\"label\":\"Max failed logins\",\"type\":\"text\",\"value\":\"3\",\"default\":\"3\",\"desc\":\"\"}],\"blockTime\":[{\"label\":\"Block for, seconds\",\"type\":\"text\",\"value\":\"3600\",\"default\":\"3600\",\"desc\":\"\"}]}','1','','0','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_snippets`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_snippets`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_snippets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Snippet',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COMMENT 'Default Properties',
  `moduleguid` varchar(32) NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the snippet',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Contains the site snippets.';

#
# Dumping data for table `alv1_site_snippets`
#

INSERT INTO `alv1_site_snippets` VALUES ('1','DLCrumbs','<strong>1.2</strong> DLCrumbs','0','6','0','return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLCrumbs.php\';','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('2','DLMenu','<strong>1.3.0</strong> Snippet to build menu with DocLister','0','6','0','return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLMenu.php\';\n','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('3','DLSitemap','<strong>1.0.0</strong> Snippet to build XML sitemap','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DLSitemap.php\';\n','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('4','DocInfo','<strong>1</strong> Take any field from any document (fewer requests than GetField)','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/docinfo/snippet.docinfo.php\';\n','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('5','DocLister','<strong>2.3.14</strong> Snippet to display the information of the tables by the description rules. The main goal - replacing Ditto and CatalogView','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/DocLister/snippet.DocLister.php\';\n','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('6','FormLister','<strong>1.7.18</strong> Form processor','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/FormLister/snippet.FormLister.php\';\n','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('7','if','<strong>1.3</strong> A simple conditional snippet. Allows for eq/neq/lt/gt/etc logic within templates, resources, chunks, etc.','0','6','0','return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('8','phpthumb','<strong>1.3</strong> PHPThumb creates thumbnails and altered images on the fly and caches them','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';\n','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('9','summary','<strong>2.0.2</strong> Truncates the string to the specified length','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/summary/snippet.summary.php\';','0','','','0','0','0');

INSERT INTO `alv1_site_snippets` VALUES ('13','prepare_img_product','подготавливаем изображение товара','0','10','0','\n$data[\'image\'] = $modx->runSnippet(\'phpthumb\', array(\'input\' => \'/assets/templates/enteley_template/img/material/\'.$data[\'image\'], \'options\'=>\'w=178,h=178,zc=1\'));\n\nreturn $data;','0','{}',' ','1525428301','1525429911','0');

INSERT INTO `alv1_site_snippets` VALUES ('14','prepare_img_fashion','подготавливаем изображение товара (мода)','0','10','0','\n$data[\'image\'] = $modx->runSnippet(\'phpthumb\', array(\'input\' => \'/assets/templates/enteley_template/img/slide/\'.$data[\'image\'], \'options\'=>\'w=378,h=378,zc=1\'));\n\nreturn $data;','0','{}',' ','0','1525430023','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_templates`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_templates`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `templatename` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'url to icon file',
  `template_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-page,1-content',
  `content` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `selectable` tinyint(4) NOT NULL DEFAULT '1',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Contains the site templates.';

#
# Dumping data for table `alv1_site_templates`
#

INSERT INTO `alv1_site_templates` VALUES ('3','Каталог товаров','Шаблон каталога товаров','0','7','','0','{{HEAD}}\n    <body>\n		{{WRAPPER_INDEX}}\n        \n		{{MODAL_CONTACT}}\n\n		{{ICONS}}\n\n		{{MAIN_SCRIPT}}\n    </body>\n</html>','0','1','0','1525338594');

INSERT INTO `alv1_site_templates` VALUES ('4','О компании','Шаблон страницы о компании','0','7','','0','{{HEAD}}\n    <body>\n		{{WRAPPER_ABOUT}}\n        \n		{{MODAL_CONTACT}}\n\n		{{ICONS}}\n\n		{{MAIN_SCRIPT}}\n    </body>\n</html>','0','1','1525167596','1525338639');

INSERT INTO `alv1_site_templates` VALUES ('5','Памятка покупателю','Шаблон страницы памятка покупателю','0','7','','0','{{HEAD}}\n    <body>\n		{{WRAPPER_PRAVILA}}\n        \n		{{MODAL_CONTACT}}\n\n		{{ICONS}}\n\n		{{MAIN_SCRIPT}}\n    </body>\n</html>','0','1','0','1525338658');

INSERT INTO `alv1_site_templates` VALUES ('6','Оплата и доставка','Шаблон страницы оплата и доставка','0','7','','0','{{HEAD}}\n    <body>\n		{{WRAPPER_OPLATA_DOSTAVKA}}\n        \n		{{MODAL_CONTACT}}\n\n		{{ICONS}}\n\n		{{MAIN_SCRIPT}}\n    </body>\n</html>','0','1','0','1525338647');

INSERT INTO `alv1_site_templates` VALUES ('7','Акции','Шаблон страницы акции','0','7','','0','{{HEAD}}\n    <body>\n		{{WRAPPER_STOCK}}\n        \n		{{MODAL_CONTACT}}\n\n		{{ICONS}}\n\n		{{MAIN_SCRIPT}}\n    </body>\n</html>','0','1','0','1525338610');

INSERT INTO `alv1_site_templates` VALUES ('8','Контакты','Шаблон страницы контакты','0','7','','0','{{HEAD}}\n    <body>\n		{{WRAPPER_CONTACT}}\n        \n		{{MODAL_CONTACT}}\n\n		{{ICONS}}\n\n		{{MAIN_SCRIPT}}\n    </body>\n</html>','0','1','0','1525338627');

INSERT INTO `alv1_site_templates` VALUES ('9','404 - Документ не найден','Ох ... это 404! (Страница не найдена)','0','7','','0','{{HEAD}}\n    <body>\n		 <div class=\"wrapper\">           \n			 <main class=\"main\">\n                <section class=\"main__section inner\">\n                    <header class=\"main__header\">\n                        <h1 class=\"main__title\">404 - Документ не найден!</h1>\n                    </header>\n                    \n                    <p>Похоже, вы пытались перейти туда, чего не существует ... возможно, вам нужно войти в систему или вам понадобится одна из следующих страниц:</p>\n					\n					[[DLMenu? &parents=`0`]]\n					\n                </section>    \n            </main>\n        </div>\n    </body>\n</html>','0','1','1525170940','1525171043');


# --------------------------------------------------------

#
# Table structure for table `alv1_site_tmplvar_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_tmplvar_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_tmplvar_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for template variable access permissions.';

#
# Dumping data for table `alv1_site_tmplvar_access`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_site_tmplvar_contentvalues`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_tmplvar_contentvalues`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_tmplvar_contentvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `contentid` int(10) NOT NULL DEFAULT '0' COMMENT 'Site Content Id',
  `value` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_tvid_contentid` (`tmplvarid`,`contentid`),
  KEY `idx_tmplvarid` (`tmplvarid`),
  KEY `idx_id` (`contentid`),
  FULLTEXT KEY `value_ft_idx` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site Template Variables Content Values Link Table';

#
# Dumping data for table `alv1_site_tmplvar_contentvalues`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_site_tmplvar_templates`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_tmplvar_templates`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Site Template Variables Templates Link Table';

#
# Dumping data for table `alv1_site_tmplvar_templates`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_site_tmplvars`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_site_tmplvars`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_site_tmplvars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `elements` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) NOT NULL DEFAULT '' COMMENT 'Display Control',
  `display_params` text COMMENT 'Display Control Properties',
  `default_text` text,
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_rank` (`rank`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Site Template Variables';

#
# Dumping data for table `alv1_site_tmplvars`
#

INSERT INTO `alv1_site_tmplvars` VALUES ('1','textarea','desc','Meta description','Meta description','0','1','0','','0','','','[*introtext*]','0','0');

INSERT INTO `alv1_site_tmplvars` VALUES ('2','text','keyw','Meta keywords','Meta keywords','0','1','0','','0','','','[*pagetitle*]','0','0');

INSERT INTO `alv1_site_tmplvars` VALUES ('3','checkbox','noIndex','No index page','Meta robots','0','1','0','Нет==<meta name=\"robots\" content=\"noindex, nofollow\">','0','','','','0','0');

INSERT INTO `alv1_site_tmplvars` VALUES ('4','text','titl','Meta title','Meta title','0','1','0','','0','','','[*pagetitle*] - [(site_name)]','0','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_system_eventnames`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_system_eventnames`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_system_eventnames` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `service` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'System Service number',
  `groupname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1036 DEFAULT CHARSET=utf8 COMMENT='System Event Names.';

#
# Dumping data for table `alv1_system_eventnames`
#

INSERT INTO `alv1_system_eventnames` VALUES ('1','OnDocPublished','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('2','OnDocUnPublished','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('3','OnWebPagePrerender','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('4','OnWebLogin','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('5','OnBeforeWebLogout','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('6','OnWebLogout','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('7','OnWebSaveUser','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('8','OnWebDeleteUser','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('9','OnWebChangePassword','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('10','OnWebCreateGroup','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('11','OnManagerLogin','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('12','OnBeforeManagerLogout','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('13','OnManagerLogout','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('14','OnManagerSaveUser','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('15','OnManagerDeleteUser','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('16','OnManagerChangePassword','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('17','OnManagerCreateGroup','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('18','OnBeforeCacheUpdate','4','');

INSERT INTO `alv1_system_eventnames` VALUES ('19','OnCacheUpdate','4','');

INSERT INTO `alv1_system_eventnames` VALUES ('107','OnMakePageCacheKey','4','');

INSERT INTO `alv1_system_eventnames` VALUES ('20','OnLoadWebPageCache','4','');

INSERT INTO `alv1_system_eventnames` VALUES ('21','OnBeforeSaveWebPageCache','4','');

INSERT INTO `alv1_system_eventnames` VALUES ('22','OnChunkFormPrerender','1','Chunks');

INSERT INTO `alv1_system_eventnames` VALUES ('23','OnChunkFormRender','1','Chunks');

INSERT INTO `alv1_system_eventnames` VALUES ('24','OnBeforeChunkFormSave','1','Chunks');

INSERT INTO `alv1_system_eventnames` VALUES ('25','OnChunkFormSave','1','Chunks');

INSERT INTO `alv1_system_eventnames` VALUES ('26','OnBeforeChunkFormDelete','1','Chunks');

INSERT INTO `alv1_system_eventnames` VALUES ('27','OnChunkFormDelete','1','Chunks');

INSERT INTO `alv1_system_eventnames` VALUES ('28','OnDocFormPrerender','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('29','OnDocFormRender','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('30','OnBeforeDocFormSave','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('31','OnDocFormSave','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('32','OnBeforeDocFormDelete','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('33','OnDocFormDelete','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('1033','OnDocFormUnDelete','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('1034','onBeforeMoveDocument','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('1035','onAfterMoveDocument','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('34','OnPluginFormPrerender','1','Plugins');

INSERT INTO `alv1_system_eventnames` VALUES ('35','OnPluginFormRender','1','Plugins');

INSERT INTO `alv1_system_eventnames` VALUES ('36','OnBeforePluginFormSave','1','Plugins');

INSERT INTO `alv1_system_eventnames` VALUES ('37','OnPluginFormSave','1','Plugins');

INSERT INTO `alv1_system_eventnames` VALUES ('38','OnBeforePluginFormDelete','1','Plugins');

INSERT INTO `alv1_system_eventnames` VALUES ('39','OnPluginFormDelete','1','Plugins');

INSERT INTO `alv1_system_eventnames` VALUES ('40','OnSnipFormPrerender','1','Snippets');

INSERT INTO `alv1_system_eventnames` VALUES ('41','OnSnipFormRender','1','Snippets');

INSERT INTO `alv1_system_eventnames` VALUES ('42','OnBeforeSnipFormSave','1','Snippets');

INSERT INTO `alv1_system_eventnames` VALUES ('43','OnSnipFormSave','1','Snippets');

INSERT INTO `alv1_system_eventnames` VALUES ('44','OnBeforeSnipFormDelete','1','Snippets');

INSERT INTO `alv1_system_eventnames` VALUES ('45','OnSnipFormDelete','1','Snippets');

INSERT INTO `alv1_system_eventnames` VALUES ('46','OnTempFormPrerender','1','Templates');

INSERT INTO `alv1_system_eventnames` VALUES ('47','OnTempFormRender','1','Templates');

INSERT INTO `alv1_system_eventnames` VALUES ('48','OnBeforeTempFormSave','1','Templates');

INSERT INTO `alv1_system_eventnames` VALUES ('49','OnTempFormSave','1','Templates');

INSERT INTO `alv1_system_eventnames` VALUES ('50','OnBeforeTempFormDelete','1','Templates');

INSERT INTO `alv1_system_eventnames` VALUES ('51','OnTempFormDelete','1','Templates');

INSERT INTO `alv1_system_eventnames` VALUES ('52','OnTVFormPrerender','1','Template Variables');

INSERT INTO `alv1_system_eventnames` VALUES ('53','OnTVFormRender','1','Template Variables');

INSERT INTO `alv1_system_eventnames` VALUES ('54','OnBeforeTVFormSave','1','Template Variables');

INSERT INTO `alv1_system_eventnames` VALUES ('55','OnTVFormSave','1','Template Variables');

INSERT INTO `alv1_system_eventnames` VALUES ('56','OnBeforeTVFormDelete','1','Template Variables');

INSERT INTO `alv1_system_eventnames` VALUES ('57','OnTVFormDelete','1','Template Variables');

INSERT INTO `alv1_system_eventnames` VALUES ('58','OnUserFormPrerender','1','Users');

INSERT INTO `alv1_system_eventnames` VALUES ('59','OnUserFormRender','1','Users');

INSERT INTO `alv1_system_eventnames` VALUES ('60','OnBeforeUserFormSave','1','Users');

INSERT INTO `alv1_system_eventnames` VALUES ('61','OnUserFormSave','1','Users');

INSERT INTO `alv1_system_eventnames` VALUES ('62','OnBeforeUserFormDelete','1','Users');

INSERT INTO `alv1_system_eventnames` VALUES ('63','OnUserFormDelete','1','Users');

INSERT INTO `alv1_system_eventnames` VALUES ('64','OnWUsrFormPrerender','1','Web Users');

INSERT INTO `alv1_system_eventnames` VALUES ('65','OnWUsrFormRender','1','Web Users');

INSERT INTO `alv1_system_eventnames` VALUES ('66','OnBeforeWUsrFormSave','1','Web Users');

INSERT INTO `alv1_system_eventnames` VALUES ('67','OnWUsrFormSave','1','Web Users');

INSERT INTO `alv1_system_eventnames` VALUES ('68','OnBeforeWUsrFormDelete','1','Web Users');

INSERT INTO `alv1_system_eventnames` VALUES ('69','OnWUsrFormDelete','1','Web Users');

INSERT INTO `alv1_system_eventnames` VALUES ('70','OnSiteRefresh','1','');

INSERT INTO `alv1_system_eventnames` VALUES ('71','OnFileManagerUpload','1','');

INSERT INTO `alv1_system_eventnames` VALUES ('72','OnModFormPrerender','1','Modules');

INSERT INTO `alv1_system_eventnames` VALUES ('73','OnModFormRender','1','Modules');

INSERT INTO `alv1_system_eventnames` VALUES ('74','OnBeforeModFormDelete','1','Modules');

INSERT INTO `alv1_system_eventnames` VALUES ('75','OnModFormDelete','1','Modules');

INSERT INTO `alv1_system_eventnames` VALUES ('76','OnBeforeModFormSave','1','Modules');

INSERT INTO `alv1_system_eventnames` VALUES ('77','OnModFormSave','1','Modules');

INSERT INTO `alv1_system_eventnames` VALUES ('78','OnBeforeWebLogin','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('79','OnWebAuthentication','3','');

INSERT INTO `alv1_system_eventnames` VALUES ('80','OnBeforeManagerLogin','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('81','OnManagerAuthentication','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('82','OnSiteSettingsRender','1','System Settings');

INSERT INTO `alv1_system_eventnames` VALUES ('83','OnFriendlyURLSettingsRender','1','System Settings');

INSERT INTO `alv1_system_eventnames` VALUES ('84','OnUserSettingsRender','1','System Settings');

INSERT INTO `alv1_system_eventnames` VALUES ('85','OnInterfaceSettingsRender','1','System Settings');

INSERT INTO `alv1_system_eventnames` VALUES ('86','OnMiscSettingsRender','1','System Settings');

INSERT INTO `alv1_system_eventnames` VALUES ('87','OnRichTextEditorRegister','1','RichText Editor');

INSERT INTO `alv1_system_eventnames` VALUES ('88','OnRichTextEditorInit','1','RichText Editor');

INSERT INTO `alv1_system_eventnames` VALUES ('89','OnManagerPageInit','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('90','OnWebPageInit','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('101','OnLoadDocumentObject','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('104','OnBeforeLoadDocumentObject','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('105','OnAfterLoadDocumentObject','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('91','OnLoadWebDocument','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('92','OnParseDocument','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('106','OnParseProperties','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('108','OnBeforeParseParams','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('93','OnManagerLoginFormRender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('94','OnWebPageComplete','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('95','OnLogPageHit','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('96','OnBeforeManagerPageInit','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('97','OnBeforeEmptyTrash','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('98','OnEmptyTrash','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('99','OnManagerLoginFormPrerender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('100','OnStripAlias','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('102','OnMakeDocUrl','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('103','OnBeforeLoadExtension','5','');

INSERT INTO `alv1_system_eventnames` VALUES ('200','OnCreateDocGroup','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('201','OnManagerWelcomePrerender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('202','OnManagerWelcomeHome','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('203','OnManagerWelcomeRender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('204','OnBeforeDocDuplicate','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('205','OnDocDuplicate','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('206','OnManagerMainFrameHeaderHTMLBlock','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('207','OnManagerPreFrameLoader','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('208','OnManagerFrameLoader','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('209','OnManagerTreeInit','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('210','OnManagerTreePrerender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('211','OnManagerTreeRender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('212','OnManagerNodePrerender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('213','OnManagerNodeRender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('214','OnManagerMenuPrerender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('215','OnManagerTopPrerender','2','');

INSERT INTO `alv1_system_eventnames` VALUES ('224','OnDocFormTemplateRender','1','Documents');

INSERT INTO `alv1_system_eventnames` VALUES ('999','OnPageUnauthorized','1','');

INSERT INTO `alv1_system_eventnames` VALUES ('1000','OnPageNotFound','1','');

INSERT INTO `alv1_system_eventnames` VALUES ('1001','OnFileBrowserUpload','1','File Browser Events');


# --------------------------------------------------------

#
# Table structure for table `alv1_system_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_system_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_system_settings` (
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text,
  PRIMARY KEY (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains Content Manager settings.';

#
# Dumping data for table `alv1_system_settings`
#

INSERT INTO `alv1_system_settings` VALUES ('settings_version','1.4.2');

INSERT INTO `alv1_system_settings` VALUES ('manager_theme','default');

INSERT INTO `alv1_system_settings` VALUES ('server_offset_time','0');

INSERT INTO `alv1_system_settings` VALUES ('manager_language','russian-UTF8');

INSERT INTO `alv1_system_settings` VALUES ('modx_charset','UTF-8');

INSERT INTO `alv1_system_settings` VALUES ('site_name','Enteley');

INSERT INTO `alv1_system_settings` VALUES ('site_start','1');

INSERT INTO `alv1_system_settings` VALUES ('error_page','7');

INSERT INTO `alv1_system_settings` VALUES ('unauthorized_page','7');

INSERT INTO `alv1_system_settings` VALUES ('site_status','1');

INSERT INTO `alv1_system_settings` VALUES ('auto_template_logic','parent');

INSERT INTO `alv1_system_settings` VALUES ('default_template','3');

INSERT INTO `alv1_system_settings` VALUES ('old_template','3');

INSERT INTO `alv1_system_settings` VALUES ('publish_default','1');

INSERT INTO `alv1_system_settings` VALUES ('friendly_urls','1');

INSERT INTO `alv1_system_settings` VALUES ('friendly_alias_urls','1');

INSERT INTO `alv1_system_settings` VALUES ('use_alias_path','1');

INSERT INTO `alv1_system_settings` VALUES ('cache_type','2');

INSERT INTO `alv1_system_settings` VALUES ('failed_login_attempts','3');

INSERT INTO `alv1_system_settings` VALUES ('blocked_minutes','60');

INSERT INTO `alv1_system_settings` VALUES ('use_captcha','0');

INSERT INTO `alv1_system_settings` VALUES ('emailsender','kiddr@rambler.ru');

INSERT INTO `alv1_system_settings` VALUES ('use_editor','1');

INSERT INTO `alv1_system_settings` VALUES ('use_browser','1');

INSERT INTO `alv1_system_settings` VALUES ('fe_editor_lang','russian-UTF8');

INSERT INTO `alv1_system_settings` VALUES ('fck_editor_toolbar','standard');

INSERT INTO `alv1_system_settings` VALUES ('fck_editor_autolang','0');

INSERT INTO `alv1_system_settings` VALUES ('editor_css_path','');

INSERT INTO `alv1_system_settings` VALUES ('editor_css_selectors','');

INSERT INTO `alv1_system_settings` VALUES ('upload_maxsize','10485760');

INSERT INTO `alv1_system_settings` VALUES ('manager_layout','4');

INSERT INTO `alv1_system_settings` VALUES ('auto_menuindex','1');

INSERT INTO `alv1_system_settings` VALUES ('session.cookie.lifetime','604800');

INSERT INTO `alv1_system_settings` VALUES ('mail_check_timeperiod','600');

INSERT INTO `alv1_system_settings` VALUES ('manager_direction','ltr');

INSERT INTO `alv1_system_settings` VALUES ('xhtml_urls','0');

INSERT INTO `alv1_system_settings` VALUES ('automatic_alias','1');

INSERT INTO `alv1_system_settings` VALUES ('datetime_format','dd-mm-YYYY');

INSERT INTO `alv1_system_settings` VALUES ('warning_visibility','0');

INSERT INTO `alv1_system_settings` VALUES ('remember_last_tab','1');

INSERT INTO `alv1_system_settings` VALUES ('enable_bindings','1');

INSERT INTO `alv1_system_settings` VALUES ('seostrict','1');

INSERT INTO `alv1_system_settings` VALUES ('number_of_results','30');

INSERT INTO `alv1_system_settings` VALUES ('theme_refresher','');

INSERT INTO `alv1_system_settings` VALUES ('show_picker','0');

INSERT INTO `alv1_system_settings` VALUES ('show_newresource_btn','0');

INSERT INTO `alv1_system_settings` VALUES ('show_fullscreen_btn','0');

INSERT INTO `alv1_system_settings` VALUES ('email_sender_method','1');

INSERT INTO `alv1_system_settings` VALUES ('site_id','5ae6fd49cbcf4');

INSERT INTO `alv1_system_settings` VALUES ('site_unavailable_page','');

INSERT INTO `alv1_system_settings` VALUES ('reload_site_unavailable','');

INSERT INTO `alv1_system_settings` VALUES ('site_unavailable_message','В настоящее время сайт недоступен.');

INSERT INTO `alv1_system_settings` VALUES ('siteunavailable_message_default','В настоящее время сайт недоступен.');

INSERT INTO `alv1_system_settings` VALUES ('enable_filter','0');

INSERT INTO `alv1_system_settings` VALUES ('enable_at_syntax','0');

INSERT INTO `alv1_system_settings` VALUES ('cache_default','1');

INSERT INTO `alv1_system_settings` VALUES ('search_default','1');

INSERT INTO `alv1_system_settings` VALUES ('custom_contenttype','application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json');

INSERT INTO `alv1_system_settings` VALUES ('docid_incrmnt_method','0');

INSERT INTO `alv1_system_settings` VALUES ('enable_cache','1');

INSERT INTO `alv1_system_settings` VALUES ('minifyphp_incache','0');

INSERT INTO `alv1_system_settings` VALUES ('server_protocol','http');

INSERT INTO `alv1_system_settings` VALUES ('rss_url_news','http://feeds.feedburner.com/evocms-release-news');

INSERT INTO `alv1_system_settings` VALUES ('track_visitors','0');

INSERT INTO `alv1_system_settings` VALUES ('top_howmany','10');

INSERT INTO `alv1_system_settings` VALUES ('friendly_url_prefix','');

INSERT INTO `alv1_system_settings` VALUES ('friendly_url_suffix','.html');

INSERT INTO `alv1_system_settings` VALUES ('make_folders','0');

INSERT INTO `alv1_system_settings` VALUES ('aliaslistingfolder','0');

INSERT INTO `alv1_system_settings` VALUES ('allow_duplicate_alias','0');

INSERT INTO `alv1_system_settings` VALUES ('use_udperms','1');

INSERT INTO `alv1_system_settings` VALUES ('udperms_allowroot','0');

INSERT INTO `alv1_system_settings` VALUES ('email_method','mail');

INSERT INTO `alv1_system_settings` VALUES ('smtp_auth','0');

INSERT INTO `alv1_system_settings` VALUES ('smtp_secure','none');

INSERT INTO `alv1_system_settings` VALUES ('smtp_host','smtp.example.com');

INSERT INTO `alv1_system_settings` VALUES ('smtp_port','25');

INSERT INTO `alv1_system_settings` VALUES ('smtp_username','you@example.com');

INSERT INTO `alv1_system_settings` VALUES ('reload_emailsubject','');

INSERT INTO `alv1_system_settings` VALUES ('emailsubject','Данные для авторизации');

INSERT INTO `alv1_system_settings` VALUES ('emailsubject_default','Данные для авторизации');

INSERT INTO `alv1_system_settings` VALUES ('reload_signupemail_message','');

INSERT INTO `alv1_system_settings` VALUES ('signupemail_message','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации в системе управления сайтом [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `alv1_system_settings` VALUES ('system_email_signup_default','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации в системе управления сайтом [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `alv1_system_settings` VALUES ('reload_websignupemail_message','');

INSERT INTO `alv1_system_settings` VALUES ('websignupemail_message','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации на [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `alv1_system_settings` VALUES ('system_email_websignup_default','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации на [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `alv1_system_settings` VALUES ('reload_system_email_webreminder_message','');

INSERT INTO `alv1_system_settings` VALUES ('webpwdreminder_message','Здравствуйте, [+uid+]!\n\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\n\n[+surl+]\n\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\n\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\n\nС уважением, Администрация');

INSERT INTO `alv1_system_settings` VALUES ('system_email_webreminder_default','Здравствуйте, [+uid+]!\n\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\n\n[+surl+]\n\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\n\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\n\nС уважением, Администрация');

INSERT INTO `alv1_system_settings` VALUES ('tree_page_click','27');

INSERT INTO `alv1_system_settings` VALUES ('use_breadcrumbs','0');

INSERT INTO `alv1_system_settings` VALUES ('global_tabs','1');

INSERT INTO `alv1_system_settings` VALUES ('group_tvs','0');

INSERT INTO `alv1_system_settings` VALUES ('resource_tree_node_name','pagetitle');

INSERT INTO `alv1_system_settings` VALUES ('session_timeout','15');

INSERT INTO `alv1_system_settings` VALUES ('tree_show_protected','0');

INSERT INTO `alv1_system_settings` VALUES ('datepicker_offset','-10');

INSERT INTO `alv1_system_settings` VALUES ('number_of_logs','100');

INSERT INTO `alv1_system_settings` VALUES ('number_of_messages','40');

INSERT INTO `alv1_system_settings` VALUES ('which_editor','TinyMCE4');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_theme','custom');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_skin','lightgray');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_skintheme','inlite');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_template_docs','');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_template_chunks','');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_entermode','p');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_element_format','xhtml');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_schema','html5');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_custom_plugins','advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_custom_buttons1','undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_custom_buttons2','link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code formatselect');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_custom_buttons3','');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_custom_buttons4','');

INSERT INTO `alv1_system_settings` VALUES ('tinymce4_blockFormats','Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3');

INSERT INTO `alv1_system_settings` VALUES ('allow_eval','with_scan');

INSERT INTO `alv1_system_settings` VALUES ('safe_functions_at_eval','time,date,strtotime,strftime');

INSERT INTO `alv1_system_settings` VALUES ('check_files_onlogin','index.php\n.htaccess\nmanager/index.php\nmanager/includes/config.inc.php');

INSERT INTO `alv1_system_settings` VALUES ('validate_referer','1');

INSERT INTO `alv1_system_settings` VALUES ('rss_url_security','http://feeds.feedburner.com/evocms-security-news');

INSERT INTO `alv1_system_settings` VALUES ('error_reporting','1');

INSERT INTO `alv1_system_settings` VALUES ('send_errormail','0');

INSERT INTO `alv1_system_settings` VALUES ('pwd_hash_algo','UNCRYPT');

INSERT INTO `alv1_system_settings` VALUES ('reload_captcha_words','');

INSERT INTO `alv1_system_settings` VALUES ('captcha_words','EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote');

INSERT INTO `alv1_system_settings` VALUES ('captcha_words_default','EVO,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote');

INSERT INTO `alv1_system_settings` VALUES ('filemanager_path','C:/OpenServer/domains/enteley.com/assets/');

INSERT INTO `alv1_system_settings` VALUES ('upload_files','bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,fla,flv,swf,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip,JPG,JPEG,PNG,GIF,svg,tpl');

INSERT INTO `alv1_system_settings` VALUES ('upload_images','bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,svg');

INSERT INTO `alv1_system_settings` VALUES ('upload_media','au,avi,mp3,mp4,mpeg,mpg,wav,wmv');

INSERT INTO `alv1_system_settings` VALUES ('upload_flash','fla,flv,swf');

INSERT INTO `alv1_system_settings` VALUES ('new_file_permissions','0644');

INSERT INTO `alv1_system_settings` VALUES ('new_folder_permissions','0755');

INSERT INTO `alv1_system_settings` VALUES ('which_browser','mcpuk');

INSERT INTO `alv1_system_settings` VALUES ('rb_webuser','0');

INSERT INTO `alv1_system_settings` VALUES ('rb_base_dir','C:/OpenServer/domains/enteley.com/assets/');

INSERT INTO `alv1_system_settings` VALUES ('rb_base_url','assets/');

INSERT INTO `alv1_system_settings` VALUES ('clean_uploaded_filename','1');

INSERT INTO `alv1_system_settings` VALUES ('strip_image_paths','1');

INSERT INTO `alv1_system_settings` VALUES ('maxImageWidth','2600');

INSERT INTO `alv1_system_settings` VALUES ('maxImageHeight','2200');

INSERT INTO `alv1_system_settings` VALUES ('thumbWidth','150');

INSERT INTO `alv1_system_settings` VALUES ('thumbHeight','150');

INSERT INTO `alv1_system_settings` VALUES ('thumbsDir','.thumbs');

INSERT INTO `alv1_system_settings` VALUES ('jpegQuality','90');

INSERT INTO `alv1_system_settings` VALUES ('denyZipDownload','0');

INSERT INTO `alv1_system_settings` VALUES ('denyExtensionRename','0');

INSERT INTO `alv1_system_settings` VALUES ('showHiddenFiles','0');

INSERT INTO `alv1_system_settings` VALUES ('lang_code','ru');

INSERT INTO `alv1_system_settings` VALUES ('sys_files_checksum','a:4:{s:43:\"C:/OpenServer/domains/enteley.com/index.php\";s:32:\"2b1672b057429276f348f016b2b8ea5c\";s:43:\"C:/OpenServer/domains/enteley.com/.htaccess\";s:32:\"67a10fd6f78754b24eb7a6badc273fe6\";s:51:\"C:/OpenServer/domains/enteley.com/manager/index.php\";s:32:\"153c387763636c16fdd1d9225f89dcf3\";s:65:\"C:/OpenServer/domains/enteley.com/manager/includes/config.inc.php\";s:32:\"c8c7fc79e6b07de49fc0ef3a23ea6285\";}');


# --------------------------------------------------------

#
# Table structure for table `alv1_user_attributes`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_user_attributes`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text,
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Contains information about the backend users.';

#
# Dumping data for table `alv1_user_attributes`
#

INSERT INTO `alv1_user_attributes` VALUES ('1','1','Admin','1','kiddrtest@gmail.com','','','0','0','0','17','1525419612','1525447120','0','c6j3iq89ie38kutq78ntj436d4','0','0','','','','','','','','','0','0');


# --------------------------------------------------------

#
# Table structure for table `alv1_user_messages`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_user_messages`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_user_messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(60) NOT NULL DEFAULT '',
  `message` text,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `postdate` int(20) NOT NULL DEFAULT '0',
  `messageread` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Contains messages for the Content Manager messaging system.';

#
# Dumping data for table `alv1_user_messages`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_user_roles`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_user_roles`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_user_roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `frames` int(1) NOT NULL DEFAULT '0',
  `home` int(1) NOT NULL DEFAULT '0',
  `view_document` int(1) NOT NULL DEFAULT '0',
  `new_document` int(1) NOT NULL DEFAULT '0',
  `save_document` int(1) NOT NULL DEFAULT '0',
  `publish_document` int(1) NOT NULL DEFAULT '0',
  `delete_document` int(1) NOT NULL DEFAULT '0',
  `empty_trash` int(1) NOT NULL DEFAULT '0',
  `action_ok` int(1) NOT NULL DEFAULT '0',
  `logout` int(1) NOT NULL DEFAULT '0',
  `help` int(1) NOT NULL DEFAULT '0',
  `messages` int(1) NOT NULL DEFAULT '0',
  `new_user` int(1) NOT NULL DEFAULT '0',
  `edit_user` int(1) NOT NULL DEFAULT '0',
  `logs` int(1) NOT NULL DEFAULT '0',
  `edit_parser` int(1) NOT NULL DEFAULT '0',
  `save_parser` int(1) NOT NULL DEFAULT '0',
  `edit_template` int(1) NOT NULL DEFAULT '0',
  `settings` int(1) NOT NULL DEFAULT '0',
  `credits` int(1) NOT NULL DEFAULT '0',
  `new_template` int(1) NOT NULL DEFAULT '0',
  `save_template` int(1) NOT NULL DEFAULT '0',
  `delete_template` int(1) NOT NULL DEFAULT '0',
  `edit_snippet` int(1) NOT NULL DEFAULT '0',
  `new_snippet` int(1) NOT NULL DEFAULT '0',
  `save_snippet` int(1) NOT NULL DEFAULT '0',
  `delete_snippet` int(1) NOT NULL DEFAULT '0',
  `edit_chunk` int(1) NOT NULL DEFAULT '0',
  `new_chunk` int(1) NOT NULL DEFAULT '0',
  `save_chunk` int(1) NOT NULL DEFAULT '0',
  `delete_chunk` int(1) NOT NULL DEFAULT '0',
  `empty_cache` int(1) NOT NULL DEFAULT '0',
  `edit_document` int(1) NOT NULL DEFAULT '0',
  `change_password` int(1) NOT NULL DEFAULT '0',
  `error_dialog` int(1) NOT NULL DEFAULT '0',
  `about` int(1) NOT NULL DEFAULT '0',
  `category_manager` int(1) NOT NULL DEFAULT '0',
  `file_manager` int(1) NOT NULL DEFAULT '0',
  `assets_files` int(1) NOT NULL DEFAULT '0',
  `assets_images` int(1) NOT NULL DEFAULT '0',
  `save_user` int(1) NOT NULL DEFAULT '0',
  `delete_user` int(1) NOT NULL DEFAULT '0',
  `save_password` int(11) NOT NULL DEFAULT '0',
  `edit_role` int(1) NOT NULL DEFAULT '0',
  `save_role` int(1) NOT NULL DEFAULT '0',
  `delete_role` int(1) NOT NULL DEFAULT '0',
  `new_role` int(1) NOT NULL DEFAULT '0',
  `access_permissions` int(1) NOT NULL DEFAULT '0',
  `bk_manager` int(1) NOT NULL DEFAULT '0',
  `new_plugin` int(1) NOT NULL DEFAULT '0',
  `edit_plugin` int(1) NOT NULL DEFAULT '0',
  `save_plugin` int(1) NOT NULL DEFAULT '0',
  `delete_plugin` int(1) NOT NULL DEFAULT '0',
  `new_module` int(1) NOT NULL DEFAULT '0',
  `edit_module` int(1) NOT NULL DEFAULT '0',
  `save_module` int(1) NOT NULL DEFAULT '0',
  `delete_module` int(1) NOT NULL DEFAULT '0',
  `exec_module` int(1) NOT NULL DEFAULT '0',
  `view_eventlog` int(1) NOT NULL DEFAULT '0',
  `delete_eventlog` int(1) NOT NULL DEFAULT '0',
  `new_web_user` int(1) NOT NULL DEFAULT '0',
  `edit_web_user` int(1) NOT NULL DEFAULT '0',
  `save_web_user` int(1) NOT NULL DEFAULT '0',
  `delete_web_user` int(1) NOT NULL DEFAULT '0',
  `web_access_permissions` int(1) NOT NULL DEFAULT '0',
  `view_unpublished` int(1) NOT NULL DEFAULT '0',
  `import_static` int(1) NOT NULL DEFAULT '0',
  `export_static` int(1) NOT NULL DEFAULT '0',
  `remove_locks` int(1) NOT NULL DEFAULT '0',
  `display_locks` int(1) NOT NULL DEFAULT '0',
  `change_resourcetype` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Contains information describing the user roles.';

#
# Dumping data for table `alv1_user_roles`
#

INSERT INTO `alv1_user_roles` VALUES ('2','Editor','Limited to managing content','1','1','1','1','1','1','1','0','1','1','1','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','1','0','1','0','1','1','1','1','1','0','1','1','1','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','1','0','0','1','1','1');

INSERT INTO `alv1_user_roles` VALUES ('3','Publisher','Editor with expanded permissions including manage users, update Elements and site settings','1','1','1','1','1','1','1','1','1','1','1','0','1','1','1','0','0','1','1','1','1','1','1','0','0','0','0','1','1','1','1','1','1','1','1','1','0','1','1','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','0','1','0','0','1','1','1','1','0','1','0','0','1','1','1');

INSERT INTO `alv1_user_roles` VALUES ('1','Administrator','Site administrators have full access to all functions','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1');


# --------------------------------------------------------

#
# Table structure for table `alv1_user_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_user_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_user_settings` (
  `user` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text,
  PRIMARY KEY (`user`,`setting_name`),
  KEY `setting_name` (`setting_name`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains backend user settings.';

#
# Dumping data for table `alv1_user_settings`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_web_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_web_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_web_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `webuser` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_group_user` (`webgroup`,`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `alv1_web_groups`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_web_user_attributes`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_web_user_attributes`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_web_user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(25) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text,
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains information for web users.';

#
# Dumping data for table `alv1_web_user_attributes`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_web_user_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_web_user_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_web_user_settings` (
  `webuser` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text,
  PRIMARY KEY (`webuser`,`setting_name`),
  KEY `setting_name` (`setting_name`),
  KEY `webuserid` (`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains web user settings.';

#
# Dumping data for table `alv1_web_user_settings`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_web_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_web_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_web_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `cachepwd` varchar(100) NOT NULL DEFAULT '' COMMENT 'Store new unconfirmed password',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `alv1_web_users`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_webgroup_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_webgroup_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_webgroup_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `alv1_webgroup_access`
#


# --------------------------------------------------------

#
# Table structure for table `alv1_webgroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `alv1_webgroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `alv1_webgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `alv1_webgroup_names`
#
